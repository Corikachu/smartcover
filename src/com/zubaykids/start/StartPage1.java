package com.zubaykids.start;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.zubaykids.initapp.view.SectionsPagerAdapter;
import com.zubaykids.smartcover.R;

public class StartPage1 extends Activity {

	public static final String SERVER_URL = "http://172.16.100.172:52273";
    public static final String EXTRA_MESSAGE = "message";
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    
    private SectionsPagerAdapter mSectionsPagerAdapter;
	private ViewPager mViewPager;

    /*
     * 	공인IP 안내드립니다.
		어제 안내드렸던바와 같이 본 IP는 서비스목적이 아닌 연수생 외부접속용으로
		스크립트 포함한 과도한 점유율을 보이는 서비스는 자제해주시길 바랍니다.
 
		<연수센터 내부장비 입력>
		IP : 172.16.100.172
		Sub: 255.255.254.0
		Gate : 172.16.100.1
 
		<외부장비 입력>
		IP : 61.43.139.146

     */
    
    
    /*
     * 
     * Substitute you own sender ID here. This is the project number you got
     * from the API Console, as described in "Getting Started."
     * 
     */
	//String SENDER_ID = "470318190248";
    String SENDER_ID = "371100803578"; // gcm project number
    //String SENDER_ID = "973498525369"; // gcm project number
    //973498525369
    /**
     * Tag used on log messages.
     */
    static final String TAG = "GCM Demo";

    TextView mDisplay;
    GoogleCloudMessaging gcm;
    AtomicInteger msgId = new AtomicInteger();
    Context context;
	DatePicker day_picker;
	NumberPicker blood_picker;
	NumberPicker sex_picker;
	EditText myPhone;
	EditText partnerPhone;
	Button send_button;

    String regid;
    
//    public BadgeView badge_sum;
//    public BadgeView badge_tmp;
	public TranslateAnimation anim;
	
	 SharedPreferences cnt_tmp;
	 SharedPreferences cnt_sum;
	
	 SharedPreferences.Editor editor;  
     SharedPreferences.Editor editor_tmp;
	
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.startpage1);

		
//        InitBadge();
        mDisplay = (TextView) findViewById(R.id.display);

        context = getApplicationContext();
        
        // Check device for Play Services APK. If check succeeds, proceed with GCM registration.
        if (checkPlayServices()) {

           gcm = GoogleCloudMessaging.getInstance(this);
           regid = getRegistrationId(context);

            if (regid.isEmpty()) {
                registerInBackground();
            }
                
            Log.i("regid", regid);
            
            SharedPreferences mPref = PreferenceManager.getDefaultSharedPreferences(context);
            SharedPreferences.Editor editor = mPref.edit();
            editor.putString("MYREGID", regid);
            editor.commit();


            
        } else {
            Log.i(TAG, "No valid Google Play Services APK found.");
        }
		day_picker = (DatePicker)findViewById(R.id.day_picker);
		blood_picker = (NumberPicker)findViewById(R.id.blood_picker);
		sex_picker = (NumberPicker)findViewById(R.id.sex_picker);
		myPhone = (EditText)findViewById(R.id.myphone);
		partnerPhone = (EditText)findViewById(R.id.partnerphone);
		send_button = (Button)findViewById(R.id.send_button);

		String[] bloodABO_type = new String[4];
		bloodABO_type[0] = "A";
		bloodABO_type[1] = "B";
		bloodABO_type[2] = "AB";
		bloodABO_type[3] = "O";

		blood_picker.setMaxValue(bloodABO_type.length - 1);
		blood_picker.setMinValue(0);
		blood_picker.setDisplayedValues(bloodABO_type);
		
		String[] sex_type = new String[2];
		sex_type[0] = "남";
		sex_type[1] = "여";
		
		sex_picker.setMaxValue(sex_type.length - 1);
		sex_picker.setMinimumHeight(0);
		sex_picker.setDisplayedValues(sex_type);		

		send_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Log.v("onClick", "접근완료");
				doMysql();
				Log.v("doMysql", "AFTER");

				SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
				SharedPreferences.Editor editor = sp.edit();
				editor.putBoolean("START", false);
				editor.putString("BIRTH", Integer.toString(day_picker.getYear()) + 
						addZero(day_picker.getMonth() + 1) + 
						addZero(day_picker.getDayOfMonth()));
				editor.putString("STAR", changeStar(day_picker.getMonth() + 1, day_picker.getDayOfMonth()));
				editor.putString("BLOODTYPE", changeBloodType(blood_picker.getValue()));
				editor.putString("MYPHONENUMBER", "" + myPhone.getText());
				editor.putString("PARTNERPHONENUMBER", "" + partnerPhone.getText());
				editor.putString("SEX", "" + sex_picker.getValue());
				editor.commit();
				
				finish();
				
				startActivity(new Intent(getApplicationContext(),
						com.zubaykids.initapp.SmartCoverMainActivity.class));
				
				checkConnect();
			}
		});
	}

	String changeBloodType(int blood_type) {
		switch(blood_type) {
		case 0 :
			return "A";
		case 1 :
			return "B";
		case 2 :
			return "AB";
		case 3 :
			return "O";
		}
		return "error";
	}

	// 생년월일로 무슨 별자리인지 판단
	/*	Aquarius = 물병자리		10
		Prisces = 물고기자리		11
		Aries = 양자리			0
		Taurus = 황소자리			1
		Gemini = 쌍둥이자리			2
		Cancer = 게자리			3	
		Leo = 사자자리				4
		irgo = 처녀자리			5
		Libra = 천칭자리			6
		Scorpio = 전갈자리			7
		Sagittarius = 사수자리		8
		Capricorn = 염소자리		9
	 */
	String changeStar(int month, int day) {

		switch(month) {
		case 1 :
			if(day <= 19) return "9";
			else return "10";
		case 2 :
			if(day <= 18) return "10";
			else return "11";
		case 3 :
			if(day <= 20) return "11";
			else return "0";
		case 4 :
			if(day <= 19) return "0";
			else return "1";
		case 5 :
			if(day <= 20) return "1";
			else return "2";
		case 6 :
			if(day <= 21) return "2";
			else return "3";
		case 7 :
			if(day <= 22) return "3";
			else return "4";
		case 8 :
			if(day <= 22) return "4";
			else return "5";
		case 9 :
			if(day <= 23) return "5";
			else return "6";
		case 10 :
			if(day <= 22) return "6";
			else return "7";
		case 11 :
			if(day <= 22) return "7";
			else return "8";
		case 12 :
			if(day <= 24) return "8";
			else return "9";
		default :
			return "0";
		}
	}

	String addZero(int number) {

		if(number < 10) {
			return "0" + number;
		}
		else {
			return "" + number;
		}
	}
	
	String changeSex(int sex) {
		if(sex == 0) return "남";
		else return "여";
	}
	
	public void doMysql()
	{
	    Log.v("doMysql", "accessed");

	    AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
	        @Override
	        protected void onPreExecute() {
	            super.onPreExecute();
	            Log.e("AsyncTask", "onPreExecute");
	        }
	        @Override
	        protected String doInBackground(Void... params) {
	            Log.v("AsyncTask", "doInBackground");

	            String msg = "";

	            DefaultHttpClient httpclient = new DefaultHttpClient();
	            HttpPost httppost = new HttpPost("http://172.16.100.172:52273/mysql");

	            ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

	            nameValuePairs.add(new BasicNameValuePair("myday", Integer.toString(day_picker.getYear()) + 
	                    addZero(day_picker.getMonth() + 1) + 
	                    addZero(day_picker.getDayOfMonth())));
	            nameValuePairs.add(new BasicNameValuePair("mystar", changeStar(day_picker.getMonth() + 1, day_picker.getDayOfMonth())));
	            nameValuePairs.add(new BasicNameValuePair("mybt", changeBloodType(blood_picker.getValue())));
	            nameValuePairs.add(new BasicNameValuePair("mynum", "" + myPhone.getText()));
	            nameValuePairs.add(new BasicNameValuePair("yournum", "" + partnerPhone.getText()));
	            nameValuePairs.add(new BasicNameValuePair("mysex", "" + sex_picker.getValue()));
	            nameValuePairs.add(new BasicNameValuePair("myregID", regid));
	            try {
	                Log.v("AsyncTask setEntity", "before");
	                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
	                Log.v("AsyncTask setEntity", "after");

	            } catch (UnsupportedEncodingException e1) {
	                // TODO Auto-generated catch block
	                Log.v("UnsupportedEncodingException", "");
	                e1.printStackTrace();
	            }
	            //172.16.101.28
	            try {  
	                Log.v("AsyncTask Response", "before");
	                HttpResponse httpresponse = httpclient.execute(httppost);
	                Log.v("AsyncTask Response", "after");
	                Log.v("AsyncTask    ",httpresponse.getEntity().toString());
	            } catch (ClientProtocolException e) {
	                Log.v("AsyncTask", "ClientProtocolException");
	                e.printStackTrace();
	                // TODO Auto-generated catch block
	            } catch (IOException e) {
	                Log.v("AsyncTask", "IOException");

	                // TODO Auto-generated catch block
	                e.printStackTrace();
	            }

	            return msg;
	        }
	        @Override
	        protected void onPostExecute(String msg) {
	            Log.v("AsyncTask", "onPostExecute");

	        }
	    };
	    if(Build.VERSION.SDK_INT >= 11)
	        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	    else
	        task.execute();
	}
	

    
/*	private void InitBadge() {
		cnt_tmp =getSharedPreferences("CNT_TMP",
	            Context.MODE_PRIVATE);
		
		cnt_sum =getSharedPreferences("CNT_SUM",
	            Context.MODE_PRIVATE);
		
		editor = cnt_sum.edit(); 
		editor_tmp = cnt_tmp.edit();
		
		//cnt_tmp.
		
		editor.putInt("CNT_SUM",cnt_sum.getInt("CNT_SUM", 0));
		editor.commit();
		
		editor_tmp.putInt("CNT_TMP",cnt_tmp.getInt("CNT_TMP", 0));
		editor_tmp.commit();
        
		View target = findViewById(R.id.push_gcm);
		badge_sum = new BadgeView(getApplicationContext(), target);
		badge_sum.setText(""+cnt_sum.getInt("CNT_SUM", 0));
		badge_sum.setBadgePosition(1);
		
		badge_tmp = new BadgeView(getApplicationContext(), target);
		badge_tmp.setText(""+cnt_tmp.getInt("CNT_TMP",0));
		badge_tmp.setBadgePosition(2);
		
		anim = new TranslateAnimation(0, 0, 100, 0);
        anim.setInterpolator(new BounceInterpolator());
        anim.setDuration(1000);

        	badge_sum.show();
        	badge_tmp.show();
         
	}*/

    @Override
    protected void onResume() {
        super.onResume();
        // Check device for Play Services APK.
        checkPlayServices();
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

    /**
     * Stores the registration ID and the app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param context application's context.
     * @param regId registration ID
     */
    private void storeRegistrationId(Context context, String regId) {
    	final SharedPreferences prefs = getGcmPreferences(context);
    	int appVersion = getAppVersion(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    /**
     * Gets the current registration ID for application on GCM service, if there is one.
     * <p>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     *         registration ID.
     */
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getGcmPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");

        if (registrationId.isEmpty()) {
            Log.i(TAG, "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i(TAG, "App version changed.");
            return "";
        }
        return registrationId;
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and the app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground() {

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {

                String msg = "";
                try {

                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    				}
                regid = gcm.register(SENDER_ID);
                msg = "Device registered, registration ID=" + regid;
                
                SharedPreferences mPref = PreferenceManager.getDefaultSharedPreferences(context);
                SharedPreferences.Editor editor = mPref.edit();
                editor.putString("MYREGID", regid);
                editor.commit();


                    // You should send the registration ID to your server over HTTP, so it
                    // can use GCM/HTTP or CCS to send messages to your app.
                sendRegistrationIdToBackend();

                    // For this demo: we don't need to send it because the device will send
                    // upstream messages to a server that echo back the message using the
                    // 'from' address in the message.

                    // Persist the regID - no need to register again.
                storeRegistrationId(context, regid);
                }
                
                catch (Exception ex) {
                	msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
//                mDisplay.append(msg + "\n");
            }
        }.execute(null, null, null);
    }
/*
    // Send an upstream message.
    public void onClick(final View view) {

        if (view == findViewById(R.id.send)) {
        	
            new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... params) {
                    String msg = "";
                    
                	DefaultHttpClient httpclient = new DefaultHttpClient();
                	//HttpPost httppost = new HttpPost("http://223.62.188.61:52273/push");
                	//HttpPost httppost = new HttpPost("http://172.16.101.238:52273/push");
                	//HttpPost httppost = new HttpPost("http://172.16.101.42:52273/push");
                	//HttpPost httppost = new HttpPost("http://ulamk.kr:52273/push");
                	HttpPost httppost = new HttpPost("http://172.16.100.172:52273/push");
                	
                	ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                    nameValuePairs.add(new BasicNameValuePair("regid", "APA91bFXA2B6NG488fuPusCx6Dac4ygTMBnnrb67bWm9YA_WTHY5J5yEf5LoXIhCS8nDOzk9Uf9Sk-4BLjauAaam-NYn2Kzj27peaVUyChxfToeSkzc0mmgwuEC3gsGx8jRFpb2IN21WDNG3Y8LxG1BFUGastdmtoA"));
                    //nameValuePairs.add(new BasicNameValuePair("stringdata", "AndDev is Cool!"));
                    try {
						Log.v("1111", "1111");
						httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
						Log.v("2222", "22222");

					} catch (UnsupportedEncodingException e1) {
						// TODO Auto-generated catch block
						Log.v("UnsupportedEncodingException", "");
						e1.printStackTrace();
					}
                	//172.16.101.28
                    try {  
						Log.v("3333", "3333");
                    	HttpResponse httpresponse = httpclient.execute(httppost);
                    	Log.v("HttpResponse	",httpresponse.getEntity().toString());
                    } catch (ClientProtocolException e) {
						Log.v("ClientProtocolException", "ClientProtocolException");

                    	e.printStackTrace();
                        // TODO Auto-generated catch block
                    } catch (IOException e) {
						Log.v("IOException", "IOException");

                        // TODO Auto-generated catch block
                    	e.printStackTrace();
                    }

                    return msg;
                }

                @Override
                protected void onPostExecute(String msg) {
                    mDisplay.append(msg + "\n");
                }
            		}.execute(null, null, null);
            		
        	}
        
       else if (view == findViewById(R.id.clear)) {
            mDisplay.setText("");
        	}
        
        
       else if (view == findViewById(R.id.push_gcm)){
        	badge_tmp.hide();
        	
        	editor_tmp.putInt("CNT_TMP", 0);
        	editor_tmp.commit();
        	
        }
        
        
       else if (view == findViewById(R.id.register)){
           Log.e("11111","11111");

			new AsyncTask<Void,String,String>() {
		        @Override
		        protected String doInBackground(Void... params) {
		        	
		        	//DataOutputStream toGCMBrokerServer = null;
	    			//DataInputStream  fromGCMBrokerServer = null;
	    			//Socket socket = null;
	    			String resultMessage = "";
		            try {
		                if (gcm == null) {
			                Log.e("11111","2222");
		                	gcm = GoogleCloudMessaging.getInstance(
		                			                  getApplicationContext());
			                Log.e("11111","3333");

		                }
		                
		                 * GCM 서버로 현재 애플리케이션의 GCM을 보낼 수 있도록
		                 * 등록 ID를 요청한다.내부적으로 네트워크 코드가 실행되므로
		                 * 반드시 Back Ground Thread내에서 실행하여야 한다.
		                 
		                Log.e("11111","4444");

		                regid = gcm.register(SENDER_ID);
		                Log.e("발급받은 등록 ID 값 =>", regid);
		                
		                gcm.close();
		                Log.e("11111","55555");

		                // 이상 현재 진행상
		    			
		            }catch (IOException ex) {
		                Log.e("GCM.REGISTER.SEND.ERROR", ex.toString());
		            }
	                Log.e("11111","11111");
		            return resultMessage;
		        }
		        @Override
		        protected void onPostExecute(String msg) {
	                Log.e("발급받은 등록 ID 값 =>", regid);
		        }
		    }.execute(null,null,null);
       }
        
    }*/

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion(Context context) 
    {
        try 
        {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * @return Application's {@code SharedPreferences}.
     */
    private SharedPreferences getGcmPreferences(Context context) 
    {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(StartPage1.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }
    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP or CCS to send
     * messages to your app. Not needed for this demo since the device sends upstream messages
     * to a server that echoes back the message using the 'from' address in the message.
     */
    private void sendRegistrationIdToBackend() {
    	Log.v("11111","11111");
    	

    	//HttpClient httpclient = new DefaultHttpClient();
    	DefaultHttpClient httpclient = new DefaultHttpClient();
    	Log.v("11111","22222");

    	//HttpPost httppost = new HttpPost(Constant.IP_MAC+":52273/push");
    	HttpPost httppost = new HttpPost("http://223.62.188.61:52273/push");

        try {  
            // Add your data
            // List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            //nameValuePairs.add(new BasicNameValuePair("id", "12345"));
            //nameValuePairs.add(new BasicNameValuePair("stringdata", "AndDev is Cool!"));
            //httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            // Execute HTTP Post Request
            //HttpResponse response = httpclient.execute(httppost);

        	HttpResponse httpresponse = httpclient.execute(httppost);
        	Log.v("1111",httpresponse.getEntity().toString());
            
        } catch (ClientProtocolException e) {
        	Log.v("ClientProtocolException e","6666");

            // TODO Auto-generated catch block
        } catch (IOException e) {
            // TODO Auto-generated catch block
        	Log.v("IOException e",Log.getStackTraceString(e));
        }
    }
    
    void checkConnect() {
    	new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";

				DefaultHttpClient httpclient = new DefaultHttpClient();
				HttpPost httppost = new HttpPost("http://172.16.100.172:52273/check");

				ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("myregID", regid));
				try {
					Log.v("setEntity", "before");
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					Log.v("setEntity", "after");

				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					Log.v("UnsupportedEncodingException", "");
					e1.printStackTrace();
				}
				//172.16.101.28
				try {
					HttpResponse httpresponse = httpclient.execute(httppost);
					Log.v("HttpResponse	",httpresponse.getEntity().toString());
				} catch (ClientProtocolException e) {
					Log.v("ClientProtocolException", "ClientProtocolException");
					e.printStackTrace();
					// TODO Auto-generated catch block
				} catch (IOException e) {
					Log.v("IOException", "IOException");

					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return msg;
			}
			@Override
			protected void onPostExecute(String msg) {
			}
		}.execute(null, null, null);
    }
}