package com.zubaykids.weather;

import java.io.InputStream;
import java.net.URL;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.zubaykids.SaveInfo.PartnerInfo;
import com.zubaykids.initapp.ZubayApplication;
import com.zubaykids.smartcover.R;

public class WeatherService extends Service {

	private double latitude;
	private double longitude;
	private String code;
	private TimeWeather timeWeather = new TimeWeather();
	String city = new PartnerInfo().getCity();
	
	Handler mHandler;
	
	private String WEATHER_TEMP = ZubayApplication.SHARED_PREFERENCE_WEATHER_TEMP;
	private String WEATHER_IMAGE = ZubayApplication.SHARED_PREFERENCE_WEATHER_IMAGE;

	@Override
	public void onCreate() {
		super.onCreate();
		
		mHandler = new Handler();
		new Thread() {
			@Override
			public void run() {
				while(true) {
					mHandler.post(new Runnable() {

						@Override
						public void run() {
							setCity(new PartnerInfo().getCity());
							XmlParsing();
							saveWeather();
						}
					});
					try {
						Thread.sleep(1800000);
					}
					catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	public String getTem() {
		return timeWeather.getTem();
	}

	public String getCon() {
		return timeWeather.getCon();
	}

	void saveWeather() {
		SharedPreferences mPref = PreferenceManager.getDefaultSharedPreferences(this);

		SharedPreferences.Editor editor = mPref.edit();
		editor.putString(WEATHER_TEMP, timeWeather.getTem() + "º");
		editor.putString(WEATHER_IMAGE, timeWeather.getCon());
		editor.apply();
	}

	public void setCity(String city) {
		switch(city) {

		case "서울" :
			latitude = 37.564341;
			longitude = 126.975609;
			code = "1100000000";
			break;

		case "인천" :
			latitude = 37.455682;
			longitude = 126.704472;
			code = "2800000000";
			break;

		case "대전" :
			latitude = 36.350412;
			longitude = 127.384547;
			code = "3000000000";
			break;

		case "대구" :
			latitude = 35.871436;
			longitude = 128.601445;
			code = "2700000000";
			break;

		case "울산" :
			latitude = 35.538739;
			longitude = 129.311360;
			code = "3100000000";
			break;

		case "광주" :
			latitude = 35.160073;
			longitude = 126.851434;
			code = "2900000000";
			break;

		case "부산" :
			latitude = 35.179555;
			longitude = 129.075642;
			code = "2600000000";
			break;

		case "제주" :
			latitude = 33.499597;
			longitude = 126.531254;
			code = "5000000000";
			break;
		}
	}

	public void XmlParsing() {
		String UrlAddress = "http://www.kma.go.kr/wid/queryDFSRSS.jsp?zone=";
		String tagName = "";
		int count = 0;
		boolean onTem = false;
		boolean onCon = false;
		boolean end = false;

		StrictMode.enableDefaults();

		UrlAddress = UrlAddress + code;

		try {
			URL url = new URL(UrlAddress);

//			HttpURLConnection http= (HttpURLConnection) url.openConnection();
//			InputStream in = http.getInputStream();
			
			InputStream in = url.openStream();
			
			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			XmlPullParser parser = factory.newPullParser();

			parser.setInput(in, "utf-8");

			int eventType = parser.getEventType();
			boolean isItemTag1 = false;

			timeWeather = new TimeWeather();

			while (eventType != XmlPullParser.END_DOCUMENT) {
				if (eventType == XmlPullParser.START_TAG) {
					tagName = parser.getName();
					if(tagName.equals("data")) {
						isItemTag1 = true;
					}
				} else if (eventType == XmlPullParser.TEXT && isItemTag1) {
					if(tagName.equals("temp") && !onTem) {
						timeWeather.setTem(parser.getText());
						onTem = true;
					}
					if(tagName.equals("wfKor") && !onCon) {
						timeWeather.setCon(parser.getText());
						onCon = true;
					}
				} else if (eventType == XmlPullParser.END_TAG) {
					if(tagName.equals("s06") && end == false) {
						isItemTag1 = false;
						onTem = false;
						onCon = false;
						end = true;
						
						break;
					}
				}

				eventType = parser.next();
			}
		} catch (Exception e) {
			Toast.makeText(this, "error", Toast.LENGTH_SHORT).show();
		}
	}

	public int changeCon(String con) {
		if(con.equals("맑음")) {
			return R.drawable.sunny;
		} else if(con.equals("구름 조금")) {
			return R.drawable.cloud;
		} else if(con.equals("구름 많음")) {
			return R.drawable.many_cloud;
		} else if(con.equals("흐림")) {
			return R.drawable.sunny_cloud;
		} else if(con.equals("비")) {
			return R.drawable.rain;
		} else if(con.equals("눈/비")) {
			return R.drawable.snow_rain;
		} else if(con.equals("눈")) {
			return R.drawable.snow;
		} else {
			return R.drawable.sunny;
		}
	}
}