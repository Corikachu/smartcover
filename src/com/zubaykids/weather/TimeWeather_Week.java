package com.zubaykids.weather;

public class TimeWeather_Week {
	
	private String day;	// 날짜
	private String con;	// 날씨 상태
	private String tmn;	// 최저온도
	private String tmx;	// 최고온도
	private String pre;	// 강수 확률
	
	public String getDay() {
		return day;
	}
	public void setDay(String day) {
		this.day = day;
	}
	public String getCon() {
		return con;
	}
	public void setCon(String con) {
		this.con = con;
	}
	public String getTmn() {
		return tmn;
	}
	public void setTmn(String tmn) {
		this.tmn = tmn;
	}
	public String getTmx() {
		return tmx;
	}
	public void setTmx(String tmx) {
		this.tmx = tmx;
	}
	public String getPre() {
		return pre;
	}
	public void setPre(String pre) {
		this.pre = pre;
	}
}
