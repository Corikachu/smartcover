package com.zubaykids.weather;

public class TimeWeather {
	private String hour;	// 시간
	private String day;	// 날짜
	private String tem;	// 온도
	private String con;	// 상태
	private String pre;	// 강수 확률
	
	public void setHour(String hour) {
		this.hour = hour;
	}
	
	public void setDay(String day) {
		this.day = day;
	}
	
	public void setTem(String tem) {
		this.tem = tem;
	}
	
	public void setCon(String con) {
		this.con = con;
	}
	
	public void setPre(String pre) {
		this.pre = pre;
	}
	
	public String getHour() {
		return hour;
	}
	
	public String getDay() {
		return day;
	}
	
	public String getTem() {
		return tem;
	}
	public String getCon() {
		return con;
	}
	
	public String getPre() {
		return pre;
	}
}
