package com.zubaykids.weather;

import java.io.InputStream;
import java.net.URL;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import android.app.Activity;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.zubaykids.SaveInfo.PartnerInfo;
import com.zubaykids.smartcover.R;

public class Weather extends Activity {

	private double latitude;
	private double longitude;
	private String code;
	private String city = new PartnerInfo().getCity();

	private TimeWeather[] timeWeather = new TimeWeather[24];
	private TextView address;
	private TextView[] day_TextView = new TextView[5];
	private TextView[] hour_TextView = new TextView[5];
	private ImageView[] con_ImageView = new ImageView[5];
	private TextView[] tem_TextView = new TextView[5];
	private TextView[] pre_TextView = new TextView[5];
	private ImageView back_button;

	private TimeWeather_Week[] timeWeather_week = new TimeWeather_Week[5];
	private TextView address_week;
	private TextView[] day_TextView_week = new TextView[5];
	private ImageView[] con_ImageView_week = new ImageView[5];
	private TextView[] tmn_TextView_week = new TextView[5];
	private ImageView back_button_week;

	private TableLayout weatherLayout1;
	private TableLayout weatherLayout2;

	private double xAtDown;
	private double yAtDown;
	private double xAtUp;
	private double yAtUp;

	private Animation anim_up1;
	private Animation anim_up2;
	private Animation anim_down1;
	private Animation anim_down2;

	private final int SWIPE_MIN_DISTANCE = 300;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,	// block state bar
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);	// block name bar
		setContentView(R.layout.activity_weather);

		anim_up1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.weather_page_anim_up1);
		anim_up2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.weather_page_anim_up2);
		anim_down1 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.weather_page_anim_down1);
		anim_down2 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.weather_page_anim_down2);

		setView();
		setCity(city);
		XmlParsing();
		XmlParsing_week();
		setWeather();

		back_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		back_button_week.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();				
			}
		});

		weatherLayout1.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					xAtDown = event.getX();
					yAtDown = event.getY();

					break;
				case MotionEvent.ACTION_UP:
					xAtUp = event.getX();
					yAtUp = event.getY();

					if (yAtDown - yAtUp > SWIPE_MIN_DISTANCE) {
						weatherLayout1.setVisibility(View.INVISIBLE);
						weatherLayout2.setVisibility(View.VISIBLE);
						weatherLayout1.startAnimation(anim_up1);
						weatherLayout2.startAnimation(anim_up2);
					} else if(yAtUp - yAtDown > SWIPE_MIN_DISTANCE) {
						weatherLayout1.setVisibility(View.INVISIBLE);
						weatherLayout2.setVisibility(View.VISIBLE);
						weatherLayout1.startAnimation(anim_down1);
						weatherLayout2.startAnimation(anim_down2);
					} else if(xAtUp- xAtDown > SWIPE_MIN_DISTANCE) {
						 
						finish();
					}
					break;
				}
				return true;
			}
		});

		weatherLayout2.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					xAtDown = event.getX();
					yAtDown = event.getY();

					break;
				case MotionEvent.ACTION_UP:
					xAtUp = event.getX();
					yAtUp = event.getY();

					if (yAtUp - yAtDown > SWIPE_MIN_DISTANCE) {
						weatherLayout2.setVisibility(View.INVISIBLE);
						weatherLayout1.setVisibility(View.VISIBLE);
						weatherLayout1.startAnimation(anim_down1);
						weatherLayout2.startAnimation(anim_down2);
					} else if(yAtDown - yAtUp > SWIPE_MIN_DISTANCE) {
						weatherLayout2.setVisibility(View.INVISIBLE);
						weatherLayout1.setVisibility(View.VISIBLE);
						weatherLayout1.startAnimation(anim_up1);
						weatherLayout2.startAnimation(anim_up2);
					} else if(xAtUp- xAtDown > SWIPE_MIN_DISTANCE ) {
						finish();
					}
					break;
				}
				return true;
			}
		});
	}

	void setCity(String city) {
		switch(city) {

		case "서울" :
			latitude = 37.564341;
			longitude = 126.975609;
			code = "1100000000";
			break;

		case "인천" :
			latitude = 37.455682;
			longitude = 126.704472;
			code = "2800000000";
			break;

		case "대전" :
			latitude = 36.350412;
			longitude = 127.384547;
			code = "3000000000";
			break;

		case "대구" :
			latitude = 35.871436;
			longitude = 128.601445;
			code = "2700000000";
			break;

		case "울산" :
			latitude = 35.538739;
			longitude = 129.311360;
			code = "3100000000";
			break;

		case "광주" :
			latitude = 35.160073;
			longitude = 126.851434;
			code = "2900000000";
			break;

		case "부산" :
			latitude = 35.179555;
			longitude = 129.075642;
			code = "2600000000";
			break;

		case "제주" :
			latitude = 33.499597;
			longitude = 126.531254;
			code = "5000000000";
			break;
		}
	}

	void XmlParsing() {
		String UrlAddress = "http://www.kma.go.kr/wid/queryDFSRSS.jsp?zone=";
		String tagName = "";
		int count = 0;
		boolean onHour = false;
		boolean onDay = false;
		boolean onTem = false;
		boolean onCon = false;
		boolean onPre = false;
		boolean end = false;

		StrictMode.enableDefaults();

		//full_address = first_address + Double.toString(latitude) + second_address + Double.toString(longitude);
		UrlAddress = UrlAddress + code;

		try {
			URL url = new URL(UrlAddress);

			//			HttpURLConnection http= (HttpURLConnection) url.openConnection();
			//			InputStream in = http.getInputStream();

			InputStream in = url.openStream();

			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			XmlPullParser parser = factory.newPullParser();

			parser.setInput(in, "utf-8");

			int eventType = parser.getEventType();
			boolean isItemTag1 = false;
			String tmp_hour = "";

			timeWeather[count] = new TimeWeather();

			while (eventType != XmlPullParser.END_DOCUMENT) {
				if (eventType == XmlPullParser.START_TAG) {
					tagName = parser.getName();
					if(tagName.equals("data")) {
						isItemTag1 = true;
					}
				} else if (eventType == XmlPullParser.TEXT && isItemTag1) {
					if(tagName.equals("hour") && !onHour) {
						tmp_hour = parser.getText();
						timeWeather[count].setHour(changeHour(tmp_hour));
						onHour = true;
						end = false;
					}
					if(tagName.equals("day") && !onDay) {
						timeWeather[count].setDay(changeDay(parser.getText(), tmp_hour));
						onDay = true;
					}
					if(tagName.equals("temp") && !onTem) {
						timeWeather[count].setTem(parser.getText());
						onTem = true;
					}
					if(tagName.equals("wfKor") && !onCon) {
						timeWeather[count].setCon(parser.getText());
						onCon = true;
					}
					if(tagName.equals("pop") && !onPre) {
						timeWeather[count].setPre(parser.getText());
						onPre = true;
					}
				} else if (eventType == XmlPullParser.END_TAG) {
					if(tagName.equals("s06") && end == false) {
						isItemTag1 = false;
						onHour = false;
						onDay = false;
						onTem = false;
						onCon = false;
						onPre = false;
						end = true;
						count++;
						timeWeather[count] = new TimeWeather();
					}
				}

				eventType = parser.next();
			}
		} catch (Exception e) {
			Toast.makeText(this, "error", Toast.LENGTH_SHORT).show();
		}
	}

	void XmlParsing_week() {
		String UrlAddress = "http://www.kma.go.kr/weather/forecast/mid-term-xml.jsp?stnId=108";
		String tagName = "";
		int count = 0;
		boolean onDay = false;
		boolean onCon = false;
		boolean onTmn = false;
		boolean onTmx = false;
		boolean onPre = false;
		boolean onEnd = false;

		//StrictMode.enableDefaults();

		try {
			URL url = new URL(UrlAddress);

			InputStream in = url.openStream();

			XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
			XmlPullParser parser = factory.newPullParser();

			parser.setInput(in, "utf-8");

			int eventType = parser.getEventType();
			boolean isItemTag1 = false;
			boolean isItemTag2 = false;

			timeWeather_week[count] = new TimeWeather_Week();

			while (eventType != XmlPullParser.END_DOCUMENT) {
				if (eventType == XmlPullParser.START_TAG) {
					tagName = parser.getName();
					if(tagName.equals("city")) {
						isItemTag1 = true;
					}
				} else if (eventType == XmlPullParser.TEXT && isItemTag1) {
					if(city.equals(parser.getText()) && isItemTag1) {
						isItemTag2 = true;
					}
					if(isItemTag2) {
						if(tagName.equals("numEf") && !onDay) {
							timeWeather_week[count].setDay(changeDay_week(parser.getText()));
							onDay = true;
						}
						if(tagName.equals("wf") && !onCon) {
							timeWeather_week[count].setCon(parser.getText());
							onCon = true;
						}
						if(tagName.equals("tmn") && !onTmn) {
							timeWeather_week[count].setTmn(parser.getText());
							onTmn = true;
						}
						if(tagName.equals("tmx") && !onTmx) {
							timeWeather_week[count].setTmx(parser.getText());
							onTmx = true;
							onEnd = true;
						}
						if(tagName.equals("reliability") && !onPre) {
							timeWeather_week[count].setPre(parser.getText());
							onPre = true;
						}
					}
				} else if (eventType == XmlPullParser.END_TAG) {
					if(tagName.equals("reliability") && onEnd) {
						onDay = false;
						onCon = false;
						onTmn = false;
						onTmx = false;
						onPre = false;
						onEnd = false;
						count++;
						
						if(count == 5) break;
						timeWeather_week[count] = new TimeWeather_Week();
					}
				}
				
				eventType = parser.next();
			}
		} catch (Exception e) {
			Toast.makeText(this, "error", Toast.LENGTH_SHORT).show();
		}
	}

	void setView() {

		back_button = (ImageView)findViewById(R.id.back_button_weather);
		address = (TextView)findViewById(R.id.address);
		day_TextView[0] = (TextView)findViewById(R.id.day1);
		day_TextView[1] = (TextView)findViewById(R.id.day2);
		day_TextView[2] = (TextView)findViewById(R.id.day3);
		day_TextView[3] = (TextView)findViewById(R.id.day4);
		day_TextView[4] = (TextView)findViewById(R.id.day5);
		hour_TextView[0] = (TextView)findViewById(R.id.hour1);
		hour_TextView[1] = (TextView)findViewById(R.id.hour2);
		hour_TextView[2] = (TextView)findViewById(R.id.hour3);
		hour_TextView[3] = (TextView)findViewById(R.id.hour4);
		hour_TextView[4] = (TextView)findViewById(R.id.hour5);
		con_ImageView[0] = (ImageView)findViewById(R.id.con1);
		con_ImageView[1] = (ImageView)findViewById(R.id.con2);
		con_ImageView[2] = (ImageView)findViewById(R.id.con3);
		con_ImageView[3] = (ImageView)findViewById(R.id.con4);
		con_ImageView[4] = (ImageView)findViewById(R.id.con5);
		tem_TextView[0] = (TextView)findViewById(R.id.tem1);
		tem_TextView[1] = (TextView)findViewById(R.id.tem2);
		tem_TextView[2] = (TextView)findViewById(R.id.tem3);
		tem_TextView[3] = (TextView)findViewById(R.id.tem4);
		tem_TextView[4] = (TextView)findViewById(R.id.tem5);
		pre_TextView[0] = (TextView)findViewById(R.id.pre1);
		pre_TextView[1] = (TextView)findViewById(R.id.pre2);
		pre_TextView[2] = (TextView)findViewById(R.id.pre3);
		pre_TextView[3] = (TextView)findViewById(R.id.pre4);
		pre_TextView[4] = (TextView)findViewById(R.id.pre5);

		back_button_week = (ImageView)findViewById(R.id.back_button_week);
		address_week = (TextView)findViewById(R.id.address_week);
		day_TextView_week[0] = (TextView)findViewById(R.id.day1_week);
		day_TextView_week[1] = (TextView)findViewById(R.id.day2_week);
		day_TextView_week[2] = (TextView)findViewById(R.id.day3_week);
		day_TextView_week[3] = (TextView)findViewById(R.id.day4_week);
		day_TextView_week[4] = (TextView)findViewById(R.id.day5_week);
		con_ImageView_week[0] = (ImageView)findViewById(R.id.con1_week);
		con_ImageView_week[1] = (ImageView)findViewById(R.id.con2_week);
		con_ImageView_week[2] = (ImageView)findViewById(R.id.con3_week);
		con_ImageView_week[3] = (ImageView)findViewById(R.id.con4_week);
		con_ImageView_week[4] = (ImageView)findViewById(R.id.con5_week);
		tmn_TextView_week[0] = (TextView)findViewById(R.id.tmn1_week);
		tmn_TextView_week[1] = (TextView)findViewById(R.id.tmn2_week);
		tmn_TextView_week[2] = (TextView)findViewById(R.id.tmn3_week);
		tmn_TextView_week[3] = (TextView)findViewById(R.id.tmn4_week);
		tmn_TextView_week[4] = (TextView)findViewById(R.id.tmn5_week);

		weatherLayout1 = (TableLayout)findViewById(R.id.weatherLayout1);
		weatherLayout2 = (TableLayout)findViewById(R.id.weatherLayout2);
	}

	void setWeather() {

		address.setText(setAddress(0));

		for(int i=0; i<5; i++) {
			day_TextView[i].setText(timeWeather[i].getDay());
			hour_TextView[i].setText(timeWeather[i].getHour());
			con_ImageView[i].setImageResource(changeCon(timeWeather[i].getCon()));
			tem_TextView[i].setText(timeWeather[i].getTem() + "º");
			pre_TextView[i].setText(timeWeather[i].getPre() + "%");
		}
		
		address_week.setText(setAddress(1));

		for(int i=0; i<5; i++) {
			day_TextView_week[i].setText(timeWeather_week[i].getDay());
			con_ImageView_week[i].setImageResource(changeCon(timeWeather_week[i].getCon()));
			tmn_TextView_week[i].setText(timeWeather_week[i].getTmn() + "º\n\n/  \n\n" + timeWeather_week[i].getTmx() + "º");

		}
	}

	String changeDay(String day, String hour) {
		Calendar cal = Calendar.getInstance();

		int month_Int;
		int day_Int;
		int dayofweek_Int;
		String month_String;
		String day_String;
		String dayofweek_String;

		if(!day.equals("0") || hour.equals("24")) {
			cal.add(Calendar.DATE, 1);
		}

		month_Int = cal.get(Calendar.MONTH) + 1;
		day_Int = cal.get(Calendar.DAY_OF_MONTH);
		dayofweek_Int = cal.get(Calendar.DAY_OF_WEEK);

		if(month_Int < 10) {
			month_String = "0" + month_Int;
		}
		else {
			month_String = "" + month_Int;
		}

		if(day_Int < 10) {
			day_String = "0" + day_Int;
		}
		else {
			day_String = "" + day_Int;
		}

		switch(dayofweek_Int) {
		case 1 :
			dayofweek_String = "일";
			break;
		case 2 :
			dayofweek_String = "월";
			break;
		case 3 :
			dayofweek_String = "화";
			break;
		case 4 :
			dayofweek_String = "수";
			break;
		case 5 :
			dayofweek_String = "목";
			break;
		case 6 :
			dayofweek_String = "금";
			break;
		case 7 :
			dayofweek_String = "토";
			break;
		default :
			dayofweek_String = "일";
		}

		return month_String + "/" + day_String + "\n(" + dayofweek_String + ")";

	}
	
	String changeDay_week(String day) {
		Calendar cal = Calendar.getInstance();

		int month_Int;
		int day_Int;
		int dayofweek_Int;
		String month_String;
		String day_String;
		String dayofweek_String;

		cal.add(Calendar.DATE, Integer.parseInt(day) - 1);

		month_Int = cal.get(Calendar.MONTH) + 1;
		day_Int = cal.get(Calendar.DAY_OF_MONTH);
		dayofweek_Int = cal.get(Calendar.DAY_OF_WEEK);

		if(month_Int < 10) {
			month_String = "0" + month_Int;
		}
		else {
			month_String = "" + month_Int;
		}

		if(day_Int < 10) {
			day_String = "0" + day_Int;
		}
		else {
			day_String = "" + day_Int;
		}

		switch(dayofweek_Int) {
		case 1 :
			dayofweek_String = "일";
			break;
		case 2 :
			dayofweek_String = "월";
			break;
		case 3 :
			dayofweek_String = "화";
			break;
		case 4 :
			dayofweek_String = "수";
			break;
		case 5 :
			dayofweek_String = "목";
			break;
		case 6 :
			dayofweek_String = "금";
			break;
		case 7 :
			dayofweek_String = "토";
			break;
		default :
			dayofweek_String = "일";
		}

		return month_String + "/" + day_String + "\n(" + dayofweek_String + ")";

	}

	String changeHour(String hour) {
		if(hour.equals("3")) {
			return "오전 3시";
		}
		else if(hour.equals("6")) {
			return "오전 6시";
		}
		else if(hour.equals("9")) {
			return "오전 9시";
		}
		else if(hour.equals("12")) {
			return "오후 12시";
		}
		else if(hour.equals("15")) {
			return "오후 3시";
		}
		else if(hour.equals("18")) {
			return "오후 6시";
		}
		else if(hour.equals("21")) {
			return "오후 9시";
		}
		else if(hour.equals("24")) {
			return "오전 0시";
		}
		else {
			return "error";
		}
	}

	int changeCon(String con) {
		if(con.equals("맑음")) {
			return R.drawable.sunny;
		} else if(con.equals("구름 조금")) {
			return R.drawable.cloud;
		} else if(con.equals("구름 많음")) {
			return R.drawable.many_cloud;
		} else if(con.equals("흐림")) {
			return R.drawable.sunny_cloud;
		} else if(con.equals("비")) {
			return R.drawable.rain;
		} else if(con.equals("눈/비")) {
			return R.drawable.snow_rain;
		} else if(con.equals("눈")) {
			return R.drawable.snow;
		} else {
			return R.drawable.sunny;
		}
	}

	String setAddress(int week) {
		Geocoder geocoder = new Geocoder(this, Locale.getDefault());

		List<Address> list = null;

		try {
			list = geocoder.getFromLocation(latitude, longitude, 1);
		} catch(Exception e) {
			e.printStackTrace();
		}

		Address addr = list.get(0);

		if(week == 0) return addr.getAdminArea() + "(일간)";
		else return addr.getAdminArea() + "(주간)";
	}
}