package com.zubaykids.gcm;

import java.io.File;
import java.io.FileOutputStream;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.MediaScannerConnectionClient;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import com.zubaykids.initapp.ZubayApplication;

public class SuggestItem {
	
	private SharedPreferences sharedPref;
	private Context context;
	
	private BitmapLoader mBitmapLoader;
	
	private String sharedFileName = ZubayApplication.SHARED_PREFERENCE_SUGGEST_ITEM_FILENAME;
	private String sharedTitle = ZubayApplication.SHARED_PREFERENCE_SUGGEST_TITLE;
	private String sharedContent = ZubayApplication.SHARED_PREFERENCE_SUGGEST_CONTENT;
	private String sharedCount = ZubayApplication.SHARED_PREFERENCE_SUGGEST_COUNT;
	private String sharedUsed = ZubayApplication.SHARED_PREFERENCE_SUGGEST_IS_USE;
	private String sharedUrl = ZubayApplication.SHARED_PREFERENCE_SUGGEST_URL;
	
	public SuggestItem(){
		this.context = ZubayApplication.getInstance().getApplicationContext();
		mBitmapLoader = new BitmapLoader();
		sharedPref = context.getSharedPreferences(sharedFileName, Context.MODE_PRIVATE);
	}

	public void saveItem(Item item){
		if(item == null){
			return;
		}
		
		SharedPreferences.Editor edit = sharedPref.edit();
		
		int count = item.getCount();
		
		edit.putString(sharedTitle+count, item.getTitle());
		edit.putString(sharedContent+count, item.getContent());
		edit.putInt(sharedCount+count, count);
		edit.putBoolean(sharedUsed+count, true);
		edit.putString(sharedUrl+count, item.getUrl());
		edit.apply();
		
		mBitmapLoader.saveBitmapToFile(item.getBitmap(), count);
	}
	
	public void updateItem(Item item, int count){
		if(item == null){
			return;
		}
		
		SharedPreferences.Editor edit = sharedPref.edit();
		
		edit.putString(sharedTitle+count, item.getTitle());
		edit.putString(sharedContent+count, item.getContent());
		edit.putInt(sharedCount+count, count);
		edit.putBoolean(sharedUsed+count, true);
		edit.putString(sharedUrl+count, item.getUrl());
		edit.apply();
		
		mBitmapLoader.saveBitmapToFile(item.getBitmap(), count);
		
	}
	
	public void deleteItem(int count){
		SharedPreferences.Editor edit = sharedPref.edit();
		edit.putBoolean(sharedUsed+count, false);
		edit.apply();
	}
	
	public Item loadItem(int count){
		
		Item item = new Item();
		if(isItemUse(count)){
			item.setTitle(sharedPref.getString(sharedTitle+count, "DEFAULT"));
			item.setContent(sharedPref.getString(sharedContent+count, "DEFAULT"));
			item.setCount(sharedPref.getInt(sharedCount+count, 0));
			item.setUrl(sharedPref.getString(sharedUrl+count, "DEFAULT"));
			item.setBitmap(mBitmapLoader.loadBitmapFromFile(count));
		}else{
			return null;
		}
		return item;
	}
	
	public boolean isItemUse(int count){
		return sharedPref.getBoolean(sharedUsed+count, false);
	}
	
	public class BitmapLoader {
		
		public void saveBitmapToFile(Bitmap bitmap, int count){
			File filePath = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
			Log.w("FilePath", filePath.getPath().toString());
			File fileItem = new File(filePath, ""+count+".PNG");
			filePath.mkdir();
			
			FileOutputStream output = null;
			
			try{
				output = new FileOutputStream(fileItem);
				bitmap.compress(Bitmap.CompressFormat.PNG, 100, output);
				output.close();
			}catch(Exception e){
				e.printStackTrace();
			}
			String[] paths = new String[1];
			paths[0] = fileItem.getPath().toString();
			MediaScannerConnection.scanFile(context, paths, null, null);
			
			SharedPreferences.Editor editor = sharedPref.edit();
			editor.putBoolean(ZubayApplication.SHARED_PREFERENCE_SUGGEST_DATA_CHANGED, true);
			editor.apply();
			
		}
		
		public Bitmap loadBitmapFromFile(int count){
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inPreferredConfig = Bitmap.Config.ARGB_8888;
			options.inSampleSize = 2;
			
			File filePath = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
			File fileItem = new File(filePath, ""+count+".PNG");
			Bitmap bitmap = BitmapFactory.decodeFile(fileItem.getPath(), options);
			return bitmap;
		}
	}
	
	
	public static class Item {
		private Bitmap mBitmap;
		private String mTitle;
		private String mContent;
		private String mUrl;
		private int mCount;

		public Item(){
			
		}
		
		public Item(Bitmap bitmap, String title, String content, int count, String url){
			this.mBitmap = bitmap;
			this.mTitle = title;
			this.mContent = content;
			this.mCount = count;
			this.mUrl = url;
		}
		
		public Bitmap getBitmap() {
			return mBitmap;
		}

		public void setBitmap(Bitmap mBitmap) {
			this.mBitmap = mBitmap;
		}

		public String getTitle() {
			return mTitle;
		}

		public void setTitle(String mTitle) {
			this.mTitle = mTitle;
		}

		public String getContent() {
			return mContent;
		}

		public void setContent(String mContent) {
			this.mContent = mContent;
		}

		public int getCount() {
			return mCount;
		}

		public void setCount(int mCount) {
			this.mCount = mCount;
		}
		
		public void setUrl(String url){
			this.mUrl = url;
		}
		
		public String getUrl(){
			return mUrl;
		}
	}
	

}
