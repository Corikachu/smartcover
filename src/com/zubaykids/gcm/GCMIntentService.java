package com.zubaykids.gcm;

import java.io.ByteArrayOutputStream;

import org.json.JSONException;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.zubaykids.SaveInfo.PartnerInfo;
import com.zubaykids.gcm.SuggestItem.Item;
import com.zubaykids.initapp.SmartCoverMainActivity;
import com.zubaykids.smartcover.R;

/**
 * This {@code IntentService} does the actual handling of the GCM message.
 * {@code GCMBroadcastReceiver} (a {@code WakefulBroadcastReceiver}) holds a
 * partial wake lock for this service while the service does its work. When the
 * service is finished, it calls {@code completeWakefulIntent()} to release the
 * wake lock.
 */
public class GCMIntentService extends IntentService {
	public static final int NOTIFICATION_ID = 1;
	private NotificationManager mNotificationManager;
	private Notification mNoti;
	NotificationCompat.Builder builder;
	public String isKOK;
	public String p_title;
	public String p_content;
	public String p_count;
	
	private final static String TAG_TITLE = "title";
	private final static String TAG_CONTENT = "content";
	private final static String TAG_COUNT = "count";
	private final static String TAG_IMAGE_URL = "img_url";
	private final static String TAG_GO_TO_URL = "ad_url";
	private final static String TAG_IS_K_OK = "isKOK";

    public static final String TAG = "GCM Demo";
	
	public GCMIntentService() {
		super("GCMIntentService");
	}


	@Override
	protected void onHandleIntent(Intent intent) {
		Log.v("onHandleIntent","");

		// 이하 초기 설정
		Bundle extras = intent.getExtras();
		GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
		// The getMessageType() intent parameter must be the intent you received
		// in your BroadcastReceiver.
		String messageType = gcm.getMessageType(intent);
		Log.e("Save Item", "SaveItem");
		// 이하
		if (!extras.isEmpty()) { // has effect of unparcelling Bundle
			/*
			 * Filter messages based on message type. Since it is likely that
			 * GCM will be extended in the future with new message types, just
			 * ignore any message types you're not interested in, or that you
			 * don't recognize.
			 */
			if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR
					.equals(messageType)) {
				sendNotification("Send error: " + extras.toString());
			} else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED
					.equals(messageType)) {

				sendNotification("Deleted messages on server: "
						+ extras.toString());
				// If it's a regular GCM message, do some work.

			} else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE
					.equals(messageType)) {

				// This loop represents the service doing some work.
				getBundle(extras);

				// Post notification of received message.
				// sendNotification("Received: " + extras.toString());
			}
		}
		// Release the wake lock provided by the WakefulBroadcastReceiver.
		GCMBroadcastReceiver.completeWakefulIntent(intent);
	}

	// Put the message into a notification and post it.
	// This is just one simple example of what you might choose to do with
	// a GCM message.

	public String parseIMG_URL(Bundle extras) throws JSONException {
		String img_url = "";
		img_url = extras.getString(TAG_IMAGE_URL);
		isKOK = extras.getString(TAG_IS_K_OK);

		return img_url;
	}

	private void getBundle(Bundle extras) {
		mNotificationManager = (NotificationManager) this
				.getSystemService(Context.NOTIFICATION_SERVICE);
		String img_url = "";
		try {
			img_url = parseIMG_URL(extras);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		if(isKOK==null){
			return;
		}
		
		if (isKOK.equals("0")) {
			p_title = extras.getString(TAG_TITLE);
			p_content = extras.getString(TAG_CONTENT);
			p_count = extras.getString(TAG_COUNT);
			String imageUrl = extras.getString(TAG_GO_TO_URL);
			
			ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
					getApplicationContext()).build();
			ImageLoader imageloader = ImageLoader.getInstance();
			imageloader.init(config);

			Bitmap bitmap = imageloader.loadImageSync(img_url);

			//Save Suggest Item
			SuggestItem.Item item = new Item(bitmap, p_title, p_content, Integer.parseInt(p_count), imageUrl);
			SuggestItem suggest = new SuggestItem();
			suggest.saveItem(item);
			Log.e("Save Item", "SaveItem");
			
			notificationWithBigPicture(GCMIntentService.this, p_title,
					p_content, R.drawable.ic_stat_gcm,
					imageloader.loadImageSync(img_url), SmartCoverMainActivity.class);
		} else if (isKOK.equals("1")) {
	        PartnerInfo.kok = true;

			sendNotification("Received: " + extras.toString());
			
		}
		// 이하 상태바 노티 컨트롤

	}
	
	

	public void notificationWithBigPicture(Context context, String title,
			String message, int icon, Bitmap banner, Class<?> activityClass) {

		Intent intent = new Intent(context, activityClass);
		//
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		banner.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] byteArray = stream.toByteArray();
		intent.putExtra("image", byteArray);
		//

		PendingIntent pendingIntent = PendingIntent.getActivity(context, 0,
				intent, PendingIntent.FLAG_UPDATE_CURRENT);

		NotificationCompat.Builder builder = new NotificationCompat.Builder(
				context).setSmallIcon(icon).setTicker(title)
				.setContentTitle(title).setContentText(message)
				.setAutoCancel(true);

		NotificationCompat.BigPictureStyle style = new NotificationCompat.BigPictureStyle();
		style.bigPicture(banner);

		builder.setStyle(style);
		builder.setContentIntent(pendingIntent);

		builder.setDefaults(Notification.DEFAULT_VIBRATE);
		builder.setSound(RingtoneManager
				.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(NOTIFICATION_ID, builder.build());
	}

	// Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.
	private void sendNotification(String msg) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, SmartCoverMainActivity.class), 0);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
        .setSmallIcon(R.drawable.heart)
        .setContentTitle("하트를 받았습니다.")
        .setStyle(new NotificationCompat.BigTextStyle()
        .bigText("하트를 받았습니다."))
        .setContentText("하트를 받았습니다.");
        
        Toast.makeText(getApplicationContext(), "하트를 받았습니다.", Toast.LENGTH_LONG).show();

        Vibrator vibrator = (Vibrator)getSystemService(Context.VIBRATOR_SERVICE);
        long[] vibratePattern = {200, 200, 200, 200};
        
        vibrator.vibrate(vibratePattern, -1);
        
        
        mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }
    
}
