package com.zubaykids.msg;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.animation.BounceInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.ImageButton;
import android.widget.Toast;

import com.zubaykids.smartcover.R;

public class MainActivity extends Activity {
	private BadgeView badge;
	private ImageButton imgBtn;
	private TranslateAnimation anim;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.msg_main);

		imgBtn = (ImageButton)findViewById(R.id.MsgBtn);
		
		Intent intent = getIntent();
		Toast.makeText(this, "wow", Toast.LENGTH_SHORT).show();
		
		InitBadge();
	}


	private void InitBadge() {
		View target = findViewById(R.id.MsgBtn);
		badge = new BadgeView(getApplicationContext(), target);
		badge.setText("N");
		badge.setBadgePosition(2);
		anim = new TranslateAnimation(0, 0, 100, 0);
        anim.setInterpolator(new BounceInterpolator());
        anim.setDuration(1000);
        badge.show();
         
	}


	public void btnPressed(View view)
	{
		badge.hide();
		Intent msgIntent= new Intent(getApplicationContext(),SecureMessagesActivity.class);
		startActivity(msgIntent);
		
	}
}
