package com.zubaykids.msg;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.telephony.SmsMessage;
import android.widget.Toast;

import com.zubaykids.SaveInfo.PartnerInfo;
import com.zubaykids.bottomAnimation.moveRect.RectAnimation;
import com.zubaykids.initapp.SmartCoverMainActivity;
import com.zubaykids.initapp.ZubayApplication;
import com.zubaykids.smartcover.R;

public class SmsReceiver extends BroadcastReceiver 
{
	String MESSAGE_BADGE = com.zubaykids.initapp.ZubayApplication.MESSAGE_BADGE;
	Handler mHandler;
	
	public static boolean is = false;
	// All available column names in SMS table
	// [_id, thread_id, address, 
	// person, date, protocol, read, 
	// status, type, reply_path_present, 
	// subject, body, service_center, 
	// locked, error_code, seen]

	public static final String SMS_EXTRA_NAME = "pdus";
	public static final String SMS_URI = "content://sms";

	public static final String ADDRESS = "address";
	public static final String PERSON = "person";
	public static final String DATE = "date";
	public static final String READ = "read";
	public static final String STATUS = "status";
	public static final String TYPE = "type";
	public static final String BODY = "body";
	public static final String SEEN = "seen";

	public static final int MESSAGE_TYPE_INBOX = 1;
	public static final int MESSAGE_TYPE_SENT = 2;

	public static final int MESSAGE_IS_NOT_READ = 0;
	public static final int MESSAGE_IS_READ = 1;

	public static final int MESSAGE_IS_NOT_SEEN = 0;
	public static final int MESSAGE_IS_SEEN = 1;

	// Change the password here or give a user possibility to change it
	public static final byte[] PASSWORD = new byte[]{ 0x20, 0x32, 0x34, 0x47, (byte) 0x84, 0x33, 0x58 };

	public void onReceive( Context context, Intent intent ) 
	{
		SharedPreferences mPref = PreferenceManager.getDefaultSharedPreferences(context);
		SharedPreferences.Editor editor = mPref.edit();
		editor.putBoolean("MESSAGE_BADGE", true);
		editor.commit();

		PartnerInfo info = new PartnerInfo();
		info.setOutMessageCount(info.getOutMessageCount() + 1);
		
		final Handler handler = SmartCoverMainActivity.mHandler;
		handler.post(new Runnable() {
			
			@Override
			public void run() {
				RectAnimation mainLED = SmartCoverMainActivity.mainLED;
				mainLED.start();
			}
		});
	}

	private void ringtone(Context context)
	{
		Toast.makeText( context, "ringtone 실행", Toast.LENGTH_SHORT ).show();

		RingtoneManager rm = new RingtoneManager(context);
		Cursor cursor;
		rm.setType(RingtoneManager.TYPE_NOTIFICATION);
		cursor = rm.getCursor(); // 알람 사운드 리스트 가져오기
		Uri ringtoneUri = RingtoneManager.getActualDefaultRingtoneUri(
				context,RingtoneManager.TYPE_NOTIFICATION);
		Ringtone ringtone = RingtoneManager.getRingtone(context, ringtoneUri);
		ringtone.play();
	}

	private void putSmsToDatabase( ContentResolver contentResolver, SmsMessage sms )
	{
		// Create SMS row
		ContentValues values = new ContentValues();
		values.put( ADDRESS, sms.getOriginatingAddress() );
		values.put( DATE, sms.getTimestampMillis() );
		values.put( READ, MESSAGE_IS_NOT_READ );
		values.put( STATUS, sms.getStatus() );
		values.put( TYPE, MESSAGE_TYPE_INBOX );
		values.put( SEEN, MESSAGE_IS_NOT_SEEN );
		try
		{
			String encryptedPassword = StringCryptor.encrypt( new String(PASSWORD), sms.getMessageBody().toString() ); 
			values.put( BODY, encryptedPassword );
		}
		catch ( Exception e ) 
		{ 
			e.printStackTrace(); 
		}

		// Push row into the SMS table
		contentResolver.insert( Uri.parse( SMS_URI ), values );
	}
}
