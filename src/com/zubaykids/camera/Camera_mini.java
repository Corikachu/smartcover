package com.zubaykids.camera;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.zubaykids.smartcover.R;


public class Camera_mini extends Activity {
	// Log용 TAG
	private static final String TAG = "HelloCamera";
	// ImageView 설정 이미지 약하게 크기. 1 / 8의 크기로 처리
	private static final int IN_SAMPLE_SIZE = 1;
	
	private SurfaceView surface;
	private SurfaceHolder holder;
	
	ImageView camera_icon;
	ImageView camera_switch;
	ImageButton back_button;
	
	int is_camera_switch = 0;

	// 카메라 제어
	private Camera mCamera;
	// 촬영 사진보기
	private ImageView mImage;
	// 처리중 신고
	private boolean mInProgress;

	private Display mDisplay;
	
	private File file;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);	// block name bar
		setContentView(R.layout.camera_mini);

		surface = (SurfaceView) findViewById(R.id.surface_view);
		holder = surface.getHolder();

		// SurfaceView 리스너를 등록
		holder.addCallback(mSurfaceListener);

		// 셔터의 버튼에 리스너를 등록
		camera_icon = (ImageView)findViewById(R.id.shutter);
		camera_icon.setOnClickListener(mButtonListener);

		// register AutoFocusCallback on SurfaceView
		surface.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				mCamera.autoFocus(new Camera.AutoFocusCallback() {

					@Override
					public void onAutoFocus(boolean success, Camera camera) {
						// TODO Auto-generated method stub

					}
				});				
			}
		});

		back_button = (ImageButton)findViewById(R.id.back_button);
		back_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {			
				
				finish();
			}
		});
		
		camera_switch = (ImageView)findViewById(R.id.camera_switch);
		camera_switch.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				surface.setVisibility(View.GONE);
				
				if(is_camera_switch == 0) is_camera_switch = 1;
				else is_camera_switch = 0;
				
				surface.setVisibility(View.VISIBLE);
				
				
				
			}
		});


	}

	// 카메라 미리보기 SurfaceView의 리스너
	private SurfaceHolder.Callback mSurfaceListener = new SurfaceHolder.Callback()
	{
		public void surfaceCreated(SurfaceHolder holder)
		{
			// SurfaceView가 생성되면 카메라를 열
			
			Log.i(TAG, "Camera opened");
			// SDK1.5에서 setPreviewDisplay이 IO Exception을 throw한다
			try
			{
				mCamera = Camera.open(is_camera_switch);
				mCamera.setPreviewDisplay(holder);
				mCamera.setDisplayOrientation(90);
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}

		public void surfaceDestroyed(SurfaceHolder holder)
		{
			// SurfaceView가 삭제되는 시간에 카메라를 개방
			mCamera.release();
			mCamera = null;
			Log.i(TAG, "Camera released");
		}

		public void surfaceChanged(SurfaceHolder holder, int format, int width, int height)
		{
			// 미리보기 크기를 설정
			Camera.Parameters parameters = mCamera.getParameters();

			// 프리뷰 사이즈를 카메라 해상도에 맞게 설정
			int resWidth = mCamera.getParameters().getPictureSize().width;
			int resHeight = mCamera.getParameters().getPictureSize().height;
			parameters.setPreviewSize(resWidth, resHeight);

			mCamera.setParameters(parameters);
			mCamera.setDisplayOrientation(90);

			mCamera.startPreview();
			Log.i(TAG, "Camera preview started");
		}
	};

	// 버튼을 눌렀을 때 수신기
	private View.OnClickListener mButtonListener = new View.OnClickListener()
	{
		public void onClick(View v)
		{
			if (mCamera != null && mInProgress == false)
			{
				// 이미지 검색을 시작한다. 리스너 설정
				mCamera.takePicture(mShutterListener, // 셔터 후
						null, // Raw 이미지 생성 후
						mPicutureListener); // JPE 이미지 생성 후
				mInProgress = true;
			}
		}
	};

	private Camera.ShutterCallback mShutterListener = new Camera.ShutterCallback()
	{
		// 이미지를 처리하기 전에 호출된다.
		public void onShutter()
		{
			// 셔터 소리 재생 코드 생략
			Log.i(TAG, "onShutter");
		}

	};

	// JPEG 이미지를 생성 후 호출
	private Camera.PictureCallback mPicutureListener = new Camera.PictureCallback()
	{
		public void onPictureTaken(byte[] data, Camera camera)
		{
			if (data != null)
			{
				// 처리하는 이미지의 크기를 축소
				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inSampleSize = IN_SAMPLE_SIZE;
				Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length, options);
				bitmap = bitmapRotateResize(bitmap);
				// 이미지 뷰 이미지 설정
				//mImage.setImageBitmap(bitmap);
				// 정지된 프리뷰를 재개

				String mRootPath = Environment.getExternalStorageDirectory().getAbsolutePath()+"/DalSum";
				Calendar calendar = Calendar.getInstance();
				String FileName = String.format("DalSum%02d%02d%02d_%02d%02d%02d.jpg", 
						calendar.get(Calendar.YEAR) % 100, calendar.get(Calendar.MONTH)+1, 
						calendar.get(Calendar.DAY_OF_MONTH), calendar.get(Calendar.HOUR_OF_DAY), 
						calendar.get(Calendar.MINUTE), calendar.get(Calendar.SECOND));

				SaveBitmapToFileCache(bitmap, mRootPath, FileName);

				camera.startPreview();
				// 처리중 플래그를 떨어뜨림
				mInProgress = false;
			}
		}

	};

	Bitmap bitmapRotateResize(Bitmap bitmap) {
		int width = bitmap.getWidth();
		int height= bitmap.getHeight();

		Matrix matrix = new Matrix(); 
		Bitmap resizeBitmap;
		
		if(is_camera_switch == 0) {
			matrix.postRotate(90);
			resizeBitmap = Bitmap.createBitmap(bitmap, 0, 0, width*3/5, height, matrix, true);
		}
		else {
			matrix.postRotate(270);
			resizeBitmap = Bitmap.createBitmap(bitmap, width*2/5, 0, width*3/5, height, matrix, true);
		}

		

		return resizeBitmap;
	}

	public void SaveBitmapToFileCache(Bitmap bitmap, String strFilePath,
			String filename) {

		File file = new File(strFilePath);

		// If no folders
		if (!file.exists()) {
			file.mkdirs();
		}

		File fileCacheItem = new File(strFilePath + "/" + filename);
		OutputStream out = null;

		try {
			fileCacheItem.createNewFile();
			out = new FileOutputStream(fileCacheItem);

			bitmap.compress(CompressFormat.JPEG, 100, out);

			} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		File scanf_file = new File("storage/sdcard0/DalSum/" + filename);
		SingleMediaScanner scanner = new SingleMediaScanner(getApplicationContext(), scanf_file);
	}
}
