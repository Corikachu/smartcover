package com.zubaykids.call;

import java.lang.reflect.Method;

import android.app.Activity;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import com.android.internal.telephony.ITelephony;
import com.zubaykids.initapp.ZubayApplication;

public class OnSeekBarChangeListener_Reject implements OnSeekBarChangeListener {
	private Context context;
	private Activity activity;
	
	//CallActivity activity = (CallActivity)CallActivity.activity;

	public OnSeekBarChangeListener_Reject(Activity activity) {
		this.context = ZubayApplication.getInstance().getApplicationContext();
		this.activity = activity;
	}
	
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		if (seekBar.getProgress() == 100) {
			endCall();
			activity.finish();
		} else {
			seekBar.setProgress(0);
		}
	}

    public void endCall()
    {
    	TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
    	try {
    		Class<?> c = Class.forName(tm.getClass().getName());
    		Method m = c.getDeclaredMethod("getITelephony");
    		m.setAccessible(true);
    		ITelephony telephonyService = (ITelephony)m.invoke(tm);
    		telephonyService.endCall();
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    }
}