package com.zubaykids.call;

import android.view.MotionEvent;
import android.view.View;
import android.widget.SeekBar;

public class OnTouchListener implements android.view.View.OnTouchListener {
	
	SeekBar seekBar;
	
	OnTouchListener(SeekBar seekBar) {
		this.seekBar = seekBar;
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if(event.getAction() == MotionEvent.ACTION_UP) {
			seekBar.setProgress(0);
		}
		return false;
	}
}