package com.zubaykids.call;

import java.lang.reflect.Method;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.internal.telephony.ITelephony;
import com.zubaykids.SaveInfo.PartnerInfo;
import com.zubaykids.smartcover.R;

public class CallingActivity extends Activity {

	TextView name_TextView;
	TextView number_TextView;
	TextView time_TextView;
	ImageView picture_ImageView;
	Button endcall_Button;

	String name;
	String number;
	int calling_time = 0;

	Handler mHandler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.calling_activity);

		PartnerInfo.activity = CallingActivity.this;

		Intent intent = getIntent();
		number = intent.getStringExtra("NUMBER");
		name = intent.getStringExtra("NAME");

		connectView();

		name_TextView.setText(name);
		number_TextView.setText(number);

		runClock();

		endcall_Button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				endCall();
				finish();

				try{
					Thread.sleep(2000);
				}
				catch(Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	void connectView() {
		name_TextView = (TextView)findViewById(R.id.name);
		number_TextView = (TextView)findViewById(R.id.number);
		time_TextView = (TextView)findViewById(R.id.time);
		picture_ImageView = (ImageView)findViewById(R.id.picture);
		endcall_Button = (Button)findViewById(R.id.endcall);
	}

	void runClock() {

		// 시계 구현
		mHandler = new Handler();

		new Thread() {
			@Override
			public void run() {
				while(true) {
					mHandler.post(new Runnable() {


						@Override
						public void run() {
							calling_time ++;
							time_TextView.setText(changeTime(calling_time));
						}
					});

					try {
						Thread.sleep(1000);
					}
					catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
	}

	public void endCall()
	{
		TelephonyManager tm = (TelephonyManager)getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
		try {
			Class<?> c = Class.forName(tm.getClass().getName());
			Method m = c.getDeclaredMethod("getITelephony");
			m.setAccessible(true);
			ITelephony telephonyService = (ITelephony)m.invoke(tm);
			telephonyService.endCall();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	String changeTime(int time) {
		int hour;
		int minute;
		int second;
		String hour_s;
		String minute_s;
		String second_s;

		hour = time /3600;    	
		minute = time / 60;
		second = time % 60;

		if(hour < 10) {
			hour_s = "0" + hour;
		}
		else {
			hour_s = "" + hour;
		}
		if(minute < 10) {
			minute_s = "0" + minute;
		}
		else {
			minute_s = "" + minute;
		}
		if(second < 10) {
			second_s = "0" + second;
		}
		else {
			second_s = "" + second;
		}

		if(hour == 0) {
			return minute_s + ":" + second_s;
		}
		else {
			return hour_s + ":" + minute_s + ":" + second_s;
		}
	}
}
