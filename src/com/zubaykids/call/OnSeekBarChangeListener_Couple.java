package com.zubaykids.call;

import java.lang.reflect.Method;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.android.internal.telephony.ITelephony;
import com.zubaykids.initapp.ZubayApplication;

public class OnSeekBarChangeListener_Couple implements OnSeekBarChangeListener {
	private Context context;
	//CallActivity activity = (CallActivity)CallActivity.activity;
	private Activity activity;
	private String name;
	private String number;

	public OnSeekBarChangeListener_Couple(Activity activity) {
		this.context = ZubayApplication.getInstance().getApplicationContext();
		this.activity = activity;
	}
	
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		if (seekBar.getProgress() == 0) {
            call();
        	activity.finish();
        	
        	Intent calling = new Intent(activity.getApplicationContext(), CallingActivity.class);
        	calling.putExtra("NUMBER", number);
        	calling.putExtra("NAME", name);
        	activity.startActivity(calling);
		} else if(seekBar.getProgress() == 100){
			EndCall();
			activity.finish();
		} else {
			seekBar.setProgress(50);
		}
	}
	
    public void call()
    {
    	try {
    		Intent new_intent = new Intent(Intent.ACTION_MEDIA_BUTTON);
    		new_intent.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_HEADSETHOOK));
    		context.sendOrderedBroadcast(new_intent, null);
    		new_intent = null;
    	} catch (Exception e) {
    		e.printStackTrace();
    	}

    	try {
    		Intent buttonUp = new Intent(Intent.ACTION_MEDIA_BUTTON);
    		buttonUp.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_HEADSETHOOK));
    		context.sendOrderedBroadcast(buttonUp, null);
    		buttonUp = null;
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
    
    public void EndCall()
    {
    	TelephonyManager tm = (TelephonyManager)context.getSystemService(Context.TELEPHONY_SERVICE);
    	try {
    		Class<?> c = Class.forName(tm.getClass().getName());
    		Method m = c.getDeclaredMethod("getITelephony");
    		m.setAccessible(true);
    		ITelephony telephonyService = (ITelephony)m.invoke(tm);
    		telephonyService.endCall();
    	} catch(Exception e) {
    		e.printStackTrace();
    	}
    }
    
    void setNameNumber(String name, String number) {
    	this.name = name;
    	this.number = number;
    }
}