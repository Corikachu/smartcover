package com.zubaykids.call;

import java.io.InputStream;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.zubaykids.SaveInfo.PartnerInfo;
import com.zubaykids.bottomAnimation.moveRect.RectAnimation;
import com.zubaykids.initapp.view.RoundedImageView;
import com.zubaykids.smartcover.R;

public class CoupleCallActivity extends Activity {
	
	String number;
	String name;
	RoundedImageView image;
	SharedPreferences mPref;
	SeekBar seekbar1;
	Drawable drawable;
	RelativeLayout coupleCall_layout;
	
	RectAnimation coupleCallLED;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	    		   WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.couple_call_activity);
		
		coupleCall_layout = (RelativeLayout)findViewById(R.id.coupleCall_layout);
		mPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		
		setBackground();
		
		coupleCallLED = (RectAnimation)findViewById(R.id.cover_couplecall_led_animation);
		coupleCallLED.setRepeatCount(0);
		mPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
/*		seekbar1 = (SeekBar)findViewById(R.id.seekBar1);
		
		drawable = getResources().getDrawable(R.drawable.default_profile_girl);
		seekbar1.setThumb(drawable);
		
		if(mPref.getString("SEX", "0").equals("1")) {
			drawable = getResources().getDrawable(R.drawable.default_profile_man);
			seekbar1.setThumb(drawable);
		}*/
		
		PartnerInfo.activity = CoupleCallActivity.this;
		
		Intent intent = getIntent();
		number = intent.getStringExtra("number");
		name = intent.getStringExtra("name");
		
		TextView name_textView = (TextView)findViewById(R.id.coupleName);
		TextView phone_textView = (TextView)findViewById(R.id.couplePhone);
		image = (RoundedImageView)findViewById(R.id.coupleImage);
		image.setBorderWidth(15);
		image.setBorderColor("#FFCDCF");
		image.addShadow();
		
		if(mPref.getString("SEX", "0").equals("1")) {
			image.setBorderColor("#a9e9ee");
			image.setImageResource(R.drawable.default_profile_man);
		}

		try{
			InputStream image_is = openFileInput("PARTNER_PICTURE");
			Bitmap image_b = BitmapFactory.decodeStream(image_is);
			Drawable image_drawable = new BitmapDrawable(image_b);
			image.setImageDrawable(image_drawable);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		name_textView.setText(name);
		phone_textView.setText(number);
		
		SeekBar seekBar1 = (SeekBar)findViewById(R.id.seekBar1);
		OnSeekBarChangeListener_Couple couple = new OnSeekBarChangeListener_Couple(this);
		couple.setNameNumber(name, number);
		seekBar1.setOnSeekBarChangeListener(couple);
		seekBar1.setProgress(50);
		seekBar1.setOnTouchListener(new OnTouchListener(seekBar1));

	}
	
	void setNumber(String number) {
		this.number = number;
	}
	
	void setName(String name) {
		this.name = name;
	}
	
	@Override
	public void onBackPressed() {
		// block back button
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// block menu button
		return false;
	}
	
	void setBackground() {
		coupleCall_layout.setBackgroundResource(R.drawable.default_background_man);

		if(mPref.getString("SEX", "0").equals("1")) {
			coupleCall_layout.setBackgroundResource(R.drawable.default_background_girl);
		}
		
		// background 설정
		try {
			InputStream is = openFileInput("BACKGROUND_PICTURE");
			Bitmap b = BitmapFactory.decodeStream(is);
			Drawable drawable = new BitmapDrawable(b);

			coupleCall_layout.setBackground(drawable);				
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
