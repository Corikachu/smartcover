package com.zubaykids.call;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

import com.zubaykids.initapp.ZubayApplication;

public class OnSeekBarChangeListener_Call implements OnSeekBarChangeListener {
	private Context context;
	private Activity activity;
	private String name;
	private String number;

	public OnSeekBarChangeListener_Call(Activity activity) {
		this.context = ZubayApplication.getInstance().getApplicationContext();
		this.activity = activity;
	}
	
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress,
			boolean fromUser) {
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		if (seekBar.getProgress() == 100) {
            call();
        	activity.finish();
        	
        	Intent calling = new Intent(activity.getApplicationContext(), CallingActivity.class);
        	calling.putExtra("NUMBER", number);
        	calling.putExtra("NAME", name);
        	activity.startActivity(calling);
		} else {
			seekBar.setProgress(0);
		}
	}
	
    public void call()
    {
    	try
    	{
    		Intent new_intent = new Intent(Intent.ACTION_MEDIA_BUTTON);
    		new_intent.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_HEADSETHOOK));
    		context.sendOrderedBroadcast(new_intent, null);
    		new_intent = null;
    	} catch (Exception e)
    	{
    		e.printStackTrace();
    	}

    	try
    	{
    		Intent buttonUp = new Intent(Intent.ACTION_MEDIA_BUTTON);
    		buttonUp.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_HEADSETHOOK));
    		context.sendOrderedBroadcast(buttonUp, null);
    		buttonUp = null;
    	} catch (Exception e)
    	{
    		e.printStackTrace();
    	}
    }
    
    void setNameNumber(String name, String number) {
    	this.name = name;
    	this.number = number;
    }
}