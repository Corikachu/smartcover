package com.zubaykids.call;

import java.util.Calendar;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.provider.ContactsContract;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.zubaykids.SaveInfo.PartnerInfo;
import com.zubaykids.coupleIndex.CoupleIndex;

public class CallPhoneStateListener extends PhoneStateListener{

	Cursor cursor;
	Context context;
	int call_flag = 0;
	String couplePhoneNumber = new PartnerInfo().getPartnerPhone();
	String callNumber;
	public static String callName;
	String number;
	String name;
	String CALL_BADGE = com.zubaykids.initapp.ZubayApplication.CALL_BADGE;
	int[] phoneState = {-1, -1};
	public static boolean isOutCall = false;

	public void setContext(Context context) {
		this.context = context;
	}

	@Override
	public void onCallStateChanged(int state, String incomingNumber) {

		Log.v("callstate", "start");

		switch (state) {
		case TelephonyManager.CALL_STATE_IDLE:

			Log.v("callstate", "idle");

			phoneState[0] = phoneState[1];
			phoneState[1] = TelephonyManager.CALL_STATE_IDLE;
			Log.v("callstate", "" + phoneState[0] + " " + phoneState[1]);
			if(phoneState[0] == TelephonyManager.CALL_STATE_RINGING) {

				Log.v("callstate", "add");

				PartnerInfo.activity.finish();

				SharedPreferences mPref = PreferenceManager.getDefaultSharedPreferences(context);
				SharedPreferences.Editor editor = mPref.edit();
				editor.putBoolean(CALL_BADGE, true);
				editor.commit();
				PartnerInfo info = new PartnerInfo();

				info.setOutCallCount(info.getOutCallCount() + 1);
			}

			callName="";
			callNumber="";
			//save Endcall Time to calculate CoupleIndex.
			CoupleIndex coupleIndex = new CoupleIndex();
			coupleIndex.setEndCallTime(Calendar.getInstance().getTimeInMillis());
			break;
		case TelephonyManager.CALL_STATE_OFFHOOK:	// ��ȭ�� �Ծ��� ��

			Log.v("callstate", "offhook");

			phoneState[0] = phoneState[1];
			phoneState[1] = TelephonyManager.CALL_STATE_OFFHOOK;

			Log.v("callstate", "" + phoneState[0] + " " + phoneState[1]);

			break;
		case TelephonyManager.CALL_STATE_RINGING:	// ��ȭ�� ���� ��

			Log.v("callstate", "ringing");

			phoneState[0] = phoneState[1];
			phoneState[1] = TelephonyManager.CALL_STATE_RINGING;

			Log.v("callstate", "" + phoneState[0] + " " + phoneState[1]);

			callNumber = incomingNumber;	// insert calling number

			cursor = context.getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);

			if(cursor.moveToFirst()) {
				do {
					int phone_idx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
					int name_idx = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);

					number = cursor.getString(phone_idx);	// get calling phone number
					name = cursor.getString(name_idx);	// get calling name

					number = deletebar(number);	// delete "-"

					if(callNumber.equals(number)) {
						callName = name;
					}
				} while(cursor.moveToNext());
			}

			if(couplePhoneNumber.equals(callNumber)) {

				final Intent callActivityIntent = new Intent(context, CoupleCallActivity.class);	// callactivity ���
				callActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);		//
				callActivityIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
				callActivityIntent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
				//callActivityIntent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);

				callActivityIntent.putExtra("number", callNumber);
				callActivityIntent.putExtra("name", callName);

				new Runnable() {

					@Override
					public void run() {
						try {
							Thread.sleep(400);
							context.startActivity(callActivityIntent);
						} catch (InterruptedException e) {
							context.startActivity(callActivityIntent);
						}
					}
				}.run();;
			}
			else {

				final Intent callActivityIntent = new Intent(context, CallActivity.class);	// callactivity ���
				callActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);		//
				callActivityIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
				callActivityIntent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
				//callActivityIntent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);

				callActivityIntent.putExtra("NUMBER", callNumber);
				callActivityIntent.putExtra("NAME", callName);

				new Runnable() {

					@Override
					public void run() {
						try {
							Thread.sleep(400);
							context.startActivity(callActivityIntent);
						} catch (InterruptedException e) {
							context.startActivity(callActivityIntent);
						}
					}
				}.run();;
			}

			break;
		default:
			//Log.i(TAG, "CallPhoneStateListener->onCallStateChanged() -> default -> " + Integer.toString(state));
			break;
		}
	}

	String deletebar(String number) {
		StringBuffer sb = new StringBuffer();

		for(int i=0; i<number.length(); i++) {
			if(Character.isDigit(number.charAt(i))) {
				sb.append(number.charAt(i));
			}
		}

		return sb.toString();
	}
}