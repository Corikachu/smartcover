package com.zubaykids.call;

import java.io.InputStream;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.zubaykids.SaveInfo.PartnerInfo;
import com.zubaykids.bottomAnimation.moveRect.RectAnimation;
import com.zubaykids.smartcover.R;

public class CallActivity extends Activity {
	
	String number;
	String name;
	RelativeLayout call_layout;
	SharedPreferences mPref;
	
	RectAnimation CallLED;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	    		   WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.call_activity);
		
		CallLED = (RectAnimation)findViewById(R.id.cover_call_led_animation);
		CallLED.setRepeatCount(0);
		
		call_layout = (RelativeLayout)findViewById(R.id.call_layout);
		mPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		
		setBackground();
		
		PartnerInfo.activity = CallActivity.this;
		
		Intent intent = getIntent();
		number = intent.getStringExtra("NUMBER");
		name = intent.getStringExtra("NAME");
		
		TextView textView1 = (TextView)findViewById(R.id.textView1);
		TextView textView2 = (TextView)findViewById(R.id.textView2);
		ImageView image = (ImageView)findViewById(R.id.image);
		
		textView1.setText(name);
		textView2.setText(number);
		
		image.setImageResource(R.drawable.default_picture);
		
		SeekBar seekBar1 = (SeekBar)findViewById(R.id.seekBar1);
		OnSeekBarChangeListener_Call call = new OnSeekBarChangeListener_Call(this);
		call.setNameNumber(name, number);
		seekBar1.setOnSeekBarChangeListener(call);
		seekBar1.setOnTouchListener(new OnTouchListener(seekBar1));
		
		
		SeekBar seekBar2 = (SeekBar)findViewById(R.id.seekBar2);
		seekBar2.setOnSeekBarChangeListener(new OnSeekBarChangeListener_Reject(this));
		seekBar2.setOnTouchListener(new OnTouchListener(seekBar2));

	}
	
	void setNumber(String number) {
		this.number = number;
	}
	
	void setName(String name) {
		this.name = name;
	}
	
	@Override
	public void onBackPressed() {
		// block back button
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// block menu button
		return false;
	}
	
	void setBackground() {
		call_layout.setBackgroundResource(R.drawable.default_background_man);

		if(mPref.getString("SEX", "0").equals("1")) {
			call_layout.setBackgroundResource(R.drawable.default_background_girl);
		}
		
		// background 설정
		try {
			InputStream is = openFileInput("BACKGROUND_PICTURE");
			Bitmap b = BitmapFactory.decodeStream(is);
			Drawable drawable = new BitmapDrawable(b);

			call_layout.setBackground(drawable);				
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}
