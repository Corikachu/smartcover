package com.zubaykids.SaveInfo;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.zubaykids.bottomAnimation.moveRect.RectAnimation;
import com.zubaykids.initapp.ZubayApplication;

public class PartnerInfo {
	private String birth;
	private String star;
	private String bloodType;
	private String myPhone;
	private String partnerPhone;
	private String city;
	private String sex;
	private int outCallCount;
	private int outMessageCount;
	public static boolean kok = false;
	public static boolean callBroad = false;
	public static boolean isLED = false;

	public static Activity activity;
	
	private SharedPreferences sp;
	private SharedPreferences.Editor editor;
	private Context context;
	
	public PartnerInfo(){
		context = ZubayApplication.getInstance().getApplicationContext();
		sp = PreferenceManager.getDefaultSharedPreferences(context);
		editor = sp.edit();
		
//		loadData();
	}
	
	/*private void loadData(){
		birth = sp.getString("BIRTH", "19900101");
		star = sp.getString("STAR", "Aquarius");
		bloodType = sp.getString("BLOODTYPE", "A");
		myPhone = sp.getString("MYPHONENUMBER", "01012345678");
		partnerPhone = sp.getString("PARTNERPHONENUMBER", "01012345678");
		city = sp.getString("CITY", "서울");
		sex = sp.getString("SEX", "남");
		outCallCount = sp.getInt("OUTCALLCOUNT", 0);
		outMessageCount = sp.getInt("OUTMESSAGECOUNT", 0);
	}*/
	
	
	public String getBirth() {
		return sp.getString("BIRTH", "19910101");
	}
	public void setBirth(String birth) {
		editor.putString("BIRTH", birth);
		editor.commit();
	}
	public String getStar() {
		return sp.getString("STAR", "Aquarius");
	}
	public void setStar(String star) {
		editor.putString("STAR", star);
		editor.commit();
	}
	public String getBloodType() {
		return sp.getString("BLOODTYPE", "A");
	}
	public void setBloodType(String bloodType) {
		editor.putString("BLOODTYPE", bloodType);
		editor.commit();
	}
	public String getMyPhone() {
		return sp.getString("MYPHONENUMBER", "01012345678");
	}
	public void setMyPhone(String myPhone) {
		editor.putString("MYPHONE", myPhone);
		editor.commit();
	}
	public String getPartnerPhone() {
		return sp.getString("PARTNERPHONENUMBER", "01012345678");
	}
	public void setPartnerPhone(String partnerPhone) {
		editor.putString("PARTNERPHONE", partnerPhone);
		editor.commit();
	}
	public String getCity() {
		return sp.getString("CITY", "서울");
	}
	public void setCity(String city) {
		editor.putString("CITY", city);
		editor.commit();
	}
	public String getSex() {
		return sp.getString("SEX", "남");
	}
	public void setSex(String sex) {
		editor.putString("SEX", sex);
		editor.commit();
	}
	public int getOutCallCount() {
		return sp.getInt("OUTCALLCOUNT", 0);
	}
	public void setOutCallCount(int outCallCount) {
		editor.putInt("OUTCALLCOUNT", outCallCount);
		editor.commit();
	}
	public int getOutMessageCount() {
		return sp.getInt("OUTMESSAGECOUNT", 0);
	}
	public void setOutMessageCount(int outMessageCount) {
		editor.putInt("OUTMESSAGECOUNT", outMessageCount);
		editor.commit();
	}
}
