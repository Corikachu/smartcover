package com.zubaykids.bottomAnimation.gradientView;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class GradientView extends LinearLayout{

	public GradientView(Context context) {
		super(context);
	}

	public GradientView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public GradientView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
	}
	
}
