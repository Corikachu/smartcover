package com.zubaykids.bottomAnimation.moveRect;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.LinearLayout;

public class RectAnimation extends LinearLayout{

	public int x, y;
	private Paint mPaint;
	
	private int alpha = 1;
	private int speed = 10;
	private int speedDirction = 1;
	private boolean start = true;
	private int repeatCount = 0;
	private int limitRepeatCount = 5;
	private static float px;
	
	public RectAnimation(Context context) {
		super(context);
		setPaint();
	}

	public RectAnimation(Context context, AttributeSet attrs) {
		super(context, attrs);
		setPaint();
	}

	public RectAnimation(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		setPaint();
	}
	
	public void setPaint(){
		setWillNotDraw(false);
		mPaint = new Paint();
		mPaint.setAntiAlias(true);
		mPaint.setDither(true);
		px = getResources().getDisplayMetrics().density;
	}
	
	public void setRectColor(String color) {
		if(mPaint != null){
			mPaint.setColor(Color.parseColor(color));
		}
	}
	
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	
	/*
	 * Set Repeat Number
	 * 0 is infinite.
	 */
	public void setRepeatCount(int limitRepeatCount){
		this.limitRepeatCount = limitRepeatCount;
	}
	
	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		
		thread.run();
		
		if(repeatCount >= limitRepeatCount){
			if(limitRepeatCount != 0){
				stop();
			}
		}
		
		if(!start){
			return;
		}
		
		int height = 15;
		
		int x= 30;
		mPaint.setColor(Color.parseColor("#ff00ff"));
		mPaint.setAlpha(alpha);
		canvas.drawRect(makeRect(x, height), mPaint);
		
		mPaint.setColor(Color.parseColor("#A9E9EE"));
		mPaint.setAlpha(alpha);
		canvas.drawRect(makeRect(x+50, height), mPaint);
		
		mPaint.setColor(Color.parseColor("#FAEEAC"));
		mPaint.setAlpha(alpha);
		canvas.drawRect(makeRect(x+100, height), mPaint);
		
		mPaint.setColor(Color.parseColor("#FFC581"));
		mPaint.setAlpha(alpha);
		canvas.drawRect(makeRect(x+150, height), mPaint);
		
		mPaint.setColor(Color.parseColor("#FFA9AC"));
		mPaint.setAlpha(alpha);
		canvas.drawRect(makeRect(x+200, height), mPaint);
		
		mPaint.setColor(Color.parseColor("#FFCDCF"));
		mPaint.setAlpha(alpha);
		canvas.drawRect(makeRect(x+250, height), mPaint);
		
		
		alpha += speed*speedDirction;
		
		if(alpha<=0){
			alpha = 0;
			speedDirction *= -1;
			repeatCount++;
		}else if(alpha >=255){
			alpha = 255;
			speedDirction *= -1;
		}
		
		
	}
	
	public void start(){
		start = true;
		alpha = 0;
		repeatCount = 0;
	}
	
	public void stop(){
		start = false;
		alpha = 0;
		repeatCount = 0;
	}
	
	public static Rect makeRect(int x, int y){
		
		Rect output = new Rect();
		output.set((int)(x*px), (int)(y*px), (int)((x+50)*px), (int)((y+100)*px));

		return output;
	}
	
	private Runnable thread = new Runnable() {
		
		@Override
		public void run() {
			try{
				Thread.sleep(1);
			}catch(Exception e){
				e.printStackTrace();
			}
			invalidate();
		}
	};
}
