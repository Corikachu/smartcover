package com.zubaykids.setting;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.zubaykids.initapp.ZubayApplication;
import com.zubaykids.smartcover.R;
import com.zubaykids.weather.WeatherService;

public class SelectCity extends Activity {
	
	private ListView mListView;
	private ArrayAdapter<String> mArrayAdapter;
	private ArrayList<String> itemArray;
	private Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
	    		   WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_setting);

		mContext = ZubayApplication.getInstance().getApplicationContext();
		mListView = (ListView) findViewById(R.id.setting_listview);
		mListView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
		itemArray = new ArrayList<String>();
		initItemArray();
		mArrayAdapter = new ArrayAdapter<String>(mContext,
				R.layout.setting_listview, itemArray);
		
		mListView.setOnItemClickListener(new SettingItemClickListener());
		mListView.setAdapter(mArrayAdapter);
		
	}
	
	private void initItemArray(){
		itemArray.add("서울");
		itemArray.add("인천");
		itemArray.add("대전");
		itemArray.add("광주");
		itemArray.add("부산");
		itemArray.add("울산");
		itemArray.add("대구");
		itemArray.add("제주");
	}
	
	private class SettingItemClickListener implements OnItemClickListener{
		
		@Override
		public void onItemClick(AdapterView<?> av, View v, int i, long id) {
			
			SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
			SharedPreferences.Editor editor = sp.edit();
			
			
			switch((int)id){
				case 0 :
					editor.putString("CITY", "서울");
					finish();
					break;
				case 1 :
					editor.putString("CITY", "인천");
					finish();
					break;
				case 2 :
					editor.putString("CITY", "대전");
					finish();
					break;
				case 3 :
					editor.putString("CITY", "광주");
					finish();
					break;
				case 4 :
					editor.putString("CITY", "부산");
					finish();
					break;
				case 5 :
					editor.putString("CITY", "울산");
					finish();
					break;
				case 6 :
					editor.putString("CITY", "대구");
					finish();
					break;
				case 7 :
					editor.putString("CITY", "제주");
					finish();
					break;
			}
			
			editor.commit();
			stopService(new Intent(getApplicationContext(), WeatherService.class));
			startService(new Intent(getApplicationContext(), WeatherService.class));
		}
		
	}
	
}
