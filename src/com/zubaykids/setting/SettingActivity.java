package com.zubaykids.setting;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.zubaykids.dday.DDaySettingActivity;
import com.zubaykids.gallery.SetBackGround;
import com.zubaykids.gallery.SetMyPicture;
import com.zubaykids.gallery.SetPartnerPicture;
import com.zubaykids.initapp.FontChanger;
import com.zubaykids.initapp.ZubayApplication;
import com.zubaykids.smartcover.R;

public class SettingActivity extends Activity {

	private ListView mListView;
	private ArrayAdapter<String> mArrayAdapter;
	private ArrayList<String> itemArray;
	private Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
//		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
//		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_setting);

		//setFont
//		FontChanger.setDefaultFont("DEFAULT", "font.ttf");
		
		mContext = ZubayApplication.getInstance().getApplicationContext();
		mListView = (ListView) findViewById(R.id.setting_listview);
		mListView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
		itemArray = new ArrayList<String>();
		initItemArray();
		
		mArrayAdapter = new ArrayAdapter<String>(mContext,
				R.layout.setting_listview, itemArray){
			@Override
			public View getView(int position, View convertView,
					ViewGroup parent) {
				
				View view = super.getView(position, convertView, parent);
				if(view instanceof TextView){
					TextView textview = (TextView)view;
					if(textview != null){
//						Typeface font = Typeface.createFromAsset(getAssets(), "font.ttf");
//						textview.setTypeface(font);
						return textview;
					}
				}
				return view;
			}
		};
		
		mListView.setOnItemClickListener(new SettingItemClickListener());
		mListView.setAdapter(mArrayAdapter);
		
	}
	
	private void initItemArray(){
		itemArray.add("D-Day 설정");
		itemArray.add("날씨 지역 설정");
		itemArray.add("배경화면 설정");
		itemArray.add("내 사진 설정");
		itemArray.add("상대 사진 설정");
	}
	
	private class SettingItemClickListener implements OnItemClickListener{

		@Override
		public void onItemClick(AdapterView<?> av, View v, int i, long id) {
			switch((int)id){
				case 0 :{
					startActivity(new Intent(mContext, DDaySettingActivity.class));
					break;
				}
				case 1 :{
					startActivity(new Intent(mContext, SelectCity.class));
					break;
				}
				case 2 :{
					startActivity(new Intent(mContext, SetBackGround.class));
					break;
				}
				case 3 :{
					startActivity(new Intent(mContext, SetMyPicture.class));
					break;
				}
				case 4 :{
					startActivity(new Intent(mContext, SetPartnerPicture.class));
					break;
				}
			}
		}
		
	}

}
