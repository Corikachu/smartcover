package com.zubaykids.pairing;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.zubaykids.smartcover.R;


public class InitialPairing_Main extends Activity {
	EditText etMyPhoneNumber, etAnotherPhoneNumber;
	TextView result;
    GoogleCloudMessaging gcm;
    String SENDER_ID = "371100803578"; // gcm project number


	
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    public static final String PROPERTY_REG_ID = "registration_id";
    private static final String PROPERTY_APP_VERSION = "appVersion";

    Context context;
    String myregid="";

	
	//private final String urlPath = "http://172.16.101.52:52273/enroll";
	private final String urlPath = "http://172.16.101.221:52273/enroll";
	//172.16.101.222
	//172.16.101.52
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	context = getApplicationContext();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pair_main);
        etMyPhoneNumber=(EditText)findViewById(R.id.editText1);
        etAnotherPhoneNumber=(EditText)findViewById(R.id.editText2);
        result=(TextView)findViewById(R.id.textView3);
        
        
        if(checkPlayServices())
        {
            gcm = GoogleCloudMessaging.getInstance(this);
            myregid = getRegistrationId(context);
            if (myregid.isEmpty()) {
                registerInBackground();
            }
        }
        else
        {
     	   Log.i("FALSE : checkPlayServices()", "No valid Google Play Services APK found.");
        }
        
    }
    
    private void storeRegistrationId(Context context, String regId) {
    	final SharedPreferences prefs = getPairPreferences(context);
    	int appVersion = getAppVersion(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PROPERTY_REG_ID, regId);
        editor.putInt(PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }
    
    private void registerInBackground() {

        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {

                String msg = "";
                try {

                    if (gcm == null) {
                        gcm = GoogleCloudMessaging.getInstance(context);
                    				}
                myregid = gcm.register(SENDER_ID);
                msg = "Device registered, registration ID=" + myregid;

                Log.v("MyRegID",msg);
                storeRegistrationId(context, myregid);
                
                
                }
                
                catch (Exception ex) {
                	msg = "Error :" + ex.getMessage();
                	Log.v("Exception",msg);
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
            }
        }.execute(null, null, null);
    }
    
    public void onClick(View v){
    	switch(v.getId()){
    	case R.id.button1:
    		final String myphonenumber = etMyPhoneNumber.getText().toString();
    		final String anotherphonenumber = etAnotherPhoneNumber.getText().toString();
    				
    		new AsyncTask<Void, Void, String>() {
                @Override
                protected String doInBackground(Void... params) {
                    String msg = "";
                    
                	DefaultHttpClient httpclient = new DefaultHttpClient();
                	
                	HttpPost httppost = new HttpPost(urlPath);
                	//172.16.101.28
                	
                	ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);   
                	nameValuePairs.add(new BasicNameValuePair("myphonenumber", myphonenumber));
                	nameValuePairs.add(new BasicNameValuePair("anotherphonenumber",anotherphonenumber));
                	nameValuePairs.add(new BasicNameValuePair("myregid",myregid));
                	Log.v("OnClick : myregid",myregid);
                        try {
							httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
						} catch (UnsupportedEncodingException e) {
							// TODO Auto-generated catch block
	                    	Log.v("catch post ","UnsupportedEncodingException");
							e.printStackTrace();
						}
                    	try {
							HttpResponse httpresponse = httpclient.execute(httppost);
				        	Log.v("Delay Response",httpresponse.getEntity().toString());
						} catch (ClientProtocolException e) {
							// TODO Auto-generated catch block
	                    	Log.v("catch response","ClientProtocolException");
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
				        	Log.v("IOException e",Log.getStackTraceString(e));
							e.printStackTrace();
						}
                    	Log.v("return ","msg");
                    return msg;
                }

                @Override
                protected void onPostExecute(String msg) {
                    result.setText("success");
                }
            		}.execute(null, null, null);
            		
            break;
    	}
    }
    
    private SharedPreferences getPairPreferences(Context context) 
    {
        // This sample app persists the registration ID in shared preferences, but
        // how you store the regID in your app is up to you.
        return getSharedPreferences(InitialPairing_Main.class.getSimpleName(),
                Context.MODE_PRIVATE);
    }
    
    private static int getAppVersion(Context context) 
    {
        try 
        {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }
    
    private String getRegistrationId(Context context) {
        final SharedPreferences prefs = getPairPreferences(context);
        String registrationId = prefs.getString(PROPERTY_REG_ID, "");

        if (registrationId.isEmpty()) {
            Log.i("getRegistrationId Pairing", "Registration not found.");
            return "";
        }
        // Check if app was updated; if so, it must clear the registration ID
        // since the existing regID is not guaranteed to work with the new
        // app version.
        int registeredVersion = prefs.getInt(PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i("getRegistrationId", "App version changed.");
            return "";
        }
        return registrationId;
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else 
            {
                Log.i("checkPlayServices()", "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }

}
