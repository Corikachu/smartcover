package com.zubaykids.dday;

import java.util.GregorianCalendar;

import com.zubaykids.initapp.ZubayApplication;

/**
 * 
 * @author Corikachu
 * data format 'YYYYMMDD+title'
 */

public class DdayParser {

	private String mSentence, title;
	private int YY, MM, DD;
	private int Dday;
	private long millisecond;
	private GregorianCalendar calendar;
	
	public DdayParser() {
		calendar = new GregorianCalendar();
	}
	
	public String makeSentense(String title, int YY, int MM, int DD){
		this.title = title;
		this.YY = YY;
		this.MM = MM;
		this.DD = DD;
		
		mSentence = String.valueOf(YY);
		mSentence += makePreZeroSentense(MM);
		mSentence += makePreZeroSentense(DD);
		mSentence += title;
		
		translateMillisecond();
		return mSentence;
	}
	
	public String makeSentense(){
		mSentence = String.valueOf(YY);
		mSentence += makePreZeroSentense(MM);
		mSentence += makePreZeroSentense(DD);
		mSentence += title;
		
		translateMillisecond();
		return mSentence;
	}
	
	public DdayParser parserSentense(String sentence){
		this.mSentence = sentence;
		
		if(sentence.equals(ZubayApplication.SHARED_PREFERENCE_DEFAULT_VALUE)){
			return null;
		}
		
		this.YY = Integer.valueOf(mSentence.substring(0, 4));
		this.MM = Integer.valueOf(mSentence.substring(4, 6));
		this.DD = Integer.valueOf(mSentence.substring(6, 8));
		this.title = mSentence.substring(8);
		
		translateMillisecond();
		
		return this;
	}
	
	private void translateMillisecond(){
		calendar.set(YY, MM, DD);
		millisecond = calendar.getTimeInMillis();
	}
	
	private String makePreZeroSentense(int date){
		if(date < 10){
			return "0"+date;
		}else{
			return String.valueOf(date);
		}
	}
	
	public int getDday() {
		return Dday;
	}
	
	public String getDdayString(){
		if(Dday < 0){
			return "D "+Dday;
		}else{
			return "D +"+Dday;
		}
	}

	public void setDday(int dday) {
		Dday = dday;
	}

	public String getSentence() {
		return mSentence;
	}

	public void setSentence(String mSentence) {
		this.mSentence = mSentence;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYY() {
		return YY;
	}

	public void setYY(int yY) {
		this.YY = yY;
	}

	public int getMM() {
		return MM;
	}

	public void setMM(int mM) {
		this.MM = mM;
	}

	public int getDD() {
		return DD;
	}

	public void setDD(int dD) {
		this.DD = dD;
	}
	
	public long getMillisecond(){
		return this.millisecond;
	}
	
	public void setMillisecond(long mil){
		this.millisecond = mil;
	}
	
	//return YYYY-MM-DD. example 2014-01-01
	public String getDate(){
		int mm = MM+1;
		return ""+YY+"-"+mm+"-"+DD;
	}
}
