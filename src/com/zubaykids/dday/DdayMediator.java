package com.zubaykids.dday;

/**
 * 
 * @author Corikachu
 * 	
 * Defined Dday-Controller
 * 
 */

public class DdayMediator {

	private DdayParser mDdayParser;
	private DdayStorer mDdayStorer;
	private DdayCalculator mDdayCalculator;
	
	public DdayMediator() {
		mDdayParser = new DdayParser();
		mDdayStorer = new DdayStorer();
		mDdayCalculator = new DdayCalculator();
	}
	
	public void add(String title, int[] date){
		mDdayParser.makeSentense(title, date[0], date[1], date[2]);
		mDdayStorer.add(mDdayParser.getSentence());
	}
	
	public void update(String preTitle, int[] preDate, String postTitle, int[] postDate){
		String preSentence = mDdayParser.makeSentense(preTitle, preDate[0], preDate[1], preDate[2]);
		String updateSentence = mDdayParser.makeSentense(postTitle, postDate[0], postDate[1], postDate[2]);
		
		mDdayStorer.update(preSentence, updateSentence);
	}
	
	public void delete(String title, int[] date){
		String sentence = mDdayParser.makeSentense(title, date[0], date[1], date[2]);
		mDdayStorer.delete(sentence);
	}
	
	public DdayParser findDdayByNumber(int number){
		DdayParser parser = new DdayParser();
		
		String sentence =  mDdayStorer.find(number);
		
		if(parser.parserSentense(sentence) == null){
			return null;
		}
		
		long millisecond = parser.getMillisecond();
		int dday = mDdayCalculator.calculateDday(millisecond);
		parser.setDday(dday);
		
		return parser;
	}
	
	public int findString(String title){
		return mDdayStorer.findNumber(title);
	}
	
	public void setPrimaryNumber(int i){
		mDdayStorer.setPrimaryNumber(i);
		return;
	}
	
	public int getPrimaryNumber(){
		return mDdayStorer.getPrimaryNumber();
	}

}
