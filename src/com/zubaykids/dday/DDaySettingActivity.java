package com.zubaykids.dday;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;

import com.zubaykids.initapp.ZubayApplication;
import com.zubaykids.smartcover.R;

public class DDaySettingActivity extends Activity {

	private DdayDialog mDialog;
	private Button mAddDDayButton;
	private ListView mListView;
	private Context mContext;
	private ArrayList<DdayListItem> mDdayArray;
	private DdayMediator mDdayMediator;
	private DdayListViewAdapter mListViewAdapter;

	private SwipeDismissListViewTouchListener swipeListener;
	private SwipeDismissCallBack swipeCallBack;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_setting_dday);

		//getActionBar().setDisplayHomeAsUpEnabled(true);

		mDdayMediator = new DdayMediator();
		mContext = ZubayApplication.getInstance().getApplicationContext();
		mAddDDayButton = (Button) findViewById(R.id.add_dday_button);
		mListView = (ListView) findViewById(R.id.show_dday_listview);
		mListView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
		
		swipeCallBack = new SwipeDismissCallBack();
		swipeListener = new SwipeDismissListViewTouchListener(mListView, swipeCallBack);
		mListView.setOnTouchListener(swipeListener);
		mListView.setOnScrollListener(swipeListener.makeScrollListener());
		
		mAddDDayButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mDialog = new DdayDialog();
				mDialog.show(getFragmentManager(), "Dday Dialog");

			}
		});
		
		
	}

	@Override
	protected void onResume() {
		super.onResume();
		refreshView();
	}

	private void refreshView() {
		mDdayArray = new ArrayList<DdayListItem>();
		for (int i = 1; i <= 4; i++) {

			DdayParser parser = mDdayMediator.findDdayByNumber(i);
			if (parser != null) {
				DdayListItem item = new DdayListItem(parser);
				mDdayArray.add(item);
			}
		}

		mListViewAdapter = new DdayListViewAdapter(mDdayArray);
		mListView.setOnItemClickListener(new DdayListItemClickListener());
		mListView.setAdapter(mListViewAdapter);
		
		if(mDdayArray.size()==4){
			mAddDDayButton.setVisibility(View.INVISIBLE);
		}else{
			mAddDDayButton.setVisibility(View.VISIBLE);
		}
	}

	private class SwipeDismissCallBack implements
			SwipeDismissListViewTouchListener.DismissCallbacks {

		@Override
		public boolean canDismiss(int position) {
			return true;
		}

		@Override
		public void onDismiss(ListView listView, int[] reverseSortedPositions) {
			
			for(int i=0 ; i<reverseSortedPositions.length ; i++){
				Object o = listView.getItemAtPosition(reverseSortedPositions[i]);
				DdayListItem item = (DdayListItem)o;
				DdayParser parser = new DdayParser();
				parser = parser.parserSentense(item.getSentece());
				
				int[] date = {parser.getYY(), parser.getMM(), parser.getDD()};
				mDdayMediator.delete(parser.getTitle(), date);
				refreshView();
			}
		}

	}

	private class DdayListItemClickListener implements OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long position) {

			mDialog = new DdayDialog();
			mDialog.setflag();
			mDialog.setFindItem((int) position + 1);
			mDialog.show(getFragmentManager(), "Dday Dialog");
		}

	}

	private class DdayDialog extends DialogFragment {

		private EditText nameBox;
		private DatePicker datePicker;
		private String ddayTitle;
		private int dYear, dMonth, dDay;
		private DdayMediator mMediator;

		private AlertDialog.Builder alertBuilder;

		private boolean updateFlag = false;
		private int findItem = 0;

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);

			mMediator = new DdayMediator();

			alertBuilder = new AlertDialog.Builder(getActivity());
			LayoutInflater layoutInflater = getActivity().getLayoutInflater();

			// Dialog doesn't have RootView. (RootView is not created now)
			View view = layoutInflater.inflate(R.layout.dialog_add_dday, null,
					false);

			nameBox = (EditText) view.findViewById(R.id.add_dday_namebox);
			datePicker = (DatePicker) view
					.findViewById(R.id.add_dday_datepicker);

			if(updateFlag){
				DdayParser preDate = mMediator.findDdayByNumber(findItem);
				if(preDate.getTitle() != null){
					nameBox.setText(preDate.getTitle());
				}
				datePicker.updateDate(preDate.getYY(), preDate.getMM(), preDate.getDD());
			}
			
			
			
			alertBuilder.setView(view);
			alertBuilder.setTitle("DDAY");

			// Set Positive Button at Dialog.
			alertBuilder.setPositiveButton("확인",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							ddayTitle = nameBox.getText().toString();
							dYear = datePicker.getYear();
							dMonth = datePicker.getMonth();
							dDay = datePicker.getDayOfMonth();
							int[] date = { dYear, dMonth, dDay };

							if (!updateFlag) {

								mMediator.add(ddayTitle, date);
							} else {
								DdayParser preDday = mMediator
										.findDdayByNumber(findItem);
								int[] preDate = { preDday.getYY(),
										preDday.getMM(), preDday.getDD() };
								mMediator.update(preDday.getTitle(), preDate,
										ddayTitle, date);
							}

							refreshView();
						}
					});

			// Set Negative Button at Dialog.
			alertBuilder.setNegativeButton("취소",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {

						}
					});

			return alertBuilder.create();
		}

		public void setflag() {
			updateFlag = true;
		}

		public void setFindItem(int i) {
			this.findItem = i;
		}
	}
}
