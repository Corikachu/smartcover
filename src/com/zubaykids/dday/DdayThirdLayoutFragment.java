package com.zubaykids.dday;

import java.util.ArrayList;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zubaykids.initapp.ZubayApplication;
import com.zubaykids.smartcover.R;

public class DdayThirdLayoutFragment extends Fragment {

	private Context context;
	private FrameLayout mBackgroundFrameLayoutDark;
	private View view;
	private Animation mDarkerAnimation;
	private Animation mLighterAnimation;
	private TextView primaryTitle, primaryDday;
	private TextView leftTitle, leftDday;
	private TextView centerTitle, centerDday;
	private TextView rightTitle, rightDday;
	private RelativeLayout primaryRelativeLayout, leftRelativeLayout;
	private RelativeLayout centerRelativeLayout, rightRelativeLayout;
	
	private DdayMediator mDdayMediator;
	
	public static DdayThirdLayoutFragment getInstance(){
		DdayThirdLayoutFragment frag = new DdayThirdLayoutFragment();
		return frag;
	};
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		context = ZubayApplication.getInstance().getApplicationContext();
		view =  inflater.inflate(R.layout.oldversion_fragment_thirdlayout_dday, container, false);
		initView();
		
		//background darker and all layout will VISIBLE
		mBackgroundFrameLayoutDark.startAnimation(mDarkerAnimation);
		mBackgroundFrameLayoutDark.setVisibility(View.VISIBLE);
		mBackgroundFrameLayoutDark.setOnClickListener(new FrameLayoutToLighterClickListener());
		
		DdayLayoutClickListener ddayClickListener = new DdayLayoutClickListener();
		primaryRelativeLayout.setOnClickListener(ddayClickListener);
		leftRelativeLayout.setOnClickListener(ddayClickListener);
		centerRelativeLayout.setOnClickListener(ddayClickListener);
		rightRelativeLayout.setOnClickListener(ddayClickListener);
		
		return view;
	}
	
	@Override
	public void onResume(){
		super.onResume();
		
		mDdayMediator = new DdayMediator();
		if(mDdayMediator.getPrimaryNumber() == 0){
			return;
		}
		
		ArrayList<DdayParser> array = new ArrayList<DdayParser>();
		array.add(mDdayMediator.findDdayByNumber(mDdayMediator.getPrimaryNumber()));
		for(int i = 1 ; i <= 4 ; i++){
			if(i == mDdayMediator.getPrimaryNumber()){
				continue;
			}
			if(mDdayMediator.findDdayByNumber(i) != null){
				array.add(mDdayMediator.findDdayByNumber(i));
			}
		}
		
		for(int i = 0 ; i < array.size() ; i++){
			DdayParser parser = array.get(i);
			if(i==0){
				setTextView(parser, primaryTitle, primaryDday);
			}else if(i==1){
				setTextView(parser, leftTitle, leftDday);
			}else if(i==2){
				setTextView(parser, centerTitle, centerDday);
			}else if(i==3){
				setTextView(parser, rightTitle, rightDday);
			}
			
		}
	}
	
	private void setTextView(DdayParser parser, TextView title, TextView dday){
		if(parser == null){
			title.setText("New Dday!");
			dday.setText(" ");
		}else{
			title.setText(parser.getTitle());
			dday.setText(parser.getDdayString());
			parser = null;
		}
	}
	
	
	private void initView(){
		
		mBackgroundFrameLayoutDark = (FrameLayout)view.findViewById(R.id.framelayout_darker);
		
		mDarkerAnimation = AnimationUtils.loadAnimation(context, R.anim.layout_darker);
		mLighterAnimation = AnimationUtils.loadAnimation(context, R.anim.layout_lighter);
		
		primaryTitle = (TextView)view.findViewById(R.id.dday_primary_title_textview);
		primaryDday = (TextView)view.findViewById(R.id.dday_primary_dday_textview);
		primaryRelativeLayout = (RelativeLayout)view.findViewById(R.id.dday_primary_relativelayout);
		
		leftTitle = (TextView)view.findViewById(R.id.dday_left_title_textview);
		leftDday = (TextView)view.findViewById(R.id.dday_left_dday_textview);
		leftRelativeLayout = (RelativeLayout)view.findViewById(R.id.dday_left_relativelayout);
		
		centerTitle = (TextView)view.findViewById(R.id.dday_center_title_textview);
		centerDday = (TextView)view.findViewById(R.id.dday_center_dday_textview);
		centerRelativeLayout = (RelativeLayout)view.findViewById(R.id.dday_center_relativelayout);
		
		rightTitle = (TextView)view.findViewById(R.id.dday_right_title_textview);
		rightDday = (TextView)view.findViewById(R.id.dday_right_dday_textview);
		rightRelativeLayout = (RelativeLayout)view.findViewById(R.id.dday_right_relativelayout);
	}
	
	
	private class FrameLayoutToLighterClickListener implements View.OnClickListener{
		
		@Override
		public void onClick(View v) {
			
			mBackgroundFrameLayoutDark.startAnimation(mLighterAnimation);
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {
					view.setVisibility(View.GONE);
				}
			}, 300);
		}
	
	}
	
	public class DdayLayoutClickListener implements View.OnClickListener{

		public DdayLayoutClickListener() {
		}
		
		@Override
		public void onClick(View v) {
			switch(view.getId()){
			case R.id.dday_primary_relativelayout:
				
				break;
			case R.id.dday_left_relativelayout:
				/*String title = leftTitle.getText().toString();
				if(title.equals("New Dday!")){
					
				}
				
				int number = mDdayMediator.findString();
				mDdayMediator.setPrimaryNumber();*/
				break;
			case R.id.dday_center_relativelayout:
				
				break;
			case R.id.dday_right_relativelayout:
				
				break;
			}
		}
		
	}
}
