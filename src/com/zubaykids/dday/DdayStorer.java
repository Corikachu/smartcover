package com.zubaykids.dday;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.zubaykids.initapp.ZubayApplication;


/**
 * 
 * @author Corikachu
 * 
 * Save Dday YYYYMMDD with SharedPreferenced.
 */

public class DdayStorer {

	private SharedPreferences mPref;
	private SharedPreferences.Editor mSharedEditor; 
	private Context context;
	private String DEFAULT_VALUE, DALSOM_DDAY;
	private String PRIMARY_KEY;
	private String Dday;
	
	public DdayStorer() {
		DALSOM_DDAY = ZubayApplication.SHARED_PREFERENCE_DDAY;
		DEFAULT_VALUE = ZubayApplication.SHARED_PREFERENCE_DEFAULT_VALUE;
		PRIMARY_KEY = ZubayApplication.SHARED_PREFERENCE_DDAY_PRIMARY;
		Dday = "DDAY_";
		
		this.context = ZubayApplication.getInstance().getApplicationContext();
		this.mPref = context.getSharedPreferences(DALSOM_DDAY, Context.MODE_PRIVATE);
		mSharedEditor = mPref.edit();
	}

	public void add(String sentence){
		for(int i = 1 ; i <= 4 ; i++){
			String value = mPref.getString(Dday+i, DEFAULT_VALUE);
			
			if(value.equals(DEFAULT_VALUE)){
				mSharedEditor.putString(Dday+i, sentence);
				mSharedEditor.apply();				
				
				if(i==1){
					setPrimaryNumber(1);
				}
				
				break;
			}
			
			if(i==4){
				Toast.makeText(context, "Error : 'add' method is not working", Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	public void update(String preSentence, String updateSentence){
		for(int i = 1 ; i <=4 ; i++){
			String value = mPref.getString(Dday+i, DEFAULT_VALUE);
			
			if(value.equals(preSentence)){
				mSharedEditor.putString(Dday+i, updateSentence);
				mSharedEditor.apply();
				return;
			}
		}
		return;
	}
	
	public void delete(String sentence){
		for(int i = 1 ; i <=4 ; i++){
			String value = mPref.getString(Dday+i, DEFAULT_VALUE);
			
			if(value.equals(sentence)){
				mSharedEditor.putString(Dday+i, DEFAULT_VALUE);
				mSharedEditor.apply();
				
				changePrimaryNumber();
				return;
			}
		}
		return;
	}
	
	public String find(int number){
		String value = mPref.getString(Dday+number, DEFAULT_VALUE);
		return value;
	}
	
	public int findNumber(String title){
		for(int i = 1 ; i <=4 ; i++){
			String value = mPref.getString(Dday+i, DEFAULT_VALUE);
			
			if(value.contains(title)){
				return i;
			}
		}
		return 0;
	}
	
	//Primary D-day is shown on First Layout.
	//set Primary Number
	public void setPrimaryNumber(int i){
		mSharedEditor.putInt(PRIMARY_KEY, i);
		mSharedEditor.apply();
	}
	
	//get Primary Number. Default value is 0.
	public int getPrimaryNumber(){
		return mPref.getInt(PRIMARY_KEY, 0);
	}
	
	private void changePrimaryNumber(){
		if(find(getPrimaryNumber()).equals(DEFAULT_VALUE)){
			for(int i = 1 ; i <= 4 ; i++){
				if(find(i).equals(DEFAULT_VALUE)){
					if(i==4){
						setPrimaryNumber(0);
					}
					continue;
				}else{
					setPrimaryNumber(i);
					break;
				}
			}
		}
	}
}
