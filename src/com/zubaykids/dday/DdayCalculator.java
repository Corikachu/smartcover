package com.zubaykids.dday;

import java.util.Calendar;

public class DdayCalculator {

	Calendar mCalendar;
	
	public DdayCalculator() {
		mCalendar = Calendar.getInstance();
	}

	public int calculateDday(long ddayMillisecondTime){
		long currentTime = mCalendar.getTimeInMillis();
		long currentdate = currentTime/(1000*60*60*24);
		
		long DdayDate = ddayMillisecondTime/(1000*60*60*24);
		
		long dday = currentdate - DdayDate;
		
		return (int)dday+1;
	}
	

	
}
