package com.zubaykids.dday;

import java.util.ArrayList;

import com.zubaykids.initapp.ZubayApplication;
import com.zubaykids.smartcover.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class DdayListViewAdapter extends BaseAdapter {

	private ArrayList<DdayListItem> array;
	private LayoutInflater mInflater;
	
	public DdayListViewAdapter(ArrayList<DdayListItem> array) {
		this.array = array;
		Context context = ZubayApplication.getInstance().getApplicationContext();
		mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
	}

	@Override
	public int getCount() {
		return array.size();
	}

	@Override
	public Object getItem(int position) {
		return array.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		if(convertView == null){
			convertView = mInflater.inflate(R.layout.dday_listview_item, parent, false);
		}
		
		DdayListItem item = array.get(position);
		
		TextView title = (TextView)convertView.findViewById(R.id.dday_listview_title);
		TextView date = (TextView)convertView.findViewById(R.id.dday_listview_date);
		TextView dday = (TextView)convertView.findViewById(R.id.dday_listview_dday);
		
		title.setText(item.getTitle());
		date.setText(item.getDate());
		dday.setText(item.getDday());
		
		return convertView;
	}

	
}
