package com.zubaykids.dday;

public class DdayListItem {

	private DdayParser parser;
	private String sentence;
	private String title, date;
	private int dday;

	
	public DdayListItem(DdayParser parser) {
		this.parser = parser;
		
		sentence = parser.getSentence();
		title = parser.getTitle();
		date = parser.getDate();
		dday = parser.getDday();
	}

	public String getTitle(){
		return title;
	}
	
	public String getDate(){
		return date;
	}
	
	public String getDday(){
		return String.valueOf(dday);
	}
	
	public String getSentece(){
		return sentence;
	}
}
