package com.zubaykids.gallery;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.zubaykids.smartcover.R;

public class SetMyPicture extends Activity {

	private static final int SELECT_PICTURE = 1;
	private ImageView img;
	Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.setbackground);
		
		img = (ImageView) findViewById(R.id.img);

		Intent intent = new Intent(
				Intent.ACTION_GET_CONTENT,      // 또는 ACTION_PICK
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		intent.setType("image/*");
		intent.putExtra("crop", "true");
		intent.putExtra("outputX", 200);
		intent.putExtra("outputY", 200);
		intent.putExtra("aspectX", 1);
		intent.putExtra("aspectY", 1);
		intent.putExtra("scale", true);
		intent.putExtra(MediaStore.EXTRA_OUTPUT, getTempUri());
		intent.setAction(Intent.ACTION_GET_CONTENT);

		startActivityForResult(intent, SELECT_PICTURE);	
	}

	Uri getTempUri() {
		return Uri.fromFile(getTempFile());
	}

	File getTempFile() {
		if (isSDCARDMOUNTED()) {
			File f = new File(Environment.getExternalStorageDirectory(), // 외장메모리 경로
					"/mypicture.jpg");
			try {
				f.createNewFile();      // 외장메모리에 temp.jpg 파일 생성
			} catch (IOException e) {
			}

			return f;
		} else
			return null;
	}

	boolean isSDCARDMOUNTED() {
		String status = Environment.getExternalStorageState();
		if (status.equals(Environment.MEDIA_MOUNTED))
			return true;

		return false;
	}

	protected void onActivityResult(int requestCode, int resultCode,
			Intent imageData) {
		super.onActivityResult(requestCode, resultCode, imageData);
		
		switch (requestCode) {
		case 1 :
			if (resultCode == RESULT_OK) {
				if (imageData != null) {
					String filePath = Environment.getExternalStorageDirectory()
							+ "/mypicture.jpg";

					Bitmap selectedImage = BitmapFactory.decodeFile(filePath);
					// temp.jpg파일을 Bitmap으로 디코딩한다.

					try {
						OutputStream outputStream = openFileOutput("MY_PICTURE",
								Context.MODE_PRIVATE);
						OutputStreamWriter osw2 = new OutputStreamWriter(outputStream);
						selectedImage.compress(Bitmap.CompressFormat.PNG, 90, outputStream);
						osw2.close();
					} catch(Exception e) {
						e.printStackTrace();
					}

					img.setImageBitmap(selectedImage); 
					
					Toast.makeText(getApplicationContext(), "선택한 사진이 내 사진으로 설정되었습니다", Toast.LENGTH_SHORT).show();
					
					File f = new File(Environment.getExternalStorageDirectory(), "/mypicture.jpg");
					f.delete();
				}
			}
			else {
				finish();
			}
			break;
		}

	}
}