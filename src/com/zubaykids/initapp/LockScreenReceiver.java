package com.zubaykids.initapp;

import android.app.KeyguardManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;

//import com.zubaykids.androidTest.MyLog;


/**
 * 
 * @author corikachu
 * This class use deprecated class and method.
 * TODO : find other way to remove deprecated thing.
 *
 */
public class LockScreenReceiver extends BroadcastReceiver {

	private Context context;
	private static ControlCPU controlCPU;

	//Temp
	private KeyguardManager km = null;
//	private KeyguardManager.KeyguardLock keyLock = null;


	@Override
	public void onReceive(Context context, Intent intent) {
		this.context = context;

		if(controlCPU == null){
			controlCPU = new ControlCPU();
		}

		//Temp
		if (km == null) {
			km = (KeyguardManager)
					context.getSystemService(Context.KEYGUARD_SERVICE); 	
		}
/*		if (keyLock == null) {
			keyLock = km.newKeyguardLock(Context.KEYGUARD_SERVICE);
		}*/


		String currentAction = intent.getAction();

		if(currentAction.equals(ZubayApplication.SENSOR_OK) ||
				currentAction.equals(Intent.ACTION_SCREEN_ON)){
//			keyLock.disableKeyguard();
			startApp();
		}

	}

	public void startApp(){
		//new Intent

		//wake up CPU
		controlCPU.wakeUp();

		Intent i = new Intent(context, SmartCoverMainActivity.class);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		context.startActivity(i);

	}


	public class ControlCPU {
		private PowerManager powerManager; 
		private PowerManager.WakeLock sCpuWakeLock;

		public ControlCPU(){
			powerManager = (PowerManager)
					context.getSystemService(Context.POWER_SERVICE);

			sCpuWakeLock = powerManager.newWakeLock(
					PowerManager.PARTIAL_WAKE_LOCK |
					PowerManager.ACQUIRE_CAUSES_WAKEUP |
					PowerManager.ON_AFTER_RELEASE, "Screen");
		}

		public void wakeUp(){
			sCpuWakeLock.acquire();
		}

		public void releaseLock(){
			sCpuWakeLock.release();
		}
	}


}
