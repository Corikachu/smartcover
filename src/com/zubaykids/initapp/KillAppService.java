package com.zubaykids.initapp;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;

public class KillAppService extends Service {

	private String INTENT_KILL = ZubayApplication.INTENT_KILL_APPLICATION;
	private Context mContext;
	private LightSensorChecker checker;
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	@Override
	public void onCreate() {
		super.onCreate();
		mContext = ZubayApplication.getInstance().getApplicationContext();
		checker = new LightSensorChecker();
		checker.start();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		checker.stop();
	}
	
	private class LightSensorChecker implements SensorEventListener{

		private SensorManager mSensorManager;
		private Sensor mLightSensor;
		
		public LightSensorChecker() {
			mSensorManager = (SensorManager) mContext.getSystemService(Context.SENSOR_SERVICE);
			mLightSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
		}
		
		@Override
		public void onSensorChanged(SensorEvent event) {
			if(event.sensor.equals(mLightSensor)){
				if(event.values[0] > 30){
					mContext.sendBroadcast(new Intent(INTENT_KILL));
				}
			}
		}

		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {
			
		}
		
		public void start(){
			mSensorManager.registerListener(this, mLightSensor, SensorManager.SENSOR_DELAY_FASTEST);
		}
		
		public void stop(){
			mSensorManager.unregisterListener(this);
		}
	}
}
