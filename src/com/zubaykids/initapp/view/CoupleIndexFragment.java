package com.zubaykids.initapp.view;

import java.io.InputStream;

import android.app.Fragment;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.zubaykids.initapp.ZubayApplication;
import com.zubaykids.smartcover.R;

public class CoupleIndexFragment extends Fragment{
	
	private RoundedImageView myPicture;
	private RoundedImageView partnerPicture;
	private TextView textView1;
	private Typeface font;
	private ImageView heartImage;
	private TextView coupleIndex;
	private Animation coupleIndex_anim1;
	private Animation coupleIndex_anim2;
	private Animation coupleIndex_anim3;
	
	private Context context;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view =  inflater.inflate(R.layout.fragment_couple_index, container, false);
		context = ZubayApplication.getInstance().getApplicationContext();
		
		coupleIndex_anim1 = AnimationUtils.loadAnimation(view.getContext(), R.anim.today_dalsum);
		coupleIndex_anim2 = AnimationUtils.loadAnimation(view.getContext(), R.anim.today_dalsum2);
		coupleIndex_anim3 = AnimationUtils.loadAnimation(view.getContext(), R.anim.today_dalsum3);
		
		myPicture = (RoundedImageView)view.findViewById(R.id.profile_image_my_couple_index);
		myPicture.setBorderWidth(15);
		myPicture.setBorderColor("#a9e9ee");
		myPicture.addShadow();
		partnerPicture = (RoundedImageView)view.findViewById(R.id.profile_image_parter_couple_index);
		partnerPicture.setBorderWidth(15);
		partnerPicture.setBorderColor("#FFCDCF");
		partnerPicture.addShadow();
		textView1 = (TextView)view.findViewById(R.id.couple_index_explain_textview);
		font = Typeface.createFromAsset(view.getContext().getAssets(), "font.ttf");
		textView1.setTypeface(font);
		coupleIndex = (TextView)view.findViewById(R.id.coupleIndex);
		heartImage = (ImageView)view.findViewById(R.id.couple_index_heart);
		
		try{
			InputStream my_is = view.getContext().openFileInput("MY_PICTURE");
			Bitmap my_b = BitmapFactory.decodeStream(my_is);
			Drawable my_drawable = new BitmapDrawable(my_b);
			myPicture.setImageDrawable(my_drawable);
		} catch(Exception e) {
			e.printStackTrace();
		}
		try {			
			InputStream partner_is = view.getContext().openFileInput("PARTNER_PICTURE");
			Bitmap partner_b = BitmapFactory.decodeStream(partner_is);
			Drawable partner_drawable = new BitmapDrawable(partner_b);
			partnerPicture.setImageDrawable(partner_drawable);		
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		TextView explainTextView = (TextView)view.findViewById(R.id.couple_index_explain_textview);
		Typeface typeface = Typeface.createFromAsset(context.getAssets(), "font.ttf");
		explainTextView.setTypeface(typeface);
		
		TextView coupleIndex = (TextView)view.findViewById(R.id.coupleIndex);
		coupleIndex.setTypeface(typeface);

		myPicture.startAnimation(coupleIndex_anim1);
		partnerPicture.startAnimation(coupleIndex_anim1);
		heartImage.startAnimation(coupleIndex_anim2);
		coupleIndex.startAnimation(coupleIndex_anim3);
		textView1.startAnimation(coupleIndex_anim3);
		
		return view;
	}
}
