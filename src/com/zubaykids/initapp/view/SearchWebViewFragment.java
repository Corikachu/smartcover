package com.zubaykids.initapp.view;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.zubaykids.initapp.ZubayApplication;
import com.zubaykids.smartcover.R;

public class SearchWebViewFragment extends Fragment {

	private View view;
	private WebView mWebView;
	private Context context;
	private FragmentManager mFragmentManager;
	private FragmentTransaction mTransaction;

	private String keyword = null;
	private String url = null;
	private String title = " ";
	private String SHARED_NAME = ZubayApplication.SHARED_PREFERENCE_FIRST_OPEN_FILENAME;
	private String SHARED_FIRST_TIME = ZubayApplication.SHARED_PREFERENCE_IS_FIRST_WEBVIEW_OPEN;

	private String TAG_FIRST_WEBVIEW = ZubayApplication.TAG_FRAGMENT_FIRST_WEBVIEW;

	public SearchWebViewFragment() {
		context = ZubayApplication.getInstance().getApplicationContext();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.activity_second_webview, container,
				false);
		
		mFragmentManager = getFragmentManager();
		mTransaction = mFragmentManager.beginTransaction();

		mWebView = (WebView) view.findViewById(R.id.search_webview);
		mWebView.setWebViewClient(new WebClient());
		WebSettings set = mWebView.getSettings();
		set.setJavaScriptEnabled(true);
		set.setBuiltInZoomControls(true);

		if(keyword != null) {
			mWebView.loadUrl("http://search.naver.com/search.naver?ie=UTF-8&query="
					+ keyword);
		} else {
			mWebView.loadUrl(url);
		}
		
		//exit button
		ImageView exitImage = (ImageView)view.findViewById(R.id.webview_layout_x_image);
		exitImage.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				Fragment thisFragment = mFragmentManager.findFragmentByTag(ZubayApplication.TAG_FRAGMENT_SEARCH_WEBVIEW);
				
				if(thisFragment != null){
					mTransaction = mFragmentManager.beginTransaction();
					mTransaction.remove(thisFragment);
					mTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
					mTransaction.addToBackStack(null);
					mTransaction.commit();
				}
				
			}
		});
		
		//Title TextView
		TextView titleTextView = (TextView)view.findViewById(R.id.webview_textview);
		titleTextView.setText(title);

		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		//checkFirstTimeWebView();
	}
	
	@Override
	public void onDestroyView() {
		super.onDestroyView();
		Fragment thisFragment = mFragmentManager.findFragmentByTag(ZubayApplication.TAG_FRAGMENT_SEARCH_WEBVIEW);
		if(thisFragment != null){
			mTransaction = mFragmentManager.beginTransaction();
			mTransaction.remove(thisFragment);
			mTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			mTransaction.addToBackStack(null);
			mTransaction.commit();
		}
	}

	private void checkFirstTimeWebView() {
		SharedPreferences shared = context.getSharedPreferences(SHARED_NAME,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = shared.edit();

		// if(shared.getBoolean(SHARED_FIRST_TIME, true)){

		FragmentManager fragManager = getFragmentManager();
		FragmentTransaction trans = fragManager.beginTransaction();

		trans.replace(R.id.second_page_webview_linearlayout,
				new ExplainFirstWebView(), TAG_FIRST_WEBVIEW);
		trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		trans.commit();

		editor.putBoolean(SHARED_FIRST_TIME, false);
		editor.apply();

		// }
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
		return;
	}
	
	public void setTitle(String title){
		this.title = title;
	}

	private class WebClient extends WebViewClient {
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}
	}

	private class ExplainFirstWebView extends Fragment {

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View view = inflater.inflate(
					R.layout.fragment_explain_first_webview, container, false);

			view.findViewById(R.id.explain_first_webview_relativelayout)
					.setOnClickListener(new View.OnClickListener() {

						@Override
						public void onClick(View v) {
							Fragment thisFragment = getFragmentManager()
									.findFragmentByTag(TAG_FIRST_WEBVIEW);
							getFragmentManager().beginTransaction()
									.remove(thisFragment).commit();
						}
					});

			return view;
		}
	}
}
