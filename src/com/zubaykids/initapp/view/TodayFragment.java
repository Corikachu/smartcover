package com.zubaykids.initapp.view;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.zubaykids.initapp.ZubayApplication;
import com.zubaykids.smartcover.R;

public class TodayFragment extends Fragment {

	private Context context;
	private Typeface font;
	private View rootView;
	
	private Animation todayTitleAnimation;
	private Animation heartAnimation;
	private Animation todayContentAnimation;
	
	SharedPreferences pref;
	SharedPreferences.Editor editor;
	
	JSONObject jsonObject;
	
	TextView todayTitle;
	TextView contents;
	public static TodayFragment newInstance() {
		TodayFragment frag = new TodayFragment();

		return frag;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		context = ZubayApplication.getInstance().getApplicationContext();
		pref = PreferenceManager.getDefaultSharedPreferences(context);
		editor = pref.edit();
		
		
		
		View view = inflater.inflate(R.layout.fragment_third_layout_today,
				container, false);
		
		rootView = inflater.inflate(
				R.layout.fragment_third_layout_today, container, false);
		
		todayTitle = (TextView)view.findViewById(R.id.today_dalsum_suggest);
		contents = (TextView)view.findViewById(R.id.content_dalsum_suggest);
		ImageView heart = (ImageView)view.findViewById(R.id.indexHeart);
		
		todayTitleAnimation = AnimationUtils.loadAnimation(context, R.anim.today_dalsum);
		heartAnimation = AnimationUtils.loadAnimation(context, R.anim.today_dalsum2);
		todayContentAnimation = AnimationUtils.loadAnimation(context, R.anim.today_dalsum3);
		
		font = Typeface.createFromAsset(rootView.getContext().getAssets(), "font.ttf");
		todayTitle.setTypeface(font);
		contents.setTypeface(font);
		
		todayTitle.startAnimation(todayTitleAnimation);
		contents.startAnimation(todayContentAnimation);
		heart.startAnimation(heartAnimation);
		
		if(pref.getBoolean("fortune_repeated", true))
		{
			Log.v("oncreate","초기");
			doFortune();
		}
		else
		{
			Log.v("oncreate","반복");

        	if(pref.getBoolean("REGID", false))
        	{
        		Log.v("반복","인증성공");
        		contents.setText(pref.getString("fortune_couple","init"));
        	}
        	else
        	{
        		Log.v("반복","인증실패");
        		Log.v("textview",pref.getString("fortune_couple","init"));
        		contents.setText(pref.getString("fortune_couple","init"));
        	}
		}

		
		return view;
	}

	private void doFortune() {
	    {
			Log.v("FORTUNE_cosmopolitan","start!");
	        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {

	            @Override
	            protected String doInBackground(Void... params) {
	        		Log.v("FORTUNE_doInBackground","start!");

	                String msg="";
	                
	            	DefaultHttpClient httpclient = new DefaultHttpClient();
	            	
	            	HttpPost httpPost= new HttpPost("http://172.16.100.172:52273/fortune"); //서버 IP

					ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
					nameValuePairs.add(new BasicNameValuePair("sign",pref.getString("STAR", "init")));

					try {
						httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					} catch (UnsupportedEncodingException e1) {
						// TODO Auto-generated catch block
						Log.v("UnsupportedEncodingException","UnsupportedEncodingException");
						e1.printStackTrace();
					}			
				
	                try {
	                    HttpResponse response = httpclient.execute(httpPost);

	                    String json_string = EntityUtils.toString(response.getEntity());

	                    parseFORTUNE_JSON(json_string);  

	                } catch (ClientProtocolException e) {
							Log.v("ClientProtocolException", "ClientProtocolException");
	                	e.printStackTrace();
	                    // TODO Auto-generated catch block
	                } catch (IOException e) {
							Log.v("IOException", "IOException");
	                    // TODO Auto-generated catch block
	                	e.printStackTrace();
					}
	                return msg;
	            }

	            private void parseFORTUNE_JSON(String json_string) {
					// TODO Auto-generated method stub
	           	 try {    	
	     	        jsonObject = new JSONObject(json_string);
	     	        editor.putString("fortune_couple",jsonObject.getString("couple"));
	     	        editor.putString("fortune_single",jsonObject.getString("single"));
	     	        editor.putBoolean("fortune_repeated",false);
	     	        editor.commit();
	     	        Log.v("single",jsonObject.getString("single"));
	     	        Log.v("couple",jsonObject.getString("couple"));
	     	    } catch (JSONException e) {
	     	    	Log.v("5555","JSONException");
	     	        e.printStackTrace();
	     	    }
				}

				@Override
	            protected void onPostExecute(String msg) {
	            	Log.v("onPostExecute","");
	            	
	            	if(pref.getBoolean("REGID", false))
	            	{
	            		Log.v("인증성공","ok");
	            		contents.setText(pref.getString("fortune_couple","init"));
	            	}
	            	else
	            	{
	            		Log.v("인증실패","ok");
	            		contents.setText(pref.getString("fortune_couple","init"));
	            		//Log.v("초기 textview",pref.getString("fortune_", defValue))
	            	}
	            	
	            }
	        		};
	        if(Build.VERSION.SDK_INT >= 11)
	        	task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	        else
	        	task.execute();
	    }		
	}
}
