package com.zubaykids.initapp.view;

import java.util.ArrayList;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;

import com.zubaykids.gcm.SuggestItem;
import com.zubaykids.gcm.SuggestItem.Item;
import com.zubaykids.initapp.ZubayApplication;
import com.zubaykids.initapp.zeroPageFragment.ContentFragment1;
import com.zubaykids.initapp.zeroPageFragment.ContentFragment2;
import com.zubaykids.initapp.zeroPageFragment.ContentFragment3;
import com.zubaykids.initapp.zeroPageFragment.ContentFragment4;
import com.zubaykids.initapp.zeroPageFragment.ContentFragment5;
import com.zubaykids.smartcover.R;

public class ZeroFragment extends Fragment{

	private static final float MIN_SCALE = 0.75f;
    private static final float MIN_ALPHA = 0.75f;
	
	private VerticalViewPager mViewPager;	
	private Animation frameDarker;
	private FrameLayout mFrameLayout;
	private ViewPagerAdapter adapter;
	private Context context;
	
	public static ZeroFragment newInstance() {
		ZeroFragment instance = new ZeroFragment();
		
		return instance;
	}
	
	public ZeroFragment() {
		context = ZubayApplication.getInstance().getApplicationContext();
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
	
		View view = inflater.inflate(R.layout.fragment_zero_page, container, false);
		mViewPager = (VerticalViewPager)view.findViewById(R.id.zero_viewpager);
		
		FragmentManager fragmentManager = getChildFragmentManager();
		adapter = new ViewPagerAdapter(fragmentManager);
		mViewPager.setAdapter(adapter);
		mViewPager.setPageTransformer(true, new ViewPager.PageTransformer(){

			@Override
			public void transformPage(View view, float position) {
				int pageWidth = view.getWidth();
                int pageHeight = view.getHeight();

                if (position < -1) { // [-Infinity,-1)
                    // This page is way off-screen to the left.
                    view.setAlpha(0);

                } else if (position <= 1) { // [-1,1]
                    // Modify the default slide transition to shrink the page as well
                    float scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position));
                    float vertMargin = pageHeight * (1 - scaleFactor) / 2;
                    float horzMargin = pageWidth * (1 - scaleFactor) / 2;
                    if (position < 0) {
                        view.setTranslationY(vertMargin - horzMargin / 2);
                    } else {
                        view.setTranslationY(-vertMargin + horzMargin / 2);
                    }

                    // Scale the page down (between MIN_SCALE and 1)
                    view.setScaleX(scaleFactor);
                    view.setScaleY(scaleFactor);

                    // Fade the page relative to its size.
                    view.setAlpha(MIN_ALPHA +
                            (scaleFactor - MIN_SCALE) /
                                    (1 - MIN_SCALE) * (1 - MIN_ALPHA));

                } else { // (1,+Infinity]
                    // This page is way off-screen to the right.
                    view.setAlpha(0);
                }
			}
		});
		
		mFrameLayout = (FrameLayout)view.findViewById(R.id.fragment_zero_layout);
		frameDarker = AnimationUtils.loadAnimation(context, R.anim.layout_darker);
		mFrameLayout.startAnimation(frameDarker);
		mFrameLayout.setVisibility(View.VISIBLE);
		
		return view;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		SharedPreferences shared =  context.getSharedPreferences(ZubayApplication.SHARED_PREFERENCE_SUGGEST_ITEM_FILENAME, Context.MODE_PRIVATE);
		if(shared.getBoolean(ZubayApplication.SHARED_PREFERENCE_SUGGEST_DATA_CHANGED, false)){
			adapter.notifyDataSetChanged();
			SharedPreferences.Editor editor = shared.edit();
			editor.putBoolean(ZubayApplication.SHARED_PREFERENCE_SUGGEST_DATA_CHANGED, false);
			editor.apply();
		}
	}
	
	private class ViewPagerAdapter extends FragmentPagerAdapter {

		ArrayList<Fragment> fragments;
		
		public ViewPagerAdapter(FragmentManager fragmentManager){
			super(fragmentManager);
			fragments = new ArrayList<Fragment>();
			
			for(int i=1 ; i<=5 ; i++){
				SuggestItem.Item item = new Item();
				SuggestItem suggest = new SuggestItem();
				item = suggest.loadItem(i);
				
				if(item == null){
					return;
				}else{
					if(i==1){
						ContentFragment1 frag1 = new ContentFragment1();
						frag1.setItem(item);
						fragments.add(frag1);
					}else if(i==2){
						ContentFragment2 frag2 = new ContentFragment2();
						frag2.setItem(item);
						fragments.add(frag2);
					}else if(i==3){
						ContentFragment3 frag3 = new ContentFragment3();
						frag3.setItem(item);
						fragments.add(frag3);
					}else if(i==4){
						ContentFragment4 frag4 = new ContentFragment4();
						frag4.setItem(item);
						fragments.add(frag4);
					}else if(i==5){
						ContentFragment5 frag5 = new ContentFragment5();
						frag5.setItem(item);
						fragments.add(frag5);
					}
				}
			}
		}
		
		@Override
		public Fragment getItem(int position) {
			return fragments.get(position);
		}

		@Override
		public int getCount() {
			return fragments.size();
		}

		
		
		
	}
	
}
