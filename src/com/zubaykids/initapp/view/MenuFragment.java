package com.zubaykids.initapp.view;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zubaykids.smartcover.R;

public class MenuFragment extends Fragment {
	
	View v;

	@Override
	public View onCreateView(LayoutInflater inflater,
			ViewGroup container, Bundle savedInstancState) {
		
		v = inflater.inflate(R.layout.menu_fragment, container, false);
		
		return v;
	}
	
}
