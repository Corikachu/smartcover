package com.zubaykids.initapp.view;

import java.util.Calendar;

import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zubaykids.SaveInfo.PartnerInfo;
import com.zubaykids.bottomAnimation.moveRect.RectAnimation;
import com.zubaykids.dday.DdayMediator;
import com.zubaykids.initapp.SmartCoverMainActivity;
import com.zubaykids.initapp.ZubayApplication;
import com.zubaykids.smartcover.R;
import com.zubaykids.sound.SoundManager;
import com.zubaykids.weather.WeatherService;

public class FirstFragment extends Fragment {

	private static final String ARG_SECTION_NUMBER = "section_number";
	private String partnerPhoneNumber = new PartnerInfo().getPartnerPhone();
	private View rootView;
	private TextView time_TextView;
	private TextView month_TextView;
	private TextView day_TextView;
	private Calendar cal;
	private ImageView direct_button;
	private FrameLayout direct_framelayout;
	private ImageButton direct_videocall;
	private ImageButton direct_call;
	private ImageButton direct_message;
	private ImageButton direct_katok;
	private TextView dday_TextView;
	private TextView dday_text_TextView;
	private FrameLayout menu;
	private ImageButton weather_icon;
	private ImageView camera;
	private ImageView setting;
	private RelativeLayout relativeLayout;
	private TextView battery;
	private Context mContext;
	private TextView countCallMessage;
	private TextView textCallMessage;

	private Animation direct_button_anim;
	private Animation call_button_anim;
	private Animation videocall_button_anim;
	private Animation message_button_anim;
	private Animation katok_button_anim;
	private Animation menu_anim;
	private Animation menu_close_anim;
	private Animation dday_anim;
	private Animation dday_text_anim;
	private Animation dday_anim2;

	private double xAtDown;
	private double yAtDown;
	private double xAtUp;
	private double yAtUp;

	private final int SWIPE_MIN_DISTANCE = 200;
	private String WEATHER_IMAGE = ZubayApplication.SHARED_PREFERENCE_WEATHER_IMAGE;

	SharedPreferences sp;
	WeatherService weather;
	String dday;
	private GestureDetector gestureDetector;
	private Thread timeThread;
	private boolean ddayStop_flag = false;
	Typeface font;
	SoundManager mSoundManager;

	int random = 0;

	Handler mHandler = new Handler();

	public static FirstFragment newInstance(int sectionNumber) {
		FirstFragment fragment = new FirstFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {


		mContext = ZubayApplication.getInstance().getApplicationContext();

		//roostView is inflating layout
		rootView = inflater.inflate(
				R.layout.first_activity, container, false);

		sp = PreferenceManager.getDefaultSharedPreferences(rootView.getContext());
		gestureDetector = new GestureDetector(rootView.getContext(), gestureListener);

		weather = new WeatherService();
		//		regid = getRegistrationId(rootView.getContext());

		setView();		// findViewById 설정
		setAnimation();
		syncTime();			// 속도를 위해 처음 시간을 set
		button_ClickListener();
		setSound();

		//battery 리시버 등록
		//rootView.getContext().registerReceiver(this.mBatInfoReceiver,
		//		new IntentFilter(Intent.ACTION_BATTERY_CHANGED));


		weather_icon.setBackgroundResource(changeCon(sp.getString(WEATHER_IMAGE, "맑음")));

		runClock();

		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		//battery 리시버 등록

		// 부재중 전화, 문자 확인
		//		confirmBadge();

		confirmCallMessage();
		setDday();

		mContext.registerReceiver(this.mBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
		weather_icon.setBackgroundResource(changeCon(sp.getString(WEATHER_IMAGE, "맑음")));
	}

	@Override
	public void onPause() {
		super.onPause();
		mContext.unregisterReceiver(this.mBatInfoReceiver);
	}

	private BroadcastReceiver mBatInfoReceiver = new BroadcastReceiver(){
		@Override
		public void onReceive(Context arg0, Intent intent) {
			int level = intent.getIntExtra("level", 0);
			battery.setText(String.valueOf(level) + "%");
		}
	};

	int changeCon(String con) {
		if(con.equals("맑음")) {
			return R.drawable.sunny;
		} else if(con.equals("구름 조금")) {
			return R.drawable.cloud;
		} else if(con.equals("구름 많음")) {
			return R.drawable.many_cloud;
		} else if(con.equals("흐림")) {
			return R.drawable.sunny_cloud;
		} else if(con.equals("비")) {
			return R.drawable.rain;
		} else if(con.equals("눈/비")) {
			return R.drawable.snow_rain;
		} else if(con.equals("눈")) {
			return R.drawable.snow;
		} else {
			return R.drawable.sunny;
		}
	}

	void setView() {
		font = Typeface.createFromAsset(rootView.getContext().getAssets(), "font.ttf");

		time_TextView = (TextView)rootView.findViewById(R.id.time);
		month_TextView = (TextView)rootView.findViewById(R.id.month);
		day_TextView = (TextView)rootView.findViewById(R.id.day);
		dday_text_TextView = (TextView)rootView.findViewById(R.id.dday_text);
		dday_text_TextView.setTypeface(font);
		direct_button = (ImageView)rootView.findViewById(R.id.direct_button);
		direct_framelayout = (FrameLayout)rootView.findViewById(R.id.direct_framelayout);
		direct_videocall = (ImageButton)rootView.findViewById(R.id.direct_videocall);
		direct_call = (ImageButton)rootView.findViewById(R.id.direct_call);
		direct_message = (ImageButton)rootView.findViewById(R.id.direct_message);
		direct_katok = (ImageButton)rootView.findViewById(R.id.direct_katok);
		dday_TextView = (TextView)rootView.findViewById(R.id.dday);
		menu = (FrameLayout)rootView.findViewById(R.id.menu);
		weather_icon = (ImageButton)rootView.findViewById(R.id.weather_image);
		camera = (ImageView)rootView.findViewById(R.id.camera);
		setting = (ImageView)rootView.findViewById(R.id.setting);
		relativeLayout = (RelativeLayout)rootView.findViewById(R.id.relativeLayout1);
		battery = (TextView)rootView.findViewById(R.id.battery);
		countCallMessage = (TextView)rootView.findViewById(R.id.countCallMessage);
		textCallMessage = (TextView)rootView.findViewById(R.id.textCallMessage);

	}

	void setAnimation() {
		direct_button_anim = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.direct_button_anim);
		call_button_anim = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.call_button_anim);
		videocall_button_anim = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.videocall_button_anim);
		message_button_anim = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.message_button_anim);
		katok_button_anim = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.katok_button_anim);
		menu_anim = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.menu_anim);
		menu_close_anim = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.menu_close_anim);
		dday_anim = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.dday_anim);
		dday_text_anim = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.dday_text_anim);
		dday_anim2 = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.dday_anim2);
		dday_text_anim.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
				time_TextView.setVisibility(View.INVISIBLE);
				countCallMessage.setVisibility(View.INVISIBLE);
			}
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			@Override
			public void onAnimationEnd(Animation animation) {
				confirmCallMessage();
				ddayStop_flag = false;
			}
		});
	}
	

	void setSound() {
		mSoundManager = new SoundManager();
		mSoundManager.initSounds(rootView.getContext());
		mSoundManager.addSound(1, R.raw.sweet_favour);
		mSoundManager.addSound(2, R.raw.knock);
		mSoundManager.addSound(3, R.raw.harp_glissando);
	}

	void button_ClickListener() {
		countCallMessage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				PartnerInfo info = new PartnerInfo();

				textCallMessage.setVisibility(View.VISIBLE);	
				menu.setVisibility(View.INVISIBLE);
				menu.startAnimation(menu_close_anim);

				textCallMessage.setText("" + info.getOutCallCount() + "통의 부재중 전화가 있습니다.\n" +
						info.getOutMessageCount() + "통의 새로운 메세지가 있습니다.");
			}
		});
		camera.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent camera = new Intent(rootView.getContext(), com.zubaykids.camera.Camera_mini.class);
				startActivity(camera);
			}
		});
		setting.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent setting = new Intent(rootView.getContext(), com.zubaykids.setting.SettingActivity.class);
				startActivity(setting);
			}
		});
		direct_button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				direct_framelayout.setVisibility(View.VISIBLE);
				direct_framelayout.startAnimation(direct_button_anim);

				direct_call.startAnimation(call_button_anim);
				direct_videocall.startAnimation(videocall_button_anim);
				direct_message.startAnimation(message_button_anim);
				direct_katok.startAnimation(katok_button_anim);

				RectAnimation mainLED = SmartCoverMainActivity.mainLED;
				mainLED.setRepeatCount(3);
				mainLED.start();
				
				mSoundManager.playSound(3);
			}
		});
		direct_framelayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				direct_framelayout.setVisibility(View.INVISIBLE);
			}
		});
		direct_videocall.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent direct_videocall = new Intent("com.android.phone.videocall");
				direct_videocall.putExtra("videocall", true);
				direct_videocall.setData(Uri.parse("tel:" + partnerPhoneNumber));
				startActivity(direct_videocall);
			}
		});
		direct_call.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent direct_call = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + partnerPhoneNumber));
				direct_call.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK); 
				startActivity(direct_call);				
			}
		});
		direct_message.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent direct_message = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + partnerPhoneNumber));
				//message.putExtra( "sms_body", "Test text..." );
				startActivity(direct_message);						
			}
		});
		direct_katok.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				try {
					// 카카오톡 앱 연결
					Intent direct_katok = rootView.getContext().getPackageManager().getLaunchIntentForPackage("com.kakao.talk");
					startActivity(direct_katok);
				} catch(Exception e) {
					// 카카오톡이 설치되어 있지 않을 경우 playstore 카카오톡 다운화면으로 연결
					Intent marketLaunch = new Intent(Intent.ACTION_VIEW); 
					marketLaunch.setData(Uri.parse("https://play.google.com/store/apps/details?id=com.kakao.talk")); 
					startActivity(marketLaunch);
				}				
			}
		});
		weather_icon.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent weather_activity = new Intent(rootView.getContext(),
						com.zubaykids.weather.Weather.class);
				startActivity(weather_activity);
			}
		});

		dday_TextView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				dday_text_TextView.setAlpha(1.0f);
				dday_text_TextView.startAnimation(dday_text_anim);

				ddayStop_flag = true;
				
				RectAnimation mainLED = SmartCoverMainActivity.mainLED;
				mainLED.setRepeatCount(3);
				mainLED.start();
			}
		});

		relativeLayout.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {

				gestureDetector.onTouchEvent(event);

				switch (event.getAction()) {
				case MotionEvent.ACTION_DOWN:
					xAtDown = event.getX();
					yAtDown = event.getY();

					break;

				case MotionEvent.ACTION_UP:

					xAtUp = event.getX();
					yAtUp = event.getY();

					if(yAtDown - yAtUp > SWIPE_MIN_DISTANCE) {
						if(menu.getVisibility() == View.INVISIBLE) {
							menu.setVisibility(View.VISIBLE);
							menu.startAnimation(menu_anim);
						}
					} else if(yAtUp - yAtDown > SWIPE_MIN_DISTANCE) {
						if(menu.getVisibility() == View.VISIBLE) {
							menu.setVisibility(View.INVISIBLE);
							menu.startAnimation(menu_close_anim);
						}
					}
					break;
				}
				return true;
			}
		});

		textCallMessage.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				countCallMessage.setVisibility(View.GONE);
				textCallMessage.setVisibility(View.GONE);

				PartnerInfo info = new PartnerInfo();
				info.setOutCallCount(0);
				info.setOutMessageCount(0);

				time_TextView.setVisibility(View.VISIBLE);
			}
		});
	}

	void confirmCallMessage() {

		PartnerInfo info = new PartnerInfo();

		if(info.getOutCallCount() == 0 && info.getOutMessageCount() == 0) {
			countCallMessage.setVisibility(View.GONE);
			time_TextView.setVisibility(View.VISIBLE);

		} else {
			countCallMessage.setVisibility(View.VISIBLE);
			time_TextView.setVisibility(View.GONE);

			countCallMessage.setText("" + info.getOutCallCount() + " / " + info.getOutMessageCount());
		}
	}

	void syncTime() {

		cal = Calendar.getInstance();
		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int minute = cal.get(Calendar.MINUTE);

		String month_String;
		String day_String;
		String hour_String;
		String minute_String;

		switch(month) {
		case 0 :
			month_String = "JAN";
			break;
		case 1 :
			month_String = "FEB";
			break;
		case 2 :
			month_String = "MAR";
			break;
		case 3 :
			month_String = "APR";
			break;
		case 4 :
			month_String = "MAY";
			break;
		case 5 :
			month_String = "JUN";
			break;
		case 6 :
			month_String = "JUL";
			break;
		case 7 :
			month_String = "AUG";
			break;
		case 8 :
			month_String = "SEP";
			break;
		case 9 :
			month_String = "OCT";
			break;
		case 10 :
			month_String = "NOV";
			break;
		case 11 :
			month_String = "DEC";
			break;
		default :
			month_String = "ERR";
			break;
		}


		if(hour == 12) {
			hour_String = "12";
		}
		else if(hour%12 < 10) {
			hour_String = "0" + hour%12;
		}
		else {
			hour_String = "" + hour%12;
		}

		if(minute < 10) {
			minute_String = "0" + minute;
		}
		else {
			minute_String = "" + minute;
		}

		if(day < 10) {
			day_String = "0" + day;
		}
		else {
			day_String = "" + day;
		}

		time_TextView.setText(hour_String + "  " + minute_String);
		month_TextView.setText(month_String);
		day_TextView.setText(day_String);
	}

	void runClock() {
		// 시계 구현
		timeThread = new Thread	() {
			@Override
			public void run() {
				while(true) {
					mHandler.post(new Runnable() {

						@Override
						public void run() {
							if(!ddayStop_flag) {
								syncTime();
								confirmCallMessage();
							}
						}
					});
					try {
						Thread.sleep(1000);
					}
					catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
		};
		timeThread.start();
	}

	void setDday() {
		DdayMediator mMediator = new DdayMediator();
		if(mMediator.findDdayByNumber(1) == null){
			return;
		}
		dday_TextView.setText(mMediator.findDdayByNumber(1).getDdayString());
		dday_text_TextView.setText(mMediator.findDdayByNumber(1).getTitle());
	}


	// 감시할 모션이벤트를 onTouchEvent에 넣어주면, GestureListener가 호출된다.
	public boolean onTouchEvent(MotionEvent event) {
		return gestureDetector.onTouchEvent(event);
	}

	// SimpleOnGestureListener callback
	SimpleOnGestureListener gestureListener = new SimpleOnGestureListener() {

		@Override
		public boolean onDoubleTap(MotionEvent ev) {
			if(menu.getVisibility() == View.INVISIBLE) {
				menu.setVisibility(View.VISIBLE);
				menu.startAnimation(menu_anim);
			} else {
				menu.setVisibility(View.INVISIBLE);
				menu.startAnimation(menu_close_anim);
			}
			return true;
		}
	};
}