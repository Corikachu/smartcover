package com.zubaykids.initapp.view;

import java.util.ArrayList;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v13.app.FragmentPagerAdapter;

import com.zubaykids.initapp.ZubayApplication;

public class SectionsPagerAdapter extends FragmentPagerAdapter {

	private Context context;
	private ArrayList<Fragment> array;
	
	private Fragment zero;
	private Fragment first;
	private Fragment second;
	private Fragment third;

	public SectionsPagerAdapter(FragmentManager fm) {
		super(fm);
		context = ZubayApplication.getInstance().getApplicationContext();
		
		array = new ArrayList<Fragment>();
		
		zero = new ZeroFragment();
		first = new FirstFragment();
		second = new SecondFragment();
		third = new ThirdFragment();
		
		array.add(zero);
		array.add(first);
		array.add(second);
		array.add(third);
	}

	@Override
	public Fragment getItem(int position) {
		return array.get(position);
	}

	@Override
	public int getCount() {
		return array.size();
	}
	
	/*
	@Override
	public int getItemPosition(Object object) {
		if(object instanceof SecondFragment){
			return POSITION_NONE;
		}
		return super.getItemPosition(object);
	}
	*/
}