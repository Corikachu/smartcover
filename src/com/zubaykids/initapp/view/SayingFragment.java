package com.zubaykids.initapp.view;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.zubaykids.smartcover.R;

public class SayingFragment extends Fragment {

	public static SayingFragment newInstance(){
		SayingFragment frag = new SayingFragment();
		
		return frag;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_saying, container, false);
		
		TextView saying = (TextView)view.findViewById(R.id.saying_textview);
		
		
		return view;
	}
}
