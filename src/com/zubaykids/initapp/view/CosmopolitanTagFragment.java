package com.zubaykids.initapp.view;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.zubaykids.initapp.ZubayApplication;
import com.zubaykids.smartcover.R;

public class CosmopolitanTagFragment extends Fragment {
	 JSONObject jsonObject;
	 String href="http://cosmopolitan.joins.com/search/index.asp?sintSno=strTitle&strSearchTxt=";
	 String content[]= new String[10];
	 SharedPreferences bef_pref;
	 SharedPreferences.Editor bef_editor;
	 
	private Context context;
	View view;

	public static CosmopolitanTagFragment newInstance() {
		CosmopolitanTagFragment fragment = new CosmopolitanTagFragment();
		return fragment;
	}
	
	public CosmopolitanTagFragment() {
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		 view = inflater.inflate(R.layout.fragment_cosmopolitan_tag, container, false);
		 context =ZubayApplication.getInstance().getApplicationContext();
		 bef_pref = PreferenceManager.getDefaultSharedPreferences(context);
		 bef_editor=bef_pref.edit();
		
		 Log.v("TAGS_Oncreate Repeated",""+bef_pref.getBoolean("tags_repeated",false));
		 Log.v("TAGS_Oncreate Repeated",""+bef_pref.getBoolean("tags_repeated",true));
		 
		 
		 if(bef_pref.getBoolean("tags_repeated", true)) // 1) 초기 : if조건 참! --> async 실행
		 {											//  2) 값이 바뀌었다!--> async실행
			Log.v("TAGS_oncreate","초기");

			cosmopolitan_TAGS();
		 }
		 else //값이 바뀌지 않았다! pref 안에 저장되어 있는 img_url로 asynctask onPostExecute에서 하는 작업을 바로 실행
		 {    		
			Log.v("TAGS_oncreate","반복");
     		NoScrollGridView gridView = (NoScrollGridView)view.findViewById(R.id.cosmopolitan_tag_gridview);
     		gridView.setVerticalScrollBarEnabled(false);
     		
     		gridView.setAdapter(new TagAdapter());
		 }

		return view;
	}
    public void parseTAGS_JSON(String json) throws JSONException
    {
    	 try {    	
    	        jsonObject = new JSONObject(json);
    	    	for(int i=1;i<11;i++)
    	    	{
    	    		content[i-1]=jsonObject.getString("content_"+i);
    	    		bef_editor.putString(""+(i-1), content[i-1]);
    	    		bef_editor.commit();
    	    	}
    	    	bef_editor.putBoolean("tags_repeated", false);
    	    	bef_editor.commit();
    	    } catch (JSONException e) {
    	    	Log.v("5555","JSONException");
    	        e.printStackTrace();
    	    }
    }
    
	
    public void cosmopolitan_TAGS()
    {
		Log.v("TAGS_cosmopolitan","start!");
        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... params) {
        		Log.v("TAGS_doInBackground","start!");

                String msg="";
                
            	DefaultHttpClient httpclient = new DefaultHttpClient();
            	
            	HttpPost httpPost= new HttpPost("http://172.16.100.172:52273/tags"); //서버 IP
			
                try {
                    HttpResponse response = httpclient.execute(httpPost);

                    String json_string = EntityUtils.toString(response.getEntity());

                    parseTAGS_JSON(json_string);  

                } catch (ClientProtocolException e) {
						Log.v("ClientProtocolException", "ClientProtocolException");
                	e.printStackTrace();
                } catch (IOException e) {
						Log.v("IOException", "IOException");
                	e.printStackTrace();
				} catch (JSONException e) {
						Log.v("JSONException", "JSONException");
					e.printStackTrace();
				}
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
            	Log.v("onPostExecute","");
            	
        		NoScrollGridView gridView = (NoScrollGridView)view.findViewById(R.id.cosmopolitan_tag_gridview);
        		gridView.setVerticalScrollBarEnabled(false);
        		
        		gridView.setAdapter(new TagAdapter());
            }
        		};
        if(Build.VERSION.SDK_INT >= 11)
        	task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
        	task.execute();
    }
	
	
	private class TagAdapter extends BaseAdapter{

		ArrayList<Item> array;
		
		public TagAdapter() {
			array = new ArrayList<Item>();
			
			array.add(new Item("     "+(1)+"위)"+bef_pref.getString(""+(0), "init"),href+bef_pref.getString(""+(0), "init")));
			array.add(new Item("     "+(6)+"위)"+bef_pref.getString(""+(5), "init"),href+bef_pref.getString(""+(5), "init")));
			array.add(new Item("     "+(2)+"위)"+bef_pref.getString(""+(1), "init"),href+bef_pref.getString(""+(1), "init")));
			array.add(new Item("     "+(7)+"위)"+bef_pref.getString(""+(6), "init"),href+bef_pref.getString(""+(6), "init")));
			array.add(new Item("     "+(3)+"위)"+bef_pref.getString(""+(2), "init"),href+bef_pref.getString(""+(2), "init")));
			array.add(new Item("     "+(8)+"위)"+bef_pref.getString(""+(7), "init"),href+bef_pref.getString(""+(7), "init")));
			array.add(new Item("     "+(4)+"위)"+bef_pref.getString(""+(3), "init"),href+bef_pref.getString(""+(3), "init")));
			array.add(new Item("     "+(9)+"위)"+bef_pref.getString(""+(8), "init"),href+bef_pref.getString(""+(8), "init")));
			array.add(new Item("     "+(5)+"위)"+bef_pref.getString(""+(4), "init"),href+bef_pref.getString(""+(4), "init")));
			array.add(new Item("     "+(10)+"위)"+bef_pref.getString(""+(9), "init"),href+bef_pref.getString(""+(9), "init")));

		}
		
		@Override
		public int getCount() {
			return array.size();
		}

		@Override
		public Object getItem(int position) {
			return array.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			TextView t;
			t = new TextView(context);
			t.setLayoutParams(new GridView.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
			
			t.setPadding(5, 5, 5, 5);
			t.setText(array.get(position).getTag());
			t.setTextSize(20f);
			//t.setGravity(Gravity.CENTER);
			t.setTag(position);
			
			if(position%2 == 0){
				t.setTextColor(Color.parseColor("#FAEEAC"));
			}else{
				t.setTextColor(Color.parseColor("#FFCDCF"));
			}
			Typeface fontType = Typeface.createFromAsset(context.getAssets(), "font.ttf");
			t.setTypeface(fontType);
			
			final int pos = position;
			t.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					
					int position = (int) v.getTag();
					Log.v("position",position+"");
					Log.v("onClick","");
		        	FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
		          	
		        	SearchWebViewFragment webview = new SearchWebViewFragment();
		        	webview.setUrl(array.get(pos).getUrl());
		        	fragmentTransaction.replace(R.id.cosmopolitan_article_parent_layout, webview,"COSMO_TAGS");
		        	fragmentTransaction.commit();
				}
			});

			return t;
		}
		
		private class Item {
			String tag;
			String url;
			
			public Item(String string, String url) {
				this.tag = string;
				this.url = url;
			}

			public String getTag() {
				return tag;
			}

			public void setTag(String string) {
				this.tag = string;
			}

			public String getUrl() {
				return url;
			}

			public void setUrl(String url) {
				this.url = url;
			}
		}
	}
	
}
