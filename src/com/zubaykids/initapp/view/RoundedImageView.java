package com.zubaykids.initapp.view;

/*
 * RoundedImageView.
 * This class extends ImageView
 * 1:1 ratio Image is shown circle by this class
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class RoundedImageView extends ImageView {

	private int mCanvasSize;
	private Paint mPaintBorder;
	private int mBorderWidth;

	public RoundedImageView(Context context) {
		super(context);
		setPaints();
	}
	
	public RoundedImageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		setPaints();
	}

	public RoundedImageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setPaints();
	}
	
	public void setPaints(){
		mPaintBorder = new Paint();
		mPaintBorder.setAntiAlias(true);
	}

	public void setBorderWidth(int borderWidth) {
		this.mBorderWidth = borderWidth;
		
		requestLayout();
		invalidate();
	}

	public void setBorderColor(String borderColor) {
		if (mPaintBorder != null) {
			mPaintBorder.setColor(Color.parseColor(borderColor));
		}
		invalidate();
	}

	public void addShadow() {
		setLayerType(LAYER_TYPE_SOFTWARE, mPaintBorder);
		mPaintBorder.setShadowLayer(4.0f, 0.0f, 2.0f, Color.BLACK);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		Drawable drawable = getDrawable();
		if (drawable == null) {
			return;
		}
		if (getWidth() == 0 || getHeight() == 0) {
			return;
		}
		
		int height = canvas.getHeight();
		int width = canvas.getWidth();
		
		Bitmap getBitmap = ((BitmapDrawable) drawable).getBitmap();
		Bitmap bitmap = getBitmap.copy(Bitmap.Config.ARGB_8888, true);

		int circleCenter = (height + (mBorderWidth*2))/2;
		int drawc =  circleCenter+mBorderWidth;
		
		canvas.drawCircle(width/2, height/2, circleCenter-20.0f, mPaintBorder);
		Bitmap roundBitmap = getCroppedBitmap(bitmap, getWidth());
		canvas.drawBitmap(roundBitmap, 0, 0, null);
	}
	
	
	public static Bitmap getCroppedBitmap(Bitmap bmp, int radius) {
		Bitmap sbmp;
		if (bmp.getWidth() != radius || bmp.getHeight() != radius) {
			sbmp = Bitmap.createScaledBitmap(bmp, radius, radius, false);
		} else
			sbmp = bmp;
		Bitmap output = Bitmap.createBitmap(sbmp.getWidth(), sbmp.getHeight(),
				Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final Paint mPaint = new Paint();
		final Rect rect = new Rect(0, 0, sbmp.getWidth(), sbmp.getHeight());
		
		mPaint.setAntiAlias(true);
		mPaint.setDither(true);
		mPaint.setFilterBitmap(true);
		canvas.drawARGB(0, 0, 0, 0);
		//mPaint.setAlpha(0);
		canvas.drawCircle(sbmp.getWidth() / 2 - 0.7f,
				sbmp.getHeight() / 2 - 0.7f, sbmp.getWidth() / 2 - 15f, mPaint);
		mPaint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(sbmp, rect, rect, mPaint);

		return output;
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// This method must call setMeasuredDimension.
		int width = measureViewWidth(widthMeasureSpec);
		int height = measureViewHeight(heightMeasureSpec);
		setMeasuredDimension(width, height);
	}

	private int measureViewWidth(int measureSpecWidth) {
		int result = 0;
		int specMode = MeasureSpec.getMode(measureSpecWidth);
		int specSize = MeasureSpec.getSize(measureSpecWidth);

		if (specMode == MeasureSpec.EXACTLY || specMode == MeasureSpec.AT_MOST) {
			// The parent has determined an exact size for child.
			// Or the child can be as large as it wants up the specified size.
			result = specSize;
		} else {
			// The parent has not imposed any constraint on the Child.
			result = mCanvasSize;
		}

		return result;
	}

	private int measureViewHeight(int measureSpecHeight) {
		int result = 0;
		int specMode = MeasureSpec.getMode(measureSpecHeight);
		int specSize = MeasureSpec.getSize(measureSpecHeight);

		if (specMode == MeasureSpec.EXACTLY || specMode == MeasureSpec.AT_MOST) {
			// The parent has determined an exact size for child.
			// Or the child can be as large as it wants up the specified size.
			result = specSize;
		} else {
			// The parent has not imposed any constraint on the Child.
			result = mCanvasSize;
		}

		return result;
	}

}
