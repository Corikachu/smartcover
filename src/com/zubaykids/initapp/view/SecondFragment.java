package com.zubaykids.initapp.view;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.zubaykids.SaveInfo.PartnerInfo;
import com.zubaykids.bottomAnimation.moveRect.RectAnimation;
import com.zubaykids.initapp.SmartCoverMainActivity;
import com.zubaykids.initapp.ZubayApplication;
import com.zubaykids.smartcover.R;
import com.zubaykids.sound.SoundManager;
import com.zubaykids.start.StartPage1;

public class SecondFragment extends Fragment {
	private static final String ARG_SECTION_NUMBER = "section_number";
	private RoundedImageView myPicture;
	private RoundedImageView partnerPicture;
	private TextView keyWord_1;
	private TextView keyWord_2;
	private TextView keyWord_3;
	private TextView keyWord_4;
	private TextView keyWord_5;
	private Button keyword_button_1;
	private Button keyword_button_2;
	private Button keyword_button_3;
	private Button keyword_button_4;
	private Button keyword_button_5;
	private Button back_button;
	private ImageView kok_heart_partner_send;
	private ImageView kok_heart_my_send;
	private View rootView;
	private ImageButton heart;

	private Animation keyword_start_anim1;
	private Animation keyword_start_anim2;
	private Animation keyword_start_anim3;
	private Animation keyword_start_anim4;
	private Animation keyword_start_anim5;
	private Animation keyword_end_anim1;
	private Animation keyword_end_anim2;
	private Animation keyword_end_anim3;
	private Animation keyword_end_anim4;
	private Animation keyword_end_anim5;
	private Animation kok_partner_anim;
	private Animation kok_my_anim;
	private Animation kok_my_heart_anim;
	private Animation kok_partner_anim_send;
	private int keyWord_deep = 0;
	private String[] keyword_first = {"2014년연말", "크리스마스", "2015년새해", "화이트데이", "발렌타인데이"};
	private String[] keyword_second = {"선물", "여행지", "데이트", "날짜", "음식"};
	private String select_first;
	private String select_second;	
	private int kok_partner_twinkle_count = 0;
	private boolean inner_kok = false;

	Handler mHandler;
	String regid;
	String yourregID;
	SoundManager mSoundManager;
	public static final String PROPERTY_REG_ID = "registration_id";
	SharedPreferences mPref;

	private FragmentManager fragmentManager;
	private FragmentTransaction fragmentTransaction;
	private Typeface font;

	public static SecondFragment newInstance(int sectionNumber) {
		SecondFragment fragment = new SecondFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		//rootView is inflating layout
		rootView = inflater.inflate(
				R.layout.second_activity, container, false);

		mHandler = new Handler();

		setView();
		setAnimation();
		clickListener();
		getRegId();

		confirmKok();

		setSound();
		
		fragmentManager = getFragmentManager();
		fragmentTransaction = fragmentManager.beginTransaction();

		try{
			InputStream my_is = rootView.getContext().openFileInput("MY_PICTURE");
			Bitmap my_b = BitmapFactory.decodeStream(my_is);
			Drawable my_drawable = new BitmapDrawable(my_b);
			myPicture.setImageDrawable(my_drawable);
		} catch(Exception e) {
			e.printStackTrace();
		}
		try {			
			InputStream partner_is = rootView.getContext().openFileInput("PARTNER_PICTURE");
			Bitmap partner_b = BitmapFactory.decodeStream(partner_is);
			Drawable partner_drawable = new BitmapDrawable(partner_b);
			partnerPicture.setImageDrawable(partner_drawable);		
		} catch(Exception e) {
			e.printStackTrace();
		}

		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();

		rangeText();

		keyWord_deep = 0;
		
		myPicture.setImageResource(R.drawable.default_profile_man);
		partnerPicture.setImageResource(R.drawable.default_profile_girl);
		
		if(mPref.getString("SEX", "0").equals("1")) {
			myPicture.setImageResource(R.drawable.default_profile_girl);
			partnerPicture.setImageResource(R.drawable.default_profile_man);
		}

		try{
			InputStream my_is = rootView.getContext().openFileInput("MY_PICTURE");
			Bitmap my_b = BitmapFactory.decodeStream(my_is);
			Drawable my_drawable = new BitmapDrawable(my_b);
			myPicture.setImageDrawable(my_drawable);
		} catch(Exception e) {
			e.printStackTrace();
		}
		try {			
			InputStream partner_is = rootView.getContext().openFileInput("PARTNER_PICTURE");
			Bitmap partner_b = BitmapFactory.decodeStream(partner_is);
			Drawable partner_drawable = new BitmapDrawable(partner_b);
			partnerPicture.setImageDrawable(partner_drawable);		
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	void setView() {
		myPicture = (RoundedImageView)rootView.findViewById(R.id.profile_image_my);
		myPicture.setBorderWidth(15);
		myPicture.setBorderColor("#a9e9ee");
		myPicture.addShadow();
		partnerPicture = (RoundedImageView)rootView.findViewById(R.id.profile_image_parter);
		partnerPicture.setBorderWidth(15);
		partnerPicture.setBorderColor("#FFCDCF");
		partnerPicture.addShadow();

		mPref = PreferenceManager.getDefaultSharedPreferences(rootView.getContext());

		if(mPref.getString("SEX", "0").equals("1")) {
			myPicture.setBorderColor("#FFCDCF");
			//			myPicture.setImageResource(R.drawable.default_profile_girl);
			partnerPicture.setBorderColor("#a9e9ee");
			//			partnerPicture.setImageResource(R.drawable.default_profile_man);
		}

		keyWord_1 = (TextView)rootView.findViewById(R.id.keyWord_1);
		keyWord_2 = (TextView)rootView.findViewById(R.id.keyWord_2);
		keyWord_3 = (TextView)rootView.findViewById(R.id.keyWord_3);
		keyWord_4 = (TextView)rootView.findViewById(R.id.keyWord_4);
		keyWord_5 = (TextView)rootView.findViewById(R.id.keyWord_5);
		font = Typeface.createFromAsset(rootView.getContext().getAssets(), "font.ttf");
		keyWord_1.setTypeface(font);
		keyWord_2.setTypeface(font);
		keyWord_3.setTypeface(font);
		keyWord_4.setTypeface(font);
		keyWord_5.setTypeface(font);


		keyword_button_1 = (Button)rootView.findViewById(R.id.keyWord_button_1);
		keyword_button_2 = (Button)rootView.findViewById(R.id.keyWord_button_2);
		keyword_button_3 = (Button)rootView.findViewById(R.id.keyWord_button_3);
		keyword_button_4 = (Button)rootView.findViewById(R.id.keyWord_button_4);
		keyword_button_5 = (Button)rootView.findViewById(R.id.keyWord_button_5);
		back_button = (Button)rootView.findViewById(R.id.back_button);

		kok_heart_partner_send = (ImageView)rootView.findViewById(R.id.kok_heart_partner_send);
		kok_heart_my_send = (ImageView)rootView.findViewById(R.id.kok_heart_my_send);
		heart = (ImageButton)rootView.findViewById(R.id.second_layout_heart);

	}

	void setAnimation() {
		keyword_start_anim1 = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.keyword_start_anim1);
		keyword_start_anim2 = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.keyword_start_anim2);
		keyword_start_anim3 = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.keyword_start_anim3);
		keyword_start_anim4 = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.keyword_start_anim4);
		keyword_start_anim5 = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.keyword_start_anim5);
		keyword_end_anim1 = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.keyword_end_anim1);
		keyword_end_anim2 = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.keyword_end_anim2);
		keyword_end_anim3 = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.keyword_end_anim3);
		keyword_end_anim4 = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.keyword_end_anim4);
		keyword_end_anim5 = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.keyword_end_anim5);

		kok_partner_anim_send = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.kok_twinkle_partner_anim2);
		kok_partner_anim = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.kok_partner_anim);
		kok_my_anim = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.kok_my_anim);
		kok_my_heart_anim = AnimationUtils.loadAnimation(rootView.getContext(), R.anim.kok_my_anim);

		setKeywordAnimation keyword_anim = new setKeywordAnimation();
		keyword_end_anim1.setAnimationListener(keyword_anim);
		keyword_end_anim2.setAnimationListener(keyword_anim);
		keyword_end_anim3.setAnimationListener(keyword_anim);
		keyword_end_anim4.setAnimationListener(keyword_anim);
		keyword_end_anim5.setAnimationListener(keyword_anim);

		// 하트 날라가는 animation
		kok_partner_anim_send.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			@Override
			public void onAnimationEnd(Animation animation) {
				heart.setVisibility(View.VISIBLE);
				partnerPicture.startAnimation(kok_partner_anim);
			}
		});
		// 하트를 보냈을때 사진이 컸다 작아졌다.
		kok_partner_anim.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			@Override
			public void onAnimationEnd(Animation animation) {
				if(kok_partner_twinkle_count < 2) {
					partnerPicture.startAnimation(kok_partner_anim);
					kok_partner_twinkle_count++;
				}
			}
		});
		kok_my_anim.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			@Override
			public void onAnimationEnd(Animation animation) {
				if(inner_kok == true)
					partnerPicture.startAnimation(kok_my_anim);
			}
		});
		kok_my_heart_anim.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			@Override
			public void onAnimationEnd(Animation animation) {
				if(inner_kok == true)
					kok_heart_my_send.startAnimation(kok_my_heart_anim);
			}
		});
	}
	
	void setSound() {
		mSoundManager = new SoundManager();
		mSoundManager.initSounds(rootView.getContext());
		mSoundManager.addSound(1, R.raw.sweet_favour);
		mSoundManager.addSound(2, R.raw.knock);
	}

	class setKeywordAnimation implements AnimationListener {
		@Override
		public void onAnimationStart(Animation animation) {
		}
		@Override
		public void onAnimationRepeat(Animation animation) {				
		}
		@Override
		public void onAnimationEnd(Animation animation) {

			if(keyWord_deep == 0) {
				keyWord_1.setText(keyword_first[0]);
				keyWord_2.setText(keyword_first[1]);
				keyWord_3.setText(keyword_first[2]);
				keyWord_4.setText(keyword_first[3]);
				keyWord_5.setText(keyword_first[4]);
			} else if(keyWord_deep == 1) {
				keyWord_1.setText(keyword_second[0]);
				keyWord_2.setText(keyword_second[1]);
				keyWord_3.setText(keyword_second[2]);
				keyWord_4.setText(keyword_second[3]);
				keyWord_5.setText(keyword_second[4]);
			}
			if(animation == keyword_end_anim1) keyWord_1.startAnimation(keyword_start_anim1);
			else if(animation == keyword_end_anim2) keyWord_2.startAnimation(keyword_start_anim2);
			else if(animation == keyword_end_anim3) keyWord_3.startAnimation(keyword_start_anim3);
			else if(animation == keyword_end_anim4) keyWord_4.startAnimation(keyword_start_anim4);
			else if(animation == keyword_end_anim5) keyWord_5.startAnimation(keyword_start_anim5);
		}
	}

	private void clickListener() {
		KeyWordClickListener listener = new KeyWordClickListener();
		keyword_button_1.setOnClickListener(listener);
		keyword_button_2.setOnClickListener(listener);
		keyword_button_3.setOnClickListener(listener);
		keyword_button_4.setOnClickListener(listener);
		keyword_button_5.setOnClickListener(listener);
		back_button.setOnClickListener(listener);
		myPicture.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				PartnerInfo.kok = false;
				inner_kok = false;
			}
		});
		heart.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				heart.setVisibility(View.INVISIBLE);
				kok_partner_twinkle_count = 0;
				kok_heart_partner_send.startAnimation(kok_partner_anim_send);

				RectAnimation mainLED = SmartCoverMainActivity.mainLED;
				mainLED.setRepeatCount(3);
				mainLED.start();

				sendGCM();

				Log.v("JangSound", "playSound");
				mSoundManager.playSound(1);
				Log.v("JangSound", "playSound2");

			}
		});
	}

	private class KeyWordClickListener implements View.OnClickListener{

		@Override
		public void onClick(View v) {

			String text;

			// back button을 눌렀을 때
			if(v.getId() == R.id.back_button) {
				if(keyWord_deep > 0) keyWord_deep = 0;
				resetLocation();
			} else {

				// 어떤 버튼을 클릭했는지
				if(v.getId() == R.id.keyWord_button_1) { 
					text = keyWord_1.getText().toString(); 
				} else if(v.getId() == R.id.keyWord_button_2) { 
					text = keyWord_2.getText().toString(); 
				} else if(v.getId() == R.id.keyWord_button_3) { 
					text = keyWord_3.getText().toString(); 
				} else if(v.getId() == R.id.keyWord_button_4) { 
					text = keyWord_4.getText().toString(); 
				} else {
					text = keyWord_5.getText().toString();
				}

				if(keyWord_deep == 0) {
					select_first = text;
					keyWord_deep++;
				}
				else if(keyWord_deep == 1) {
					select_second = text;

					SearchWebViewFragment webview = new SearchWebViewFragment();
					webview.setKeyword(select_first + " " + select_second);
					webview.setTitle(select_first + " " + select_second);
					FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
					fragmentTransaction.replace(R.id.second_page_parent_layout, webview, ZubayApplication.TAG_FRAGMENT_SEARCH_WEBVIEW);
					fragmentTransaction.addToBackStack(null);
					fragmentTransaction.commit();

					keyWord_deep = 0;

				}


				resetLocation();
			}
			if(keyWord_deep == 0) {
				back_button.setVisibility(View.GONE);
			} else {
				back_button.setVisibility(View.VISIBLE);
			}
		}
	}

	void rangeText() {

		keyWord_1.setText(keyword_first[0]);
		keyWord_2.setText(keyword_first[1]);
		keyWord_3.setText(keyword_first[2]);
		keyWord_4.setText(keyword_first[3]);
		keyWord_5.setText(keyword_first[4]);

		setLocation();
	}

	void setLocation() {
		keyWord_1.startAnimation(keyword_start_anim1);
		keyWord_2.startAnimation(keyword_start_anim2);
		keyWord_3.startAnimation(keyword_start_anim3);
		keyWord_4.startAnimation(keyword_start_anim4);
		keyWord_5.startAnimation(keyword_start_anim5);
	}


	void resetLocation() {
		keyWord_1.startAnimation(keyword_end_anim1);
		keyWord_2.startAnimation(keyword_end_anim2);
		keyWord_3.startAnimation(keyword_end_anim3);
		keyWord_4.startAnimation(keyword_end_anim4);
		keyWord_5.startAnimation(keyword_end_anim5);
	}

	// 콕찔렸을 때 broadcast에서 PartnerInfo.kok true로 변경		
	void confirmKok() {
		new Thread() {
			@Override
			public void run() {
				while(true) {
					mHandler.post(new Runnable() {

						@Override
						public void run() {
							if(PartnerInfo.kok == true) {
								Log.v("kok", "kok");

								if(!inner_kok) { 
									myPicture.startAnimation(kok_my_anim);
									kok_heart_my_send.startAnimation(kok_my_heart_anim);
									mSoundManager.playSound(2);
								}
								inner_kok = true;
							}
						}
					});
					try {
						Thread.sleep(1000);
					}
					catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
	}


	public void sendGCM()
	{
		Log.v("sendGCM","go!");
		AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";

				DefaultHttpClient httpclient = new DefaultHttpClient();

				HttpPost httppost = new HttpPost("http://172.16.100.172:52273/push");
				Log.v("sendGCM",yourregID);
				Log.v("sendGCM",regid);
				ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("yourregID", yourregID));
				try {
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					Log.v("UnsupportedEncodingException", "");
					e1.printStackTrace();
				}
				try {  
					Log.v("sendGCM Response", "Before");
					HttpResponse httpresponse = httpclient.execute(httppost);
					Log.v("sendGCM Response","After");
				} catch (ClientProtocolException e) {
					Log.v("ClientProtocolException", "ClientProtocolException");
					e.printStackTrace();
					// TODO Auto-generated catch block
				} catch (IOException e) {
					Log.v("IOException", "IOException");
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return msg;
			}
			@Override
			protected void onPostExecute(String msg) {
			}
		};

		if(Build.VERSION.SDK_INT >= 11)
			task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		else
			task.execute();

	}




	private SharedPreferences getGcmPreferences(Context context) 
	{
		// This sample app persists the registration ID in shared preferences, but
		// how you store the regID in your app is up to you.
		return rootView.getContext().getSharedPreferences(StartPage1.class.getSimpleName(),
				Context.MODE_PRIVATE);
	}

	private static int getAppVersion(Context context) 
	{
		try 
		{
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	void getRegId() {
		SharedPreferences mPref = PreferenceManager.getDefaultSharedPreferences(rootView.getContext());
		regid = mPref.getString("MYREGID", "000");
		yourregID = mPref.getString("YOURREGID", "000");
	}
}