package com.zubaykids.initapp.view;

import java.util.ArrayList;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.support.v13.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;

import com.zubaykids.SaveInfo.PartnerInfo;
import com.zubaykids.initapp.ZubayApplication;
import com.zubaykids.smartcover.R;

public class ThirdFragment extends Fragment {
	private static final String ARG_SECTION_NUMBER = "section_number";
	private View rootView;
	private VerticalViewPager mViewPager;
	private SectionsThirdPagerAdapter pagerAdapter;
	private FrameLayout mBackgroundFrameLayoutDark;
	private Animation mDarkerAnimation;
	
	private static final float MIN_SCALE = 0.75f;
    private static final float MIN_ALPHA = 0.75f;

	private Context context = ZubayApplication.getInstance()
			.getApplicationContext();

	public static ThirdFragment newInstance(int sectionNumber) {
		ThirdFragment fragment = new ThirdFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	public ThirdFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		// rootView is inflating layout
		rootView = inflater.inflate(R.layout.fragment_third_viewpager, container, false);
		mViewPager = (VerticalViewPager)rootView.findViewById(R.id.third_viewpager);
		
		FragmentManager frag = getChildFragmentManager();
		pagerAdapter = new SectionsThirdPagerAdapter(frag);
		mViewPager.setAdapter(pagerAdapter);
		mViewPager.setPageTransformer(true, new ViewPager.PageTransformer() {
			
			@Override
			public void transformPage(View view, float position) {
				int pageWidth = view.getWidth();
                int pageHeight = view.getHeight();

                if (position < -1) { // [-Infinity,-1)
                    // This page is way off-screen to the left.
                    view.setAlpha(0);

                } else if (position <= 1) { // [-1,1]
                    // Modify the default slide transition to shrink the page as well
                    float scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position));
                    float vertMargin = pageHeight * (1 - scaleFactor) / 2;
                    float horzMargin = pageWidth * (1 - scaleFactor) / 2;
                    if (position < 0) {
                        view.setTranslationY(vertMargin - horzMargin / 2);
                    } else {
                        view.setTranslationY(-vertMargin + horzMargin / 2);
                    }

                    // Scale the page down (between MIN_SCALE and 1)
                    view.setScaleX(scaleFactor);
                    view.setScaleY(scaleFactor);

                    // Fade the page relative to its size.
                    view.setAlpha(MIN_ALPHA +
                            (scaleFactor - MIN_SCALE) /
                                    (1 - MIN_SCALE) * (1 - MIN_ALPHA));

                } else { // (1,+Infinity]
                    // This page is way off-screen to the right.
                    view.setAlpha(0);
                }
			}
		});
		
		
		mBackgroundFrameLayoutDark = (FrameLayout)rootView.findViewById(R.id.framelayout_darker_thirdlayout);
		mDarkerAnimation = AnimationUtils.loadAnimation(context, R.anim.layout_darker);
		mBackgroundFrameLayoutDark.startAnimation(mDarkerAnimation);
		mBackgroundFrameLayoutDark.setVisibility(View.VISIBLE);
		
		return rootView;
	}
	
	


	private class SectionsThirdPagerAdapter extends FragmentPagerAdapter{

		private ArrayList<Fragment> mFragmentArrayList;
		private Fragment mCosmopolitanTagFragment;
		private Fragment mCosmopolitanArticleFragment;
		private Fragment mDdayFragment;
		private Fragment mSayingFragment;
		private Fragment mCoupleIndexFragment;
		
		public SectionsThirdPagerAdapter(FragmentManager fm) {
			super(fm);
			createChildrenView();
			mFragmentArrayList = new ArrayList<Fragment>();
			mFragmentArrayList.add(mDdayFragment);
			mFragmentArrayList.add(mCoupleIndexFragment);
			//mFragmentArrayList.add(mSayingFragment);
			mFragmentArrayList.add(mCosmopolitanTagFragment);
			mFragmentArrayList.add(mCosmopolitanArticleFragment);

		}
		
		@Override
		public Fragment getItem(int position) {
			return mFragmentArrayList.get(position);
		}

		@Override
		public int getCount() {
			return mFragmentArrayList.size();
		}

		private void createChildrenView(){
			mCosmopolitanTagFragment = new CosmopolitanTagFragment();
			mCosmopolitanArticleFragment = new CosmopolitanArticleFragment();
			mDdayFragment = new TodayFragment();
			mSayingFragment = new SayingFragment();
			mCoupleIndexFragment = new CoupleIndexFragment();
		}
	}

	@Override
	public View getView() {
		return super.getView();
	}

	@Override
	public void onResume() {
		super.onResume();
	}
	
	
}