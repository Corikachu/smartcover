package com.zubaykids.initapp.view;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.Base64;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.zubaykids.initapp.ZubayApplication;
import com.zubaykids.smartcover.R;

public class CosmopolitanArticleFragment extends Fragment{
	 JSONObject jsonObject;
	 String img_url="";
	 String href;
	 String title="";
	 String date="";
	 
	 Bitmap bmp;
	 RelativeLayout layout;
	 Context context;
	 SharedPreferences bef_pref;
	 SharedPreferences.Editor bef_editor;
	public static CosmopolitanArticleFragment newInstance() {
		CosmopolitanArticleFragment fragment = new CosmopolitanArticleFragment();
		return fragment;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		 context =ZubayApplication.getInstance().getApplicationContext();
		 bef_pref = PreferenceManager.getDefaultSharedPreferences(context);
		 bef_editor=bef_pref.edit();
		 
		 //bef_pref = context.getSharedPreferences("TIPS", Context.MODE_PRIVATE);
		 //bef_editor=bef_pref.edit();
			
		 View view = inflater.inflate(R.layout.fragment_cosmopolitan_article, container, false);
		 layout = (RelativeLayout)view.findViewById(R.id.cosmopolitan_article_parent_layout);
		 
		 Log.v("TIPS_Oncreate Repeated",""+bef_pref.getBoolean("tips_repeated",false));
		 Log.v("TIPS_Oncreate Repeated",""+bef_pref.getBoolean("tips_repeated",true));
		 if(bef_pref.getBoolean("tips_repeated", true)) // 1) 초기 : if조건 참! --> async 실행
		 {												//  2) 값이 바뀌었다!--> async실행
			 Log.v("TIS_oncreate","초기");

			 TIPS_cosmopolitan();
		 }
		 else //값이 바뀌지 않았다! pref 안에 저장되어 있는 img_url로 asynctask onPostExecute에서 하는 작업을 바로 실행
		 {    		
			 Log.v("TIPS_oncreate","반복");
			 
			 String previouslyEncodedImage = bef_pref.getString("BMP", "init");

			 if( !previouslyEncodedImage.equalsIgnoreCase("init") )
			 {
			     byte[] b = Base64.decode(previouslyEncodedImage, Base64.DEFAULT);
			     Bitmap bitmap = BitmapFactory.decodeByteArray(b, 0, b.length);
			     Drawable d = new BitmapDrawable(getResources(), bitmap);
				 
			     if(d != null){
			    	 layout.setBackground(d);
			     }
			 }

			 else
			 {
				 imageLoad(bef_pref.getString("img_url", img_url));
			 }
		 }


		layout.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
	        	 FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
	          	Log.v("href",bef_pref.getString("tips_href", "init"));
	        	 SearchWebViewFragment webview = new SearchWebViewFragment();
	        	 webview.setUrl(bef_pref.getString("tips_href", "init"));
	        	 fragmentTransaction.replace(R.id.cosmopolitan_article_parent_layout, webview, ZubayApplication.TAG_FRAGMENT_SEARCH_WEBVIEW);
	        	// fragmentTransaction.replace(R.id.layout, webview, ZubayApplication.TAG_FRAGMENT_SEARCH_WEBVIEW);
	        	 fragmentTransaction.addToBackStack(null);
	        	 fragmentTransaction.commit();
			}
		});
		TextView content = (TextView)view.findViewById(R.id.cosmopolitan_article_content);
		
		Typeface type = Typeface.createFromAsset(context.getAssets(), "font.ttf");
		content.setTypeface(type);
		
		content.setText(bef_pref.getString("title", "init")+'('+bef_pref.getString("date", "init")+')');
		
		return view;
	}

	
	public void imageLoad(String img_url)
	{
		Log.v("img_Load",img_url);
		Log.v("title",title);
		Log.v("href",href);
		final Context context = ZubayApplication.getInstance().getApplicationContext();
		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).build();
		ImageLoader imageloader = ImageLoader.getInstance();
		//ImageLoader.getInstance().destroy();
		imageloader.init(config);
		
		imageloader.loadImage(img_url, new SimpleImageLoadingListener() {
			
	        @Override
	        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage)
	        {
	        	imageSetup(context,title,href,loadedImage,layout);
	        }
	    });	        
	    
	}   
	
	public void imageSetup(Context context, String title, String href, Bitmap banner,RelativeLayout layout)
	{
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		banner.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] byteArray = stream.toByteArray();
				
		Bitmap bitmap = BitmapFactory.decodeByteArray(byteArray,0,byteArray.length);
		//bmp = bitmap.createScaledBitmap(bitmap, 170, 154, false);
		Drawable d = new BitmapDrawable(getResources(),bitmap);
		//bef_editor.put
		layout.setBackground(d);
		
		//이하 pref 저장
		String encodedImage = Base64.encodeToString(byteArray,Base64.DEFAULT);
		bef_editor.putString("BMP", encodedImage);
		bef_editor.commit();
	}
	
    public void parseCOSMOJSON(String json) throws JSONException
    {
    	 try {    	

    	        jsonObject = new JSONObject(json);
    	    	
    	        href=jsonObject.getString("href");
    	    	img_url=jsonObject.getString("img");
    	    	title=jsonObject.getString("title");
    	    	date=jsonObject.getString("date");

    	    	
    	    		if(date==bef_pref.getString("date", "")) // 1)초기 : getString이 ""이 되면서 else문으로 넘어감
    	    		{			
    	    												//  2) 이전과 같다! == 반복O --> 값을 다시 넣을 필요가 없음
    	    	    	Log.v("TIPS_parseJson","반복");

    	    			bef_editor.putBoolean("tips_repeated", true);
    	    			bef_editor.commit();
    	    	    	Log.v("TIPS_repeated if",""+bef_pref.getBoolean("tips_repeated", false));

    	    		}
    	    		else 									// 1)이전과 다르다! == 반복X --> 다른 값이므로 pref안에 다른 값을 넣어야함!
    	    		{
    	    	    	Log.v("TIPS_parseJson","초기");

    	    			bef_editor.putBoolean("tips_repeated", false);
    	    			bef_editor.commit();
    	    	    	Log.v("TIPS_repeated else",""+bef_pref.getBoolean("tips_repeated", false));

    	    			bef_editor.putString("tips_href",href);
    	    			bef_editor.commit();

    	    			bef_editor.putString("img_url",img_url);
    	    			bef_editor.commit();

    	    			bef_editor.putString("title",title);
    	    			bef_editor.commit();

    	    			bef_editor.putString("date",date);
    	    			bef_editor.commit();

    	    		}   

    	    } catch (JSONException e) {
    	    	Log.v("5555","JSONException");
    	        e.printStackTrace();
    	    }
    }

    public void TIPS_cosmopolitan()
    {
		Log.v("TIPS_cosmopolitan","start!");
       AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {

            @Override
            protected String doInBackground(Void... params) {
        		Log.v("TIPS_doInBackground","start!");

        		String json_string="";
        		
            	DefaultHttpClient httpclient = new DefaultHttpClient();
            	
            	HttpPost httpPost= new HttpPost("http://172.16.100.172:52273/tips"); //서버 IP
			
                try {
                    HttpResponse response = httpclient.execute(httpPost);                	
                    json_string = EntityUtils.toString(response.getEntity());
                    parseCOSMOJSON(json_string);  
                	
                } catch (ClientProtocolException e) {
                	e.printStackTrace();
                } catch (IOException e) {
                	e.printStackTrace();
				} catch (JSONException e) {
					e.printStackTrace();
				}
                
               Log.v("doInBackground","finish");
                return json_string;
            }
            
            @Override
            protected void onPostExecute(String json_string) {
            	super.onPostExecute(json_string);
            	Log.v("result",json_string);        		
        		imageLoad(img_url);
            }
        		}; // 
        if(Build.VERSION.SDK_INT >= 11)
        	task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        else
        	task.execute();
Log.v("AsyncTask","fisnish");
    }
}
