package com.zubaykids.initapp;

import android.app.Application;

public class ZubayApplication extends Application {

	private static ZubayApplication appInstance;

	public static final String SENSOR_OK = "zubay.com.service.sensor";

	//Dday SharedPreference key
	public static final String SHARED_PREFERENCE_DDAY = "DALSOM_DDAY";
	public static final String SHARED_PREFERENCE_DEFAULT_VALUE = "DEFAULT_DDAY";
	public static final String SHARED_PREFERENCE_DDAY_PRIMARY = "DDAY_PRIMARY";
	
	//Weather SharedPreference key
	public static final String SHARED_PREFERENCE_WEATHER_TEMP = "WEATHER_TEMP";
	public static final String SHARED_PREFERENCE_WEATHER_IMAGE = "WEATHER_IMAGE";
	
	//Save Couple Info SharedPreference key
	public static final String SHARED_PREFERENCE_SAVE_INFO = "SAVE_INFO";
	
	//Couple Index SharedPreference key
	public static final String SHARED_PREFERENCE_COUPLE_INDEX_ENDCALL = "ENDCALL_TIME";
	
	//First Time Exec SharedPreference key
	public static final String SHARED_PREFERENCE_FIRST_OPEN_FILENAME = "FIRST_OPEN";
	public static final String SHARED_PREFERENCE_IS_FIRST_WEBVIEW_OPEN = "WEBVIEW_FIRST";
	public static final String SHARED_PREFERENCE_IS_FIRST_APP_USED = "FIRST_APP_USED";
	
	//GCM Suggest Save SharedPreference key
	public static final String SHARED_PREFERENCE_SUGGEST_ITEM_FILENAME = "GCM_SUGGEST_FILENAME";
	public static final String SHARED_PREFERENCE_SUGGEST_TITLE = "GCM_SUGGEST_TITLE";
	public static final String SHARED_PREFERENCE_SUGGEST_CONTENT = "GCM_SUGGEST_CONTENT";
	public static final String SHARED_PREFERENCE_SUGGEST_COUNT = "GCM_SUGGEST_COUNT";
	public static final String SHARED_PREFERENCE_SUGGEST_IS_USE = "GCM_SUGGEST_IS_USD";
	public static final String SHARED_PREFERENCE_SUGGEST_URL = "GCM_SUGGEST_URL";
	
	public static final String SHARED_PREFERENCE_SUGGEST_DATA_CHANGED = "GCM_DATA_CHANGE";
	
	//Fragment Tag
	public static final String TAG_FRAGMENT_SEARCH_WEBVIEW = "TAG_WEBVIEW";
	public static final String TAG_FRAGMENT_FIRST_WEBVIEW = "TAG_FIRST_WEBVIEW";
	
	// BadgeView Tag
	public static final String CALL_BADGE = "CALL_BADGE";
	public static final String MESSAGE_BADGE = "MESSAGE_BADGE";
	
	//Intent
	public static final String INTENT_KILL_APPLICATION = "com.zubaykids.KILL_APPLICATION";
	
	
	public ZubayApplication() {
		appInstance = this;
	}

	public static ZubayApplication getInstance() {
		return appInstance;
	}
}
