package com.zubaykids.initapp;

import java.lang.reflect.Field;

import android.content.Context;
import android.graphics.Typeface;

public final class FontChanger {
	
	public static void setDefaultFont(String staticTypefaceFieldName, String fontAssetName){
		Context context = null;
		context = ZubayApplication.getInstance().getApplicationContext();
		final Typeface regular = Typeface.createFromAsset(context.getAssets(), fontAssetName);
		replaceFont(staticTypefaceFieldName, regular);
	}
	
	protected static void replaceFont(String staticTypefaceFieldName, final Typeface newTypeface){
		try{
			final Field staticField = Typeface.class.getDeclaredField(staticTypefaceFieldName);
			staticField.setAccessible(true);
			staticField.set(null, newTypeface);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
