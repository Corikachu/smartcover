package com.zubaykids.initapp.zeroPageFragment;

import com.zubaykids.smartcover.R;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class CoupleFestivalFragment extends Fragment{
	
	public static CoupleFestivalFragment newInstance(){
		CoupleFestivalFragment frag = new CoupleFestivalFragment();
		return frag;
	}
	
	public CoupleFestivalFragment() {
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.fragment_couple_festival, container, false);
		
		return view;
	}
}
