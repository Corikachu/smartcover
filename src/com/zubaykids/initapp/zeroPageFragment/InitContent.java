package com.zubaykids.initapp.zeroPageFragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.zubaykids.gcm.SuggestItem;
import com.zubaykids.gcm.SuggestItem.Item;
import com.zubaykids.initapp.ZubayApplication;
import com.zubaykids.smartcover.R;

public class InitContent {
	
	private Context context;
	
	public InitContent(){
		context = ZubayApplication.getInstance().getApplicationContext();
		
		Bitmap bm1 = BitmapFactory.decodeResource(context.getResources(), R.drawable.dalsome1);
		Bitmap bm2 = BitmapFactory.decodeResource(context.getResources(), R.drawable.dalsome2);
		Bitmap bm3 = BitmapFactory.decodeResource(context.getResources(), R.drawable.dalsome3);
		Bitmap bm4 = BitmapFactory.decodeResource(context.getResources(), R.drawable.dalsome4);
		Bitmap bm5 = BitmapFactory.decodeResource(context.getResources(), R.drawable.dalsome5);
		
		SuggestItem.Item item1 = new Item(bm1, "이런 특별한 커플 사진 어때요", "[Wooubi 스튜디오]", 1, "http://wooubistudio.tistory.com/262");
		SuggestItem.Item item2 = new Item(bm2, "커플운동화로 껑충~!", "[뉴발란스]", 2, "http://nbkorea.tistory.com/616");
		SuggestItem.Item item3 = new Item(bm3, "귀엽고 상큼한 캐릭터 커플티 추천!", "[허니플라자]", 3, "http://blog.naver.com/PostThumbnailView.nhn?blogId=honeyplaza1&logNo=110171672171&categoryNo=1&parentCategoryNo=");
		SuggestItem.Item item4 = new Item(bm4, "추억을 피규어로 남겨요", "[MOA스튜디오]", 4, "http://3dstudiomoa.com/gallery-moa/");
		SuggestItem.Item item5 = new Item(bm5, "야간 데이트 어때요?", "[청계천 등불축제]", 5, "http://okok.co.kr/154");
		
		SuggestItem suggest = new SuggestItem();
		suggest.saveItem(item1);
		suggest.saveItem(item2);
		suggest.saveItem(item3);
		suggest.saveItem(item4);
		suggest.saveItem(item5);
	}
}
