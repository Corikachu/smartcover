package com.zubaykids.initapp.zeroPageFragment;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.zubaykids.gcm.SuggestItem;
import com.zubaykids.initapp.ZubayApplication;
import com.zubaykids.initapp.view.SearchWebViewFragment;
import com.zubaykids.smartcover.R;

public class ContentFragment4 extends Fragment{
	private static ClickListener listener;
	private SuggestItem.Item item;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_suggest_content_container4, container, false);
		
		RelativeLayout layout = (RelativeLayout)view.findViewById(R.id.suggest_content_parent_layout4);
		TextView title = (TextView)view.findViewById(R.id.suggest_content_title_textview4);
		TextView content = (TextView)view.findViewById(R.id.suggest_content_content_textview4);
		
		Bitmap bitmap = item.getBitmap();
		Drawable background = new BitmapDrawable(getResources(), bitmap);
		layout.setBackground(background);
		title.setText(item.getTitle());
		content.setText(item.getContent());
		
		RelativeLayout bottomLayout = (RelativeLayout)view.findViewById(R.id.suggest_bottom_relativelayout4);
		listener = new ClickListener();
		bottomLayout.setOnClickListener(listener);	
		return view;
	}
	
	public class ClickListener implements View.OnClickListener{

		@Override
		public void onClick(View v) {
			SearchWebViewFragment mWebView = new SearchWebViewFragment();
			mWebView.setUrl(item.getUrl());
			mWebView.setTitle(item.getTitle());
			FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
			fragmentTransaction.replace(R.id.suggest_content_parent_layout4, mWebView, ZubayApplication.TAG_FRAGMENT_SEARCH_WEBVIEW);
			fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			fragmentTransaction.addToBackStack(null);
			fragmentTransaction.commit();
		}
		
	}

	public void setItem(SuggestItem.Item item){
		this.item = item;
	}
}
