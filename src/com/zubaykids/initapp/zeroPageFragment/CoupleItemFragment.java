package com.zubaykids.initapp.zeroPageFragment;

import com.zubaykids.smartcover.R;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

public class CoupleItemFragment extends Fragment {
	
	public static CoupleItemFragment newInstance(){
		CoupleItemFragment frag = new CoupleItemFragment();
		return frag;
	}
	
	public CoupleItemFragment() {
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_couple_item, container, false);
		
		RelativeLayout layout = (RelativeLayout)view.findViewById(R.id.couple_suggest_item_linearlayout);
		
		
		return view;
	}
}
