package com.zubaykids.initapp;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewPager;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.zubaykids.SaveInfo.PartnerInfo;
import com.zubaykids.bottomAnimation.moveRect.RectAnimation;
import com.zubaykids.call.CallPhoneStateListener;
import com.zubaykids.initapp.view.SectionsPagerAdapter;
import com.zubaykids.initapp.zeroPageFragment.InitContent;
import com.zubaykids.smartcover.R;
import com.zubaykids.weather.WeatherService;

public class SmartCoverMainActivity extends Activity {

	private SectionsPagerAdapter mSectionsPagerAdapter;
	private ViewPager mViewPager;
	private KillAppReceiver mReceiver;

	RelativeLayout background;
	JSONObject jsonObject = new JSONObject();
	String tmp_myregID;
	String tmp_yourregID;
	String regid = "";
	String yourregID = "";
	SharedPreferences start;
	private final String TAG = "KOK";
	public static final String PROPERTY_REG_ID = "registration_id";
	private static final String PROPERTY_APP_VERSION = "appVersion";
	private static String INTENT_KILL_APP = ZubayApplication.INTENT_KILL_APPLICATION;
	public static ArrayList<Activity> actList = new ArrayList<Activity>();

	public static RectAnimation mainLED;
	public static Handler mHandler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.activity_smart_cover_main);

		mHandler = new Handler();
		mainLED = (RectAnimation)findViewById(R.id.cover_main_led_animation);
		mainLED.setRepeatCount(5);

		//set Font
		//		FontChanger.setDefaultFont("DEFAULT", "font.ttf");

		SharedPreferences mPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

		// 남녀 따로 default_배경화면
		background = (RelativeLayout)findViewById(R.id.background);
		background.setBackgroundResource(R.drawable.default_background_man);

		if(mPref.getString("SEX", "0").equals("1")) {
			background.setBackgroundResource(R.drawable.default_background_girl);
		}

		// check this app is first start
		start = PreferenceManager.getDefaultSharedPreferences(this);
		if(isFirstExec()) {
			finish();
			Intent intent = new Intent(this,
					com.zubaykids.start.StartPage1.class);
			startActivity(intent);

			//init ZeroPage Content
			InitContent it = new InitContent();

			turnOffFirstTimeApp();
		}else {
			// set background
			try {
				InputStream is = openFileInput("BACKGROUND_PICTURE");
				Bitmap b = BitmapFactory.decodeStream(is);
				Drawable drawable = new BitmapDrawable(b);

				background.setBackground(drawable);				
			} catch(Exception e) {
				e.printStackTrace();
			}

			if(!start.getBoolean("REGID", false)) {
				regid = start.getString("MYREGID", "000");
				Log.d("regid", regid);
				//				SharedPreferences.Editor editor = start.edit();

				getREGID();

				//				editor.putBoolean("REGID", true);
				//				editor.commit();
			}

			FragmentManager frag = getFragmentManager();
			mSectionsPagerAdapter = new SectionsPagerAdapter(frag);

			// Set up the ViewPager with the sections adapter.
			mViewPager = (ViewPager) findViewById(R.id.pager);
			mViewPager.setAdapter(mSectionsPagerAdapter);
			mViewPager.setCurrentItem(1);
			mViewPager.setOffscreenPageLimit(5);

			// Stop LockScreen Service
			stopService(new Intent(getApplicationContext(), LockScreenService.class));
			// and start kill application service from LightSensor
			mReceiver = new KillAppReceiver();
			registerKillAppReceiver();
			//startService(new Intent(getApplicationContext(), KillAppService.class));

			// weather Service, 30분마다 weather정보 reset
			startService(new Intent(getApplicationContext(), WeatherService.class));


			if(PartnerInfo.callBroad == false) { 
				callServiceRegister();
				PartnerInfo.callBroad = true;
			}

		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		this.getActionBar().hide();

		// add lock_Screen Flag.
		getWindow().addFlags(
				WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED
				| WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD
				| WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
				| WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

		// background 설정
		try {
			InputStream is = openFileInput("BACKGROUND_PICTURE");
			Bitmap b = BitmapFactory.decodeStream(is);
			Drawable drawable = new BitmapDrawable(b);

			background.setBackground(drawable);				
		} catch(Exception e) {
			e.printStackTrace();
		}

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();

		//start background lock-screen service
		startService(new Intent(getApplicationContext(), LockScreenService.class));

		//unregister kill application broadcast receiver
		//unregisterKillAppReceiver();

	}

	/*
	 * Get IntentFiler to kill a Application
	 */
	private static IntentFilter createIntentFilter() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(INTENT_KILL_APP);
		return filter;
	}

	protected void registerKillAppReceiver(){
		getApplication().registerReceiver(mReceiver, createIntentFilter());
	}

	protected void unregisterKillAppReceiver(){
		getApplication().unregisterReceiver(mReceiver);
	}

	public class KillAppReceiver extends BroadcastReceiver{
		@Override
		public void onReceive(Context context, Intent intent) {
			if(intent.getAction().equals(INTENT_KILL_APP)){
				finish();
			}
		}
	}


	private boolean isFirstExec(){
		String sharedFileName = ZubayApplication.SHARED_PREFERENCE_FIRST_OPEN_FILENAME;
		String key = ZubayApplication.SHARED_PREFERENCE_IS_FIRST_APP_USED;
		SharedPreferences shared = getSharedPreferences(sharedFileName, Context.MODE_PRIVATE);

		return shared.getBoolean(key, true);
	}

	private void turnOffFirstTimeApp(){
		String sharedFileName = ZubayApplication.SHARED_PREFERENCE_FIRST_OPEN_FILENAME;
		String key = ZubayApplication.SHARED_PREFERENCE_IS_FIRST_APP_USED;
		SharedPreferences shared = getSharedPreferences(sharedFileName, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = shared.edit();

		editor.putBoolean(key, false);
		editor.apply();
	}

	private void callServiceRegister() {
		// call ServiceReceiver
		CallPhoneStateListener callReceiver = new CallPhoneStateListener();
		Context context = getApplicationContext();
		callReceiver.setContext(context);
		IntentFilter callFilter = new IntentFilter();
		callFilter.setPriority(Integer.MAX_VALUE);
		callFilter.addAction("android.intent.action.PHONE_STATE");
		TelephonyManager telephony = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		telephony.listen(callReceiver, PhoneStateListener.LISTEN_CALL_STATE);
	}

	private class OnPageChangeListener implements ViewPager.OnPageChangeListener{

		@Override
		public void onPageScrollStateChanged(int arg0) {
		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
		}

		@Override
		public void onPageSelected(int position) {
			//mSectionsPagerAdapter.notifyDataSetChanged();
		}

	}

	public void getREGID()
	{
		Log.v("getREGID","getREGID");
		AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
			@Override
			protected String doInBackground(Void... params) {
				String msg = "";

				DefaultHttpClient httpclient = new DefaultHttpClient();

				//HttpPost httpPost = new HttpPost("http://172.16.101.142:52273/get"); : 노트
				HttpPost httpPost= new HttpPost("http://172.16.100.172:52273/get"); //서버 IP

				ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
				nameValuePairs.add(new BasicNameValuePair("mynum", new com.zubaykids.SaveInfo.PartnerInfo().getMyPhone()));
				//이경문 01076778474
				//노일호 01022301803
				//임원균 01050329432
				//장현우 01031107690
				try {
					httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					Log.v("UnsupportedEncodingException","UnsupportedEncodingException");
					e1.printStackTrace();
				}				
				try {
					Log.v("getREGID response","before");
					HttpResponse response = httpclient.execute(httpPost);
					Log.v("getREGID response","after");

					Log.v("json_string","before");
					String json_string = EntityUtils.toString(response.getEntity());
					Log.v("json_string AFTER",json_string);

					Log.v("parseJSON","before");
					parseJSON(json_string);  
					Log.v("parseJSON","after");

				} catch (ClientProtocolException e) {
					Log.v("ClientProtocolException", "ClientProtocolException");
					e.printStackTrace();
				} catch (IOException e) {
					Log.v("IOException", "IOException");
					e.printStackTrace();
				} catch (JSONException e) {
					Log.v("JSONException", "JSONException");
					e.printStackTrace();
				}

				if(!regid.equals("") && !yourregID.equals("")) {
					SharedPreferences.Editor editor = start.edit();
					editor.putBoolean("REGID", true);
					editor.commit();

					Log.v("regidJang", "true");
				} else {
					Log.v("regidJang", "false");
				}

				return msg;
			}

			@Override
			protected void onPostExecute(String msg) {
			}
		};

		if(Build.VERSION.SDK_INT >= 11)
			task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		else
			task.execute();
	}

	public void parseJSON(String json) throws JSONException
	{

		try {
			JSONArray jsonArray = new JSONArray(json);
			jsonObject = jsonArray.getJSONObject(0);
			Log.e(TAG, jsonObject.getString("myregID"));
			Log.e(TAG, jsonObject.getString("yourregID"));
		} catch (JSONException e) {
			Log.v("JSONException","JSONException");
			//Toast.makeText(getApplicationContext(), "상대방 등록이 정상적으로 이뤄지지 않았습니다", Toast.LENGTH_LONG).show();
			e.printStackTrace();
		}

		tmp_myregID = jsonObject.getString("myregID");

		tmp_yourregID = jsonObject.getString("yourregID");

		if(regid.equals(tmp_myregID))
		{
			Log.v("5555","1111");
			yourregID = tmp_yourregID;
		}
		else if(regid.equals(tmp_yourregID))
		{
			Log.v("6666","2222");
			yourregID=tmp_myregID;
		}
		else
		{
			//Toast.makeText(getApplicationContext(), "상대방 등록이 정상적으로 이뤄지지 않았습니다", Toast.LENGTH_LONG).show();
			Log.v("매칭 비정상","");
		}

		Log.v("yourregID", yourregID);
		SharedPreferences.Editor editor = start.edit();
		editor.putString("YOURREGID", yourregID);

		editor.commit();
	}
}
