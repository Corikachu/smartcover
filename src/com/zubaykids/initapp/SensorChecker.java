package com.zubaykids.initapp;

/**
 * @author Wonkyun Lim(Corikachu)
 * 2014. 09. 19.
 */

import java.util.ArrayList;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Looper;
import android.os.Process;
import android.util.Log;

public class SensorChecker {

	private Context context;
	private TriggerDetector mDetector;
	private Thread mDetectorThread;

	public SensorChecker() {
		context = ZubayApplication.getInstance().getApplicationContext();
		this.mDetector = new TriggerDetector(context);
	}

	public void start() {
		this.mDetectorThread = new Thread(this.mDetector);
		this.mDetectorThread.start();

	}

	public void stop() {
		if (this.mDetectorThread != null) {
			this.mDetectorThread.interrupt();
			this.mDetector.stop();
		}
	}

	private static class TriggerDetector implements Runnable,
			SensorEventListener {

		private Context context;

		// private static final String TAG = "TriggerDetector";
		private static final int SEGMENT_SIZE = 20;
		private static final int NUM_SEGMENTS = 2;
		private static final int LIMIT_LIGHT_SENSOR = 5;

		private SensorManager mSensorManager;
		private Sensor mMagnetometer, mLightSensor;
		private ArrayList<float[]> mMagnetSensorData;
		private float mLightSensorData;
		private float[] mOffset = new float[SEGMENT_SIZE];

		public TriggerDetector(Context context) {
			this.context = context;
			mMagnetSensorData = new ArrayList<float[]>();
			mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
			mMagnetometer = this.mSensorManager
					.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
			mLightSensor = this.mSensorManager
					.getDefaultSensor(Sensor.TYPE_LIGHT);

		}

		private void addData(float[] value, long time) {
			float[] values = { value[0], value[1], value[2] };

			if (this.mMagnetSensorData.size() > SEGMENT_SIZE * 2) {
				mMagnetSensorData.remove(0);
			}

			this.mMagnetSensorData.add(values);

			evaluateModel();
		}

		/*
		 * evaluate Magnet Sensor data. calculate mean, minimum, maximum data
		 * based on offset.
		 */
		private void evaluateModel() {
			if (this.mMagnetSensorData.size() < SEGMENT_SIZE * 2) {
				return;
			}

			float[] means = new float[NUM_SEGMENTS];
			float[] minimums = new float[NUM_SEGMENTS];
			float[] maximums = new float[NUM_SEGMENTS];

			float[] baseline = (float[]) mMagnetSensorData
					.get(mMagnetSensorData.size() - 1);

			for (int i = 0; i < NUM_SEGMENTS; i++) {
				int segmentStart = SEGMENT_SIZE * i;

				float[] mOffsets = computeOffsets(segmentStart, baseline);

				means[i] = computeMeans(mOffsets);
				minimums[i] = computeMinimums(mOffsets);
				maximums[i] = computeMaximums(mOffsets);
			}

			// min, max data
			float min1 = minimums[0];
			float max2 = maximums[1];


			// TODO : adjust the min and max data.
			// magnet sensor is enough changed.
			if ((min1 > 0.9F) && (max2 > 180.0F)) {
				if (mLightSensorData < LIMIT_LIGHT_SENSOR) {

					context.sendBroadcast(new Intent(ZubayApplication.SENSOR_OK));

					// clear data
					this.mMagnetSensorData.clear();
				}
			}
		}

		private float[] computeOffsets(int start, float[] baseline) {

			for (int i = 0; i < 20; i++) {
				float[] point = (float[]) mMagnetSensorData.get(start + i);
				float[] distinction = { (point[0] - baseline[0]),
						point[1] - baseline[1], point[2] - baseline[2] };

				float magnitude = (float) Math.sqrt(distinction[0]
						* distinction[0] + distinction[1] * distinction[1]
						+ distinction[2] * distinction[2]);
				this.mOffset[i] = magnitude;
			}
			return this.mOffset;
		}

		private float computeMeans(float[] offsets) {
			float sum = 0.0F;
			for (float o : offsets) {
				sum += o;
			}
			return sum / offsets.length;
		}

		private float computeMinimums(float[] offsets) {
			float min = 1.0F;
			for (float o : offsets) {
				min = Math.min(o, min);
			}
			return min;
		}

		private float computeMaximums(float[] offsets) {
			float max = -1.0F;
			for (float o : offsets) {
				max = Math.max(o, max);
			}
			return max;
		}

		@Override
		public void onSensorChanged(SensorEvent event) {
			if (event.sensor.equals(mMagnetometer)) {
				float[] values = event.values;
				if ((values[0] == 0.0F) && (values[1] == 0.0F)
						&& (values[2] == 0.0F)) {
					return;
				}

				addData(event.values, event.timestamp);

			} else if (event.sensor.equals(mLightSensor)) {
				float[] values = event.values;
				mLightSensorData = values[0];
			}
		}

		@Override
		public void onAccuracyChanged(Sensor sensor, int accuracy) {

		}

		@Override
		public void run() {
			//Process.setThreadPriority(Process.THREAD_PRIORITY_URGENT_AUDIO);
			Looper.prepare();
			this.mSensorManager.registerListener(this, mMagnetometer,
					SensorManager.SENSOR_DELAY_FASTEST);
			this.mSensorManager.registerListener(this, mLightSensor,
					SensorManager.SENSOR_DELAY_NORMAL);
			Looper.loop();
		}

		public void stop() {
			this.mSensorManager.unregisterListener(this);

		}
	}

}
