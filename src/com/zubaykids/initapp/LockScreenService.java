package com.zubaykids.initapp;

import android.app.Service;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

public class LockScreenService extends Service {

	private LockScreenReceiver mReceiver = null;
	private static SensorChecker mSensorChecker = null;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		
		mSensorChecker = new SensorChecker();
		mSensorChecker.start();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		super.onStartCommand(intent, flags, startId);

		if (mSensorChecker == null) {
			mSensorChecker = new SensorChecker();
			mSensorChecker.start();
		}

		if (intent != null) {
			if (intent.getAction() == null) {
				if (mReceiver == null) {
					mReceiver = new LockScreenReceiver();

					// get a action of Screen_On and Register Receiver
					IntentFilter filter = new IntentFilter(
							Intent.ACTION_SCREEN_OFF);
					registerReceiver(mReceiver, filter);
				}
			}
		}
		return START_REDELIVER_INTENT;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		// stop SensorChecker
		if (mSensorChecker != null) {
			mSensorChecker.stop();
		}

		// unregister receiver
		if (mReceiver != null) {
			unregisterReceiver(mReceiver);
		}
	}
}
