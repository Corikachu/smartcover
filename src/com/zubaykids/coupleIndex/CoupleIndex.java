package com.zubaykids.coupleIndex;

import java.util.Calendar;

import com.zubaykids.initapp.ZubayApplication;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

/**
 * Created by corikachu on 14. 11. 5..
 */
public class CoupleIndex {
    //step is hour at criterion.
    public static final int step1 = 0;
    public static final int step2 = 3;
    public static final int step3 = 5;
    public static final int step4 = 8;
    public static final int step5 = 12;
    public static final int step6 = 24;
    public static final int step7 = 48;
    public static final int step8 = 120;
    public String ENDCALL_TIME = ZubayApplication.SHARED_PREFERENCE_COUPLE_INDEX_ENDCALL;

    private long localTime;
    private long endCallTime;

    private int coupleIndex;

    private Calendar mCalendar;
    private Context mContext;
    private SharedPreferences mSharedPref;
    
    public CoupleIndex() {
        mCalendar = Calendar.getInstance();
        mContext = ZubayApplication.getInstance().getApplicationContext();
        mSharedPref = PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    public void setEndCallTime(long time){
        this.endCallTime = time;
        
        SharedPreferences.Editor editor = mSharedPref.edit();
        editor.putLong(ENDCALL_TIME, endCallTime);
        editor.apply();
    }
    
    public void loadEndCallTime(){
    	endCallTime = mSharedPref.getLong(ENDCALL_TIME, 0);
    }

    private void refreshTime(){
        this.localTime = mCalendar.getTimeInMillis();
    }

    private void calculateCoupleIndex(){

    	if(endCallTime < 0){
    		coupleIndex = 100;
    		return;
    	}
    	
        long time = endCallTime - localTime;

        int timeToHour = (int)time/(1000*60*60);
        int timeToMinute = (int)time/(1000*60);

        Log.d(String.valueOf(timeToHour), String.valueOf(timeToMinute));

        int timeIntervalHour = 1;
        int timeIntervalMinute = 60;

        if(timeToHour < step2){
            //this interval is that coupleIndex is 100%.
            coupleIndex = 100;

        }else if(timeToHour < step3){
            //this interval is from 100% to 90%.
            //Time is from 3 hour to 5 hour
            timeIntervalHour = step3 - step2;
            timeIntervalMinute *= timeIntervalHour;
            timeIntervalMinute /= 10;

            timeToMinute = timeToMinute - step2*60;

            int minusPercent = timeToMinute/timeIntervalMinute;

            coupleIndex = 100 - minusPercent;

        }else if(timeToHour < step4){
            //this interval is from 90% to 80%.
            timeIntervalHour = step4 - step3;
            timeIntervalMinute *= timeIntervalHour;
            timeIntervalMinute /= 10;

            timeToMinute = timeToMinute - step3*60;

            int minusPercent = timeToMinute/timeIntervalMinute;

            coupleIndex = 90 - minusPercent;

        }else if(timeToHour < step5){
            //this interval is from 80% to 70%
            timeIntervalHour = step5 - step4;
            timeIntervalMinute *= timeIntervalHour;
            timeIntervalMinute /= 10;

            timeToMinute = timeToMinute - step4*60;

            int minusPercent = timeToMinute/timeIntervalMinute;

            coupleIndex = 80 - minusPercent;

        }else if(timeToHour < step6){
            //this interval is from 70% to 60%
            timeIntervalHour = step6 - step5;
            timeIntervalMinute *= timeIntervalHour;
            timeIntervalMinute /= 10;

            timeToMinute = timeToMinute - step5*60;

            int minusPercent = timeToMinute/timeIntervalMinute;

            coupleIndex = 70 - minusPercent;

        }else if(timeToHour < step7){
            //this interval is from 60% to 50%
            timeIntervalHour = step7 - step6;
            timeIntervalMinute *= timeIntervalHour;
            timeIntervalMinute /= 10;

            timeToMinute = timeToMinute - step6*60;

            int minusPercent = timeToMinute/timeIntervalMinute;

            coupleIndex = 60 - minusPercent;

        }else if(timeToHour < step8){
            //this interval is from 50% to 40%
            timeIntervalHour = step8 - step7;
            timeIntervalMinute *= timeIntervalHour;
            timeIntervalMinute /= 10;

            timeToMinute = timeToMinute - step7*60;

            int minusPercent = timeToMinute/timeIntervalMinute;

            coupleIndex = 50 - minusPercent;

        }else{
        	//this interval is from 40% to 0%
            timeIntervalMinute = 10;

            timeToMinute = timeToMinute - step8*60;
            int minusPercent = timeToMinute/timeIntervalMinute;

            coupleIndex = 40 - minusPercent;

           //if Index < 0  out
           if(coupleIndex < 0){
               coupleIndex = 0;
           }
        }
    }

    /*
     * Get Couple Index to int.
     */
    public int getCoupleIndex(){
    	loadEndCallTime();
        refreshTime();
        calculateCoupleIndex();
        return coupleIndex;
    }
    
    /*
     * Get EndCall Time to long type.
     */
    public long getEndCallTime(){
    	loadEndCallTime();
    	return endCallTime;
    }

}
